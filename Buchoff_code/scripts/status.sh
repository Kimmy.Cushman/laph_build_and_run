#!  /bin/bash

#   report status of evolution & measurement for given runs

d0=$PWD

set -ex

# all runs
runs=$(qls -d ~/scratch/rzuseq/lsd/nc=*/nf=0/nx=*/nt=*/beta*/meas/mibspec/kappa*)

# live runs
runs=$(cat ~/all_runs | ak 1 | grep mibspec | sed -e "s|/[0-9]$||" | sort -u)

for r in $runs; do
#{
  cd $r

  pwd

  #echo read; read

  mx=bary_4u_Spin2_SHELL_SOURCE_POINT_SINK*0

  if ! qls $r/meas/*/$mx | grep -q .; then
    echo "no meas files"
    continue
  fi

  echo "most recent measurement:      $(ll -rt $r/meas/ | tail -n 1)"

  echo "number of measurements total: $(cat $r/meas/*/$mx | wc -l)"

  if ! find $r/meas/*/$mx -mtime -3 | grep -q .; then
    echo "no recent meas files"
    continue
  fi

  n_days=3
  recent_files=$(find $r -name "slurm*out" -mtime -$n_days)
  n_recent=$(grep "SU4_BARYON_FOUR.*sec" $recent_files | wc -l)
  n_recent=$[n_recent/6]  # NB: 6x per meas
  # don't do this since it's easy to get files with $n_days+1 different dates
  #n_days=$(ll $recent_files | ak 6 | sort -u | wc -l)   # NB: in case < $n_days days of output
  echo "measurements/day ($n_days-day avg): $[n_recent/n_days]"

  #ll $recent_files | sed -e "s|^|(|; s|$|)|"
  #echo "(n_recent: $n_recent n_days: $n_days)"
  #break
  
  cd $d0
#}
done

