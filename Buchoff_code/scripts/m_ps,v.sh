#!  /bin/bash

#   simple ps/v mass calculation

d0=$PWD

set -e

runs=$(qls -d ~/rzuseq/lsd/nc=4/nf=0/nx=*/nt=*/beta*/meas/mibspec/kappa*)
#runs=$(qls -d ~/rzuseq/lsd/nc=4/nf=0/nx=16/nt=32/beta=1[23].0/meas/mibspec/kappa*)
#runs=$(qls -d ~/rzuseq/lsd/nc=4/nf=0/*/*64/beta=1[23].0/meas/mibspec/kappa*)

for r in $runs; do
#{
  cd $r

  pwd

  #echo read; read

  RM -f fit.log

  SRC=SH

  for SNK in P; do    # NB: SH seems not to work well for mesons
  #{
    echo "  SRC: $SRC SNK: $SNK"

    echo "  check for data"

    qls meas/*/pion*$SRC*$SNK*0 | grep -q . || continue

    if [ -f m_ps,v.log ] && ! find meas/*/pion*$SRC*$SNK*0 -newer m_ps,v.log | grep -q .; then continue; fi

    echo "  avgerr"

    data=$(cat meas/szsc.lime*/pion*$SRC*$SNK*0)

    echo "  $(echo "$data" | wc -l) measurements"

    for (( t=0; t<20; t++ )); do
      echo t $t c $(avgerr $(echo "$data" | awk "{print \$$[t+5]}"))
    done > m_ps.avgerr_$SRC,$SNK

    data=$(cat meas/szsc.lime*/vector*$SRC*$SNK*0)

    for (( t=0; t<20; t++ )); do
      echo t $t c $(avgerr $(echo "$data" | awk "{print \$$[t+5]}"))
    done > m_v.avgerr_$SRC,$SNK

    (
      echo "a_ps = 1e-5; m_ps = 0.5"
      echo "fit [6:12] a_ps*exp(-m_ps*x) 'm_ps.avgerr_$SRC,$SNK' u 2:4:6 via a_ps,m_ps"
      echo "a_v = 1e-5; m_v = 0.5"
      echo "fit [6:12] a_v*exp(-m_v*x) 'm_v.avgerr_$SRC,$SNK' u 2:4:6 via a_v,m_v"
    ) > m_ps,v.gp

    echo "  gnuplot"

    gnuplot m_ps,v.gp &> /dev/null

    echo " " kappa ${PWD##*kappa=} $(grep "m_ps.*+/-" fit.log | tail -n 1 | sed -e "s|=||")
    echo " " kappa ${PWD##*kappa=} $(grep "m_v.*+/-"  fit.log | tail -n 1 | sed -e "s|=||")
  #}
  done

  if [ -f fit.log ]; then mv fit.log m_ps,v.log; fi

  cd $d0
#}
done

for r in $runs; do
#{
  cd $r
  pwd
  [ -f m_ps,v.log ] || continue
  params=$(pwd | sed -e "s|.*nf=0/||; s|meas/mibspec/||; s|[/=]| |g")
  echo $params $(grep "m.*+/-" m_ps,v.log)
  cd $d0
#}
done

