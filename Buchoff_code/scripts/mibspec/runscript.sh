#!  /bin/bash

set -x

date
echo "The JobID is " $SLURM_JOBID
echo "Starting at $(mydate)"

EXE=$HOME/binaries/lsd/nc=4/bgq/chroma_SU4_bary
ll $EXE

nodes=$SLURM_NNODES
tasks=$[nodes*${SLURM_TASKS_PER_NODE%(*}]
if (( nodes == 1 )); then tasks=1; fi

npx=1; npy=1; npz=1; npt=1; i=4

while (( npx*npy*npz*npt < tasks )); do
  case $i in 1 ) npx=$[npx*2];; 2 ) npy=$[npy*2];; 3 ) npz=$[npz*2];; 4 ) npt=$[npt*2];; esac
  i=$[((i+2)%4)+1]
done
if (( npx*npy*npz*npt != tasks )); then
  echo "exit - no good 'geom'"; exit
fi
geom="$npx $npy $npz $npt"

# run from .../[0-9] in order to do multiple jobs per directory
if pwd | grep -q "/[0-9]*$"; then
  ending=${PWD##*/}
  rsync ./"in.xml" ../ -au
  cd ..
else
  unset ending
fi

# create/modify input xml for each cfg

if [ ! -d cfg ] && [ -d ../../../cfg ]; then ln -s ../../../cfg ./cfg; fi

export OMP_NUM_THREADS=4 
export BG_THREADLAYOUT=1

env

chroma_cleanup .

cfg=$(ls -rt ./cfg/ | grep lime | tail -1)
SaveInt=$(head -n50 ./cfg/$cfg | grep -a SaveInterval | sed -e "s/[^>]*>//; s/<.*//")

if (( SaveInt == 10 )); then cfgs=$(ls -rt ./cfg/ | grep "lime.*0$"); fi
if (( SaveInt == 50 )); then cfgs=$(ls -rt ./cfg/ | grep "lime.*[02468]$"); fi

if [ "$ending" ]; then
  cfgs=$(echo "$cfgs" | grep "$ending$")    # nb: 0's must be included in directory name
fi

for cfg in $cfgs; do
#{
  nx=$(pwd | sed -e "s|.*nx=||; s|[^0-9].*||")
  nt=$(pwd | sed -e "s|.*nt=||; s|[^0-9].*||")
  mq=$(pwd | sed -e "s|.*/m=||; s|[^0-9.].*||")
  kappa=$(pwd | sed -e "s|.*/kappa=||; s|[^0-9.].*||")

  D=meas/$cfg
  # check for tar file
  if [ -f $D.tar ]; then tar xf $D.tar; fi
  mkdir -p $D

  inxml1="./in.xml"
  inxml2="$D/in.xml"

  outxml=$D/out.xml

  grep -B999 "<InlineMeas" $inxml1 > $inxml2

  # do 'n_meas' measurements, with random src locations ('t_srce')
  if (( nx <  32 )); then n_meas=30; fi
  if (( nx >= 32 )); then n_meas=50; fi

  # ignore following files (no longer computing)
  ignore="4u_Spin[01]"

  if qls $D/*SINK* | grep -q .; then
    n_now=$(wc -l $D/*SINK* | grep -v "$ignore" | ak 1 | sort -n | head -1)
  else
    n_now=0
  fi

  if (( n_now >= n_meas )); then continue; fi

  for (( n = n_now; n < n_meas; n++ )); do
    t_srce="$[RANDOM%nx] $[RANDOM%nx] $[RANDOM%nx] $[RANDOM%nt]"
    tmp=$(grep -A999 "<InlineMeas" $inxml1 | grep -B999 "</InlineMeas" | grep -v "InlineMeas")
    echo "$tmp" | sed -e "/<t_srce>/ s|>.*<|>$t_srce<|" >> $inxml2
  done

  grep -A999 "</InlineMeas" $inxml1 >> $inxml2

  sed -e "/<nrow>/        s|>.*<|>$nx $nx $nx $nt<|"        -i $inxml2
  sed -e "/<Mass>/        s|>.*<|>$mq<|"                    -i $inxml2
  sed -e "/<Kappa>/       s|>.*<|>$kappa<|"                 -i $inxml2
  sed -e "/<cfg_file>/    s|>.*<|>$PWD/cfg/$cfg<|"          -i $inxml2

  cd $D
  echo "$(mydate) starting $cfg" >> startstop
  srun -N $nodes -n $tasks $EXE -i ${inxml2#$D/} -o ${outxml#$D/} -geom $geom
  echo "$(mydate) $cfg finished" >> startstop
  cd -

# break # DEBUG: stop after 1 cfg
#}
done &

# only msub if still doing work
sleep 10
if find meas/*/out.xml -mmin -1 | grep -q .; then
  if [ "$ending" ]; then cd $ending; fi
  msub -l depend=$SLURM_JOBID runscript
fi

wait

echo "Finished at $(mydate)"


