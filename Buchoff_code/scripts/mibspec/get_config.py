import numpy as np
import os
import sys 

MACHINE="QUARTZ"

if MACHINE == "LASSEN":
    home = "/p/gpfs1/cushman2/laph_build_and_run"
if MACHINE == "QUARTZ":
    home = "/p/lustre1/cushman2/laph_build_and_run"
code_dir = "%s/code"%home
Step4_Observable_dir = "%s/Step4_Observables"%home

# physical params 
Nc = 4
Lx = int(sys.argv[1])
Nt = Lx*2

i = int(sys.argv[2]) # config num

config_type = "WEAK_FIELD"
baryon_base = "%s/buchoff_in_SU4.xml"%Step4_Observable_dir
print(baryon_base)

# filename 
Lx_dir = "%s/Production/L%iT%i"%(home, Lx, Nt)
beta_dir = "%s/betaWEAK"%Lx_dir
k_dir = "%s/kappaWEAK"%beta_dir
config_dir = "%s/cfg_%i"%(k_dir,i)

if not os.path.isdir(Lx_dir):
    os.system("mkdir %s"%Lx_dir)
if not os.path.isdir(beta_dir):
    os.system("mkdir %s"%beta_dir)
if not os.path.isdir(k_dir):
    os.system("mkdir %s"%k_dir)
if not os.path.isdir(config_dir):
    os.system("mkdir %s"%config_dir)
print(config_dir)



baryon_xml = "%s/baryon.xml"%config_dir
config = "%s/kWEAK/configs/cfg_%i.lime"%(beta_dir, i)


print("writing %s"%baryon_xml)
base_file = open(baryon_base, "r")
xml_config_file = open(baryon_xml, "w")

lines = base_file.readlines()
for line in lines:
    
    # output 
    if "<output_dir></output_dir>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config_dir + "/<" + post)  # need "/" !
    
    # Latt size 
    elif "<nrow></nrow>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
    elif "<LxLyLzNt></LxLyLzNt>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
    
    # type     
    elif "<cfg_type></cfg_type>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config_type + "<" + post) 
    
    elif "<cfg_file></cfg_file>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config + "<" + post) 

    else:
        xml_config_file.write(line)

base_file.close()
xml_config_file.close()

print(xml_config_file)
# run the xml to get the config
batch_output_file = "%s/baryon.out"%config_dir
os.system("rm %s"%batch_output_file)

runscript = "run_example.sh"
run = "srun -t 4 -p pdebug -o %s"%batch_output_file
run = "%s %s"%(run, runscript)
#args = "%s %s %i %s"%(MACHINE, code_dir, Nc, baryon_xml)
args = "%s"%(baryon_xml)
print("%s %s"%(run, args))
os.system("%s %s"%(run, args))





