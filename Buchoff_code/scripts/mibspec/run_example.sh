#!/bin/bash 

## sources intel compiler variables (don't change this)
export GCC_VERSION="8.3.1"
module load gcc/${GCC_VERSION}

NC=4

THREADS=8
GEOM_X=1
GEOM_Y=1
GEOM_Z=1
GEOM_T=1

MACHINE="RUBY"
#CODE_DIR="/p/lustre1/cushman2/old_su4_baryons/install/chroma-4c/bin"
#exe="${CODE_DIR}/chroma"

CODE_DIR="/p/lustre1/cushman2/old_su4_baryons/build/4c_baryons"
exe="${CODE_DIR}/chroma_4c_baryons"

XML_CONFIG=$1

echo "XML_CONFIG = ${XML_CONFIG}"
echo ""
echo ""


if [ $MACHINE == "LASSEN" ] 
then
    HOME="/p/gpfs1"
    run="jsrun --rs_per_host=32"
else
    HOME="/p/lustre1"
    run=""
fi 

geom="-geom ${GEOM_X} ${GEOM_Y} ${GEOM_Z} ${GEOM_T}" 
input="-i ${XML_CONFIG}"
outfile="-o ${XML_CONFIG}_start.out > ${XML_CONFIG}_start.stdout 2>&1"
runme="${run} ${exe} ${geom} ${input} ${outfile} < /dev/null"

##export QUDA_RESOURCE_PATH=${HOME}/QUDA_resources_Nc${NC}
##export LD_LIBRARY_PATH=${CODE_DIR}/base_install/libxml2/lib64:$LD_LIBRARY_PATH
##echo $LD_LIBRARY_PATH


echo "Before analysis: ", `date`
echo "RUN   : ${run} \n"
echo "EXE   : $exe\n"
echo "INPUT : $input\n"
echo "RUNME : $runme\n"  
time ${runme}  ## runs script here 
echo "After analysis: ", `date`

