#!  /bin/bash

set -x

date
echo "The JobID is " $SLURM_JOBID
echo "Starting at $(mydate)"

EXE=$HOME/binaries/lsd/nc=4/bgq/chroma_4c_polar
ll $EXE

export OMP_NUM_THREADS=4 
export BG_THREADLAYOUT=1

nodes=$SLURM_NNODES
tasks=$[nodes*${SLURM_TASKS_PER_NODE%(*}]
if (( nodes == 1 )); then tasks=1; fi

npx=1; npy=1; npz=1; npt=1; i=4

while (( npx*npy*npz*npt < tasks )); do
  case $i in 1 ) npx=$[npx*2];; 2 ) npy=$[npy*2];; 3 ) npz=$[npz*2];; 4 ) npt=$[npt*2];; esac
  i=$[((i+2)%4)+1]
done
if (( npx*npy*npz*npt != tasks )); then
  echo "exit - no good 'geom'"; exit
fi
geom="$npx $npy $npz $npt"

env

# run from .../[0-9] in order to do multiple jobs per directory

if pwd | grep -q "/[0-9]*$"; then
  ending=${PWD##*/}
  rsync ./"in.xml" ../ -au
  cd ..
else
  unset ending
fi

chroma_cleanup .

# create/modify input xml for each cfg

if [ ! -d cfg ] && [ -d ../../../cfg ]; then ln -s ../../../cfg ./cfg; fi

cfg=$(ls -rt ./cfg/ | grep lime | tail -1)
SaveInt=$(head -n50 ./cfg/$cfg | grep -a SaveInterval | sed -e "s/[^>]*>//; s/<.*//")

if (( SaveInt == 10 )); then cfgs=$(ls -rt ./cfg/ | grep "lime.*0$"); fi
if (( SaveInt == 50 )); then cfgs=$(ls -rt ./cfg/ | grep "lime.*[02468]$"); fi

if [ "$ending" ]; then
  cfgs=$(echo "$cfgs" | grep "$ending$")    # nb: 0's must be included in directory name
fi

for cfg in $cfgs; do
#{
  nx=$(pwd | sed -e "s|.*nx=||; s|[^0-9].*||")
  nt=$(pwd | sed -e "s|.*nt=||; s|[^0-9].*||")
  mq=$(pwd | sed -e "s|.*/m=||; s|[^0-9.].*||")
  kappa=$(pwd | sed -e "s|.*/kappa=||; s|[^0-9.].*||")

  # NB: do E=0 for testing only
  for field in 1.0 2.0 3.0; do
  #{
    # do measurements in 'tmp_meas'. push to 'meas' when finished (to avoid fu from cancellation)
    measDir=meas_polar/$cfg/E=$field
    tempDir=$measDir/tmp

    # check for tar file
    if [ -f $measDir.tar ]; then tar xf $measDir.tar; fi

    mkdir -p $measDir
    mkdir -p $tempDir

    # do 'n_meas' measurements, with src's distributed evenly in x,y *with z = t = 0*
    n_meas=20
    n_meas_x=4
    n_meas_y=$[n_meas/n_meas_x]

    # ignore following files (no longer computing)
    ignore="4u_Spin[01]"

    if qls $measDir/*SINK* | grep -q .; then
      n_now=$(wc -l $measDir/*SINK* | grep -v "$ignore" | ak 1 | sort -n | head -1)
    else
      n_now=0
    fi

    if (( n_now >= n_meas )); then continue; fi

    for (( n = n_now; n < n_meas; n++ )); do
    #{
      # remove contents of tempDir or quit if recent (aka problem)
      if find $tempDir -type f -mtime -1 | grep -q .; then
        break
      else
        RM -vf $tempDir/*
      fi

      # n = ix + iy * n_meas_x
      ix=$[n % n_meas_x]
      iy=$[n / n_meas_x]
      src_x=$[ix * (nx/n_meas_x)]
      src_y=$[iy * (nx/n_meas_y)]
      src_pos="$src_x $src_y 0 0"

      inxml_template="./in.xml"
      inxml="$tempDir/in.xml"

      outxml=$tempDir/out.xml

      cp $inxml_template $inxml

      sed -e "/<cfg_file>/  s|>[^<]*<|>$PWD/cfg/$cfg<|"   -i $inxml
      sed -e "/<nrow>/      s|>[^<]*<|>$nx $nx $nx $nt<|" -i $inxml
      sed -e "/<Mass>/      s|>[^<]*<|>$mq<|"             -i $inxml
      sed -e "/<Kappa>/     s|>[^<]*<|>$kappa<|"          -i $inxml

      sed -e "/<field_str>/ s|>[^<]*<|>$field<|"          -i $inxml

      sed -e "/<t_srce>/    s|>[^<]*<|>$src_pos<|"        -i $inxml

      ( cd $tempDir
        echo "$(mydate) starting $cfg" >> startstop
        srun -N $nodes -n $tasks $EXE -i ${inxml#$tempDir/} -o ${outxml#$tempDir/} -geom $geom
        echo "$(mydate) $cfg finished" >> startstop
      )

      # append tempDir results to measDir results and clear tempDir directory
      for tempFile in $tempDir/*; do
        # don't cat xml files
        if expr match "$tempFile" '.*\.xml$' >/dev/null; then continue; fi
        # do cat everything else (cat to temp and then mv back for even greater atomicity)
        saveFile=$measDir/${tempFile##*/}
        cat $saveFile $tempFile > $tempFile.new
        mv $tempFile.new $tempFile
      done

      mv $tempDir/* $measDir/
    #}
    done

    # clean up
    rmdir $tempDir
  #}
  done

  # DEBUG: uncomment to stop after 1 cfg
  #break
#}
done &

# only msub if still doing work
sleep 10
if find meas_polar/*/E*/tmp/out.xml -mmin -1 | grep -q .; then
  if [ "$ending" ]; then cd $ending; fi
  msub -l depend=$SLURM_JOBID runscript
fi

wait

echo "Finished at $(mydate)"

