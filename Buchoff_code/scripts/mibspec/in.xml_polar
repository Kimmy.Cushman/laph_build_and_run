<?xml version="1.0"?>
<chroma>
  <Param>
    <InlineMeasurements>

      <!-- make the shell source -->
      <elem>
        <Name>MAKE_SOURCE</Name>
        <Frequency>1</Frequency>
        <Param>
          <version>6</version>
          <Source>
            <version>2</version>
            <SourceType>SHELL_SOURCE</SourceType>
            <j_decay>3</j_decay>
            <t_srce>2 4 0 0</t_srce> <!-- Best to keep z=t=0 -->
            <SmearingParam>
              <wvf_kind>GAUGE_INV_GAUSSIAN</wvf_kind>
              <wvf_param>4.0</wvf_param>
              <wvfIntPar>70</wvfIntPar>
              <no_smear_dir>3</no_smear_dir>
            </SmearingParam>
            <Displacement>
              <version>1</version>
              <DisplacementType>NONE</DisplacementType>
            </Displacement>
          </Source>
        </Param>
        <NamedObject>
          <gauge_id>default_gauge_field</gauge_id>
          <source_id>shell_source</source_id>
        </NamedObject>
      </elem>

      <!-- make the (+) bg-phased gauge field -->
      <elem>
        <Name>BG_FIELD</Name>
        <Frequency>1</Frequency>
        <Param>
           <version>1</version>
           <charge>0.5</charge>
           <field_str>#field strength#</field_str>
           <field_norm>0.5</field_norm> <!-- Must be positive -->
        </Param>
        <NamedObject>
           <gauge_id>default_gauge_field</gauge_id>
           <bg_gauge_id>bg_gauge_field_plus</bg_gauge_id>
        </NamedObject>
      </elem>

      <!-- compute the (+) shell "half-prop" -->
      <elem>
	<Name>PROPAGATOR</Name>
        <Frequency>1</Frequency>
        <Param>
          <version>10</version>
          <quarkSpinType>FULL</quarkSpinType>
          <obsvP>true</obsvP>
          <numRetries>1</numRetries>
          <FermionAction>
            <FermAct>WILSON</FermAct>
            <Kappa>#quark kappa#</Kappa>
            <FermionBC>
              <FermBC>SIMPLE_FERMBC</FermBC>
              <boundary>1 1 1 -1</boundary>
            </FermionBC>
          </FermionAction>
          <InvertParam>
            <invType>CG_INVERTER</invType>
            <RsdCG>1.0e-12</RsdCG>
            <MaxCG>1000000</MaxCG>
          </InvertParam>
        </Param>
        <NamedObject>
          <gauge_id>bg_gauge_field_plus</gauge_id>
          <source_id>shell_source</source_id>
          <prop_id>shell_prop_bg_plus</prop_id>
        </NamedObject>
      </elem>

      <!-- erase the (+) bg gauge field -->
      <elem>
        <Name>ERASE_NAMED_OBJECT</Name>
        <NamedObject>
          <object_id>bg_gauge_field_plus</object_id>
        </NamedObject>
        <Frequency>1</Frequency>
      </elem>

      <!-- make the (-) bg-phased gauge field -->
      <elem>
        <Name>BG_FIELD</Name>
        <Frequency>1</Frequency>
        <Param>
           <version>1</version>
           <charge>-0.5</charge>
           <field_str>#field strength#</field_str>
      	    <field_norm>0.5</field_norm> <!-- Must be positive -->
        </Param>
        <NamedObject>
           <gauge_id>default_gauge_field</gauge_id>
           <bg_gauge_id>bg_gauge_field_minus</bg_gauge_id>
        </NamedObject>
      </elem>

      <!-- DEBUG: save "minus" field -->
<!--
      <elem>
        <Name>QIO_WRITE_NAMED_OBJECT</Name>
        <Frequency>1</Frequency>
        <NamedObject>
          <object_id>bg_gauge_field_minus</object_id>
          <object_type>Multi1dLatticeColorMatrix</object_type>
        </NamedObject>
        <File>
          <file_name>#"minus" bg field file#</file_name>
          <file_volfmt>SINGLEFILE</file_volfmt>
        </File>
      </elem>
-->

      <!-- compute the (-) shell "half-prop" -->
      <elem>
	<Name>PROPAGATOR</Name>
        <Frequency>1</Frequency>
        <Param>
          <version>10</version>
          <quarkSpinType>FULL</quarkSpinType>
          <obsvP>true</obsvP>
          <numRetries>1</numRetries>
          <FermionAction>
            <FermAct>WILSON</FermAct>
            <Kappa>#quark kappa#</Kappa>
            <FermionBC>
              <FermBC>SIMPLE_FERMBC</FermBC>
              <boundary>1 1 1 -1</boundary>
            </FermionBC>
          </FermionAction>
          <InvertParam>
            <invType>CG_INVERTER</invType>
            <RsdCG>1.0e-12</RsdCG>
            <MaxCG>1000000</MaxCG>
          </InvertParam>
        </Param>
        <NamedObject>
          <gauge_id>bg_gauge_field_minus</gauge_id>
          <source_id>shell_source</source_id>
          <prop_id>shell_prop_bg_minus</prop_id>
        </NamedObject>
      </elem>

      <!-- erase the (-) bg gauge field -->
      <elem>
        <Name>ERASE_NAMED_OBJECT</Name>
        <NamedObject>
          <object_id>bg_gauge_field_minus</object_id>
        </NamedObject>
        <Frequency>1</Frequency>
      </elem>

      <!-- erase the shell source -->
      <elem>
        <Name>ERASE_NAMED_OBJECT</Name>
        <NamedObject>
          <object_id>shell_source</object_id>
        </NamedObject>
        <Frequency>1</Frequency>
      </elem>

      <!-- point-sink the (+) shell "half-prop" -->
      <elem>
	<Name>SINK_SMEAR</Name>
        <Frequency>1</Frequency>
        <Param>
          <version>5</version>
          <Sink>
            <version>2</version>
            <SinkType>POINT_SINK</SinkType>
            <j_decay>3</j_decay>
          </Sink>
        </Param>
        <NamedObject>
          <gauge_id>default_gauge_field</gauge_id>
          <prop_id>shell_prop_bg_plus</prop_id>
          <smeared_prop_id>shell_point_prop_bg_plus</smeared_prop_id>
        </NamedObject>
      </elem>

      <!-- erase the (+) shell "half-prop" -->
      <elem>
        <Name>ERASE_NAMED_OBJECT</Name>
        <NamedObject>
          <object_id>shell_prop_bg_plus</object_id>
        </NamedObject>
        <Frequency>1</Frequency>
      </elem>

      <!-- point-sink the (-) shell "half-prop" -->
      <elem>
	<Name>SINK_SMEAR</Name>
        <Frequency>1</Frequency>
        <Param>
          <version>5</version>
          <Sink>
            <version>2</version>
            <SinkType>POINT_SINK</SinkType>
            <j_decay>3</j_decay>
          </Sink>
        </Param>
        <NamedObject>
          <gauge_id>default_gauge_field</gauge_id>
          <prop_id>shell_prop_bg_minus</prop_id>
          <smeared_prop_id>shell_point_prop_bg_minus</smeared_prop_id>
        </NamedObject>
      </elem>

<!-- shell-shell would require Gauge invariant sink not in Chroma.  Not written, so this is commented
      <elem>
        <Name>SINK_SMEAR</Name>
        <Frequency>1</Frequency>
        <Param>
          <version>5</version>
          <Sink>
            <version>2</version>
            <SinkType>SHELL_SINK</SinkType>
            <j_decay>3</j_decay>
            <SmearingParam>
              <wvf_kind>GAUGE_INV_GAUSSIAN</wvf_kind>
              <wvf_param>4.0</wvf_param>
              <wvfIntPar>70</wvfIntPar>
              <no_smear_dir>3</no_smear_dir>
            </SmearingParam>
            <Displacement>
              <version>1</version>
              <DisplacementType>NONE</DisplacementType>
            </Displacement>
          </Sink>
        </Param>
        <NamedObject>
          <gauge_id>default_gauge_field</gauge_id>
          <prop_id>shell_prop</prop_id>
          <smeared_prop_id>shell_shell_sprop</smeared_prop_id>
        </NamedObject>
      </elem>
-->

      <!-- erase the (-) shell "half-prop" -->
      <elem>
        <Name>ERASE_NAMED_OBJECT</Name>
        <NamedObject>
          <object_id>shell_prop_bg_minus</object_id>
        </NamedObject>
        <Frequency>1</Frequency>
      </elem>

      <!-- two-flavor neutral baryon correlator -->
      <elem>
        <Name>SU4_BARYON_TWO_FLAVOR_TWO_PROP</Name>
        <Frequency>1</Frequency>
        <Param>
           <output_dir>./</output_dir>  <!-- NB: run in appropriate dir! -->
           <prop1>shell_point_prop_bg_plus</prop1>
           <prop2>shell_point_prop_bg_minus</prop2>
           <Baryon_Spin>0</Baryon_Spin>
        </Param>
      </elem>

      <!-- erase the (+) shell-point prop -->
      <elem>
        <Name>ERASE_NAMED_OBJECT</Name>
        <NamedObject>
          <object_id>shell_point_prop_bg_plus</object_id>
        </NamedObject>
        <Frequency>1</Frequency>
      </elem>

      <!-- erase the (-) shell-point prop -->
      <elem>
        <Name>ERASE_NAMED_OBJECT</Name>
        <NamedObject>
          <object_id>shell_point_prop_bg_minus</object_id>
        </NamedObject>
        <Frequency>1</Frequency>
      </elem>

    </InlineMeasurements>
    <nrow>#volume#</nrow>
  </Param>
  <Cfg>
    <cfg_type>SZINQIO</cfg_type>
    <cfg_file>#cfg file#</cfg_file>
  </Cfg>
</chroma>

