#!  /bin/bash

#   make snapshot -> $PWD/save/$(mydate -d)

set -x
set -e

home=$HOME/projects/lsd/nc=4

rsync $home/scripts/ ./scripts --exclude ".*.sw*" -auv

rsync README scripts src save/last/ --exclude ".*.sw*" --delete-excluded --delete-after -av

mydate=$(mydate -d)

mv save/last save/$mydate

if ! (cd save; tar jcf $mydate{.tbz,}); then echo "tar failed"; fi

mv save/$mydate save/last

rsync README scripts save $home/ --exclude last -auv

