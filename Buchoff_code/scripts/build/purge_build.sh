#! /bin/bash
source ./scripts/build/env.sh

pushd ${BUILDDIR}
echo PURGING BUILD DIR
rm -rf build_chroma ./build_qdp++ ./build_qmp ./build_libxml2
popd
