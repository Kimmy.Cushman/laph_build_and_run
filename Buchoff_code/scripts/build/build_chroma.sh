#!/bin/bash
#
#################
# BUILD Chroma
#################
source ./scripts/build/env.sh

pushd ${SRCDIR}/chroma
aclocal; automake; autoconf
popd

pushd ${BUILDDIR}

if [ -d ./build_chroma ]; 
then 
  rm -rf ./build_chroma
fi

mkdir  ./build_chroma
cd ./build_chroma


${SRCDIR}/chroma/configure --prefix=${INSTALLDIR}/chroma \
	--with-qdp=${INSTALLDIR}/qdp++ \
        ${OMPENABLE} \
        --with-qmp=${INSTALLDIR}/qmp \
	--enable-cpp-wilson-dslash \
        CC="${PK_CC}"  CXX="${PK_CXX}" \
	CXXFLAGS="" CFLAGS="" \
	AR="${PK_AR}" RANLIB="${PK_RANLIB}" \
        --enable-c-scalarsite-bicgstab-kernels \
	--host=powerpc64-bgq-linux --build=none
${MAKE}
${MAKE} install

popd
