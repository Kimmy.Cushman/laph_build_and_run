##### 
# SET UP ENVIRONMENT
#

### DIRECTORIES
#export PATH=/bgsys/drivers/ppcfloor/gnu-linux/bin:$PATH
#export PATH=/bgsys/drivers/ppcfloor/comm/gcc/bin:$PATH

# The directory containing the build scripts, this script and the src/ tree
TOPDIR=`pwd`

# Install directory
INSTALLDIR=${TOPDIR}/install

# Source directory
SRCDIR=${TOPDIR}/src

# Build directory
BUILDDIR=${TOPDIR}/build


### OpenMP
OMPFLAGS="-fopenmp"
OMPENABLE="--enable-openmp"

### COMPILER FLAGS
PK_CXXFLAGS=${OMPFLAGS}" -O3 -finline-limit=50000 -fargument-noalias-global "
PK_CFLAGS=${OMPFLAGS}" -O3 -fargument-noalias-global -std=c99 "

### Make
MAKE="make -j 12"

### MPI
PK_CC=mpicc
PK_CXX=mpicxx
#PK_AR=powerpc64-bgq-linux-ar
#PK_RANLIB=powerpc64-bgq-linux-ranlib
