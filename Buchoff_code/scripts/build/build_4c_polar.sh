#!  /bin/bash

echo $0 starting

source ./scripts/build/env.sh

pushd ${BUILDDIR}

if [ -d ./4c_polar ]; then rm -rf ./4c_polar; fi

cp -vrip ${SRCDIR}/4c_polar ./

pushd 4c_polar

sed -e "s|^CHROMA=.*|CHROMA=${INSTALLDIR}/chroma-4c|" -i Makefile

${MAKE}

popd

popd

ls -lh ${BUILDDIR}/4c_polar/chroma_4c_polar

echo $0 finished


