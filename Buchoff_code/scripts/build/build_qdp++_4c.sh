#!/bin/bash
#
#################
# BUILD QMP
#################
source ./scripts/build/env.sh

pushd ${SRCDIR}/qdp++
aclocal; automake; autoconf
popd

pushd ${BUILDDIR}

if [ -d ./build_qdp++_4c ]; 
then 
  rm -rf ./build_qdp++_4c
fi

mkdir  ./build_qdp++_4c
cd ./build_qdp++_4c


${SRCDIR}/qdp++/configure \
	--prefix=${INSTALLDIR}/qdp++-4c \
	--with-libxml2=${INSTALLDIR}/libxml2 \
	--with-qmp=${INSTALLDIR}/qmp \
	--enable-Nc=4 \
	--enable-precision=double \
        --enable-parallel-arch=parscalar \
	--enable-db-lite \
	--enable-largefile \
	--enable-parallel-io \
	--enable-dml-output-buffering \
	--enable-alignment=128 \
	CXXFLAGS="${PK_CXXFLAGS}" \
	CFLAGS="${PK_CFLAGS}" \
	CXX="${PK_CXX}" \
	CC="${PK_CC}" \
	--build=none \
	${OMPENABLE}

${MAKE}
${MAKE} install

popd
