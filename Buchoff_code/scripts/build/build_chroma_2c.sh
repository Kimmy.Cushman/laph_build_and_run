#!/bin/bash
#
#################
# BUILD Chroma
#################
source ./scripts/build/env.sh

pushd ${SRCDIR}/chroma
aclocal; automake; autoconf
popd

pushd ${BUILDDIR}

if [ -d ./build_chroma_2c ]; 
then 
  rm -rf ./build_chroma_2c
fi

mkdir  ./build_chroma_2c
cd ./build_chroma_2c


${SRCDIR}/chroma/configure --prefix=${INSTALLDIR}/chroma-2c \
	--with-qdp=${INSTALLDIR}/qdp++-2c \
        ${OMPENABLE} \
        --with-qmp=${INSTALLDIR}/qmp \
        CC="${PK_CC}"  CXX="${PK_CXX}" \
	CXXFLAGS="" CFLAGS="" \
	AR="${PK_AR}" RANLIB="${PK_RANLIB}" \
        --enable-c-scalarsite-bicgstab-kernels \
	--host=powerpc64-bgq-linux --build=none
${MAKE}
${MAKE} install

popd
