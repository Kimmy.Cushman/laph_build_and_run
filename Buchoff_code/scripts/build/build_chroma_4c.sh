#!/bin/bash
#
#################
# BUILD Chroma
#################
source ./scripts/build/env.sh

pushd ${SRCDIR}/chroma
aclocal; automake; autoconf
popd

pushd ${BUILDDIR}

if [ -d ./build_chroma_4c ]; 
then 
  rm -rf ./build_chroma_4c
fi

mkdir  ./build_chroma_4c
cd ./build_chroma_4c


${SRCDIR}/chroma/configure --prefix=${INSTALLDIR}/chroma-4c \
	--with-qdp=${INSTALLDIR}/qdp++-4c \
        ${OMPENABLE} \
        --with-qmp=${INSTALLDIR}/qmp \
        CC="${PK_CC}"  CXX="${PK_CXX}" \
	CXXFLAGS="" CFLAGS="" \
	--build=none
${MAKE}
${MAKE} install

popd
