#!  /bin/bash

echo $0 starting

source ./scripts/build/env.sh

pushd ${BUILDDIR}

if [ -d ./4c_baryons ]; then rm -rf ./4c_baryons; fi

cp -vrip ${SRCDIR}/4c_baryons ./

pushd 4c_baryons

sed -e "s|^CHROMA=.*|CHROMA=${INSTALLDIR}/chroma-4c|" -i Makefile

${MAKE}

popd

popd

ls -lh ${BUILDDIR}/4c_baryons/chroma_4c_baryons

echo $0 finished

