#!/bin/bash
#
#################
# BUILD QMP
#################
source ./scripts/build/env.sh

pushd ${SRCDIR}/qdp++
aclocal; automake; autoconf
popd

pushd ${BUILDDIR}

if [ -d ./build_qdp++ ]; 
then 
  rm -rf ./build_qdp++
fi

mkdir  ./build_qdp++
cd ./build_qdp++


${SRCDIR}/qdp++/configure \
	--prefix=${INSTALLDIR}/qdp++ \
	--with-libxml2=${INSTALLDIR}/libxml2 \
	--with-qmp=${INSTALLDIR}/qmp \
	--enable-precision=double \
        --enable-parallel-arch=parscalar \
	--enable-db-lite \
	--enable-largefile \
	--enable-parallel-io \
	--enable-dml-output-buffering \
	--enable-alignment=128 \
	CXXFLAGS="${PK_CXXFLAGS}" \
	CFLAGS="${PK_CFLAGS}" \
	CXX="${PK_CXX}" \
	CC="${PK_CC}" \
	AR="${PL_AR}" \
	RANLIB="${PK_RANLIB}" \
	--host=powerpc64-bgq-linux --build=none \
	${OMPENABLE}

${MAKE}
${MAKE} install

popd
