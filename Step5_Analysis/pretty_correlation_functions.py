import numpy as np
import os
import sys

N_start =105
N_stop = 1000
N_step = 5
Lx = 16
Nt = 2*Lx
source = "POINT"

kappa_cfg = "QUENCHED"

beta_list = ["11p028", "11p5", "12p0"]
kappa_list = ["0p1554", "0p1515", "0p1475"]

state_list = ["pion", "vector"]
for com in [3,4]:
    for spin in [0,1,2]:
        state_list.append("bary_2u2d_com%i_Spin%i"%(com, spin))



home = "/p/lustre1/cushman2/laph_build_and_run"
for beta, kappa in zip(beta_list, kappa_list):
    for i in np.arange(N_start,N_stop, N_step):
        corr_dir = "%s/Production/L%iT%i/beta%s/kappa%s/cfg_%i"%(home, Lx, Nt, beta, kappa_cfg, i)
        for state in state_list:
            real_file = "%s/%s_%s_SOURCE_POINT_SINK_kimmy"%(corr_dir, state, source)
            imag_file = "%s/%s_%s_SOURCE_POINT_SINK_kimmy_im"%(corr_dir, state, source)

            real_dat_open = open(real_file, "r")
            imag_dat_open = open(imag_file, "r")

            real_dat = real_dat_open.readlines()[0]
            imag_dat = imag_dat_open.readlines()[0]
            
            real_dat = [float(val) for val in real_dat.split(" ")[4:]]
            imag_dat = [float(val) for val in imag_dat.split(" ")[4:]]

            real_dat_open.close()
            imag_dat_open.close()
            
            with open("%s.txt"%real_file, "w+") as f:
                for t in range(Nt):
                    f.write("%0.5e, %0.5e \n"%(real_dat[t], imag_dat[t]))
            f.close()
            #print("%s.txt"%real_file) 
            #np.savetxt("%s.txt"%real_file, real_dat)
            #np.savetxt("%s.txt"%imag_file, imag_dat)    
            
