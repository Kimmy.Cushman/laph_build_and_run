import numpy as np
import os
import sys
import contextlib

N_start = 0
N_stop = 200
Lx = 8
Nt = 2*Lx
beta = "WEAK"
kappa = "WEAK"
N_boot = 1000

which = sys.argv[1] # buchoff or me 
Nvec = int(sys.argv[2])
state = sys.argv[3]

if which == "buchoff":
    num_ops = 2
else:
    num_ops = 1


def boot_errors(data, N_boot):
    N = len(data)
    sets = np.zeros((N_boot, N))
    for i in range(N_boot):
        sets[i] = np.random.choice(data, size=N, replace=True)
    means = np.mean(sets, axis=1)
    mean = np.mean(means)
    sd = np.std(means)
    return mean, sd



corr_real = np.zeros((num_ops,Nt,N_stop - N_start))
corr_imag = np.zeros((num_ops,Nt,N_stop - N_start))


home = "/p/lustre1/cushman2/laph_build_and_run"
kappa_dir = "%s/Production/L%iT%i/beta%s/kappa%s"%(home, Lx, Nt, beta, kappa)
for i in np.arange(N_start,N_stop):

    print("cfg %i"%i)
    corr_dir = "%s/cfg_%i"%(kappa_dir, i)

    if which == "buchoff":
        for c,com in enumerate([3,4]):
            real_file = "%s/bary_2u2d_com%i_Spin0_SHELL_SOURCE_POINT_SINK_kimmy.txt"%(corr_dir, com)
            imag_file = "%s/bary_2u2d_com%i_Spin0_SHELL_SOURCE_POINT_SINK_kimmy_im.txt"%(corr_dir, com)
                
            real_dat = np.loadtxt(real_file)
            imag_dat = np.loadtxt(imag_file)
            if i==0:
                print(real_file)
                print(real_dat)
            for t in range(Nt):
                corr_real[c,t,i] = real_dat[t]
                corr_imag[c,t,i] = imag_dat[t]
        
    else:
        real_file = "%s/baryon_%s_corr_%ivecs_re.txt"%(corr_dir,state,Nvec)
        imag_file = "%s/baryon_%s_corr_%ivecs_im.txt"%(corr_dir,state,Nvec)
            
        c = 0
        real_dat = np.loadtxt(real_file)
        imag_dat = np.loadtxt(imag_file)
        for t in range(Nt):
            corr_real[c,t,i] = real_dat[t]
            corr_imag[c,t,i] = imag_dat[t]


av_corr_real = np.mean(corr_real, axis=2)
av_corr_imag = np.mean(corr_imag, axis=2)

sd_corr_real = np.std(corr_real, axis=2)
sd_corr_imag = np.std(corr_imag, axis=2)

boot_av_corr_real = np.zeros((num_ops, Nt))
boot_av_corr_imag = np.zeros((num_ops, Nt))

boot_sd_corr_real = np.zeros((num_ops, Nt))
boot_sd_corr_imag = np.zeros((num_ops, Nt))

for c in range(num_ops):
    for t in range(Nt):
        boot_av_corr_real[c,t] = boot_errors(corr_real[c,t,:], N_boot)[0]
        boot_av_corr_imag[c,t] = boot_errors(corr_imag[c,t,:], N_boot)[0]
        boot_sd_corr_real[c,t] = boot_errors(corr_real[c,t,:], N_boot)[1]
        boot_sd_corr_imag[c,t] = boot_errors(corr_imag[c,t,:], N_boot)[1]
        
print(sd_corr_real)
print(boot_sd_corr_real)

savename_bases = []
if which == "buchoff":
    for c,com in enumerate([3,4]):
        savename_base = "%s/bary_2u2d_com%i_Spin0_SHELL_SOURCE_POINT_SINK"%(kappa_dir, com)
        savename_bases.append(savename_base)
        np.savetxt("%s_av_real.txt"%savename_base, av_corr_real[c])
        np.savetxt("%s_av_imag.txt"%savename_base, av_corr_imag[c])
        np.savetxt("%s_sd_real.txt"%savename_base, sd_corr_real[c])
        np.savetxt("%s_sd_imag.txt"%savename_base, sd_corr_imag[c])
        np.savetxt("%s_boot_sd_real.txt"%savename_base, boot_sd_corr_real[c])
        np.savetxt("%s_boot_sd_imag.txt"%savename_base, boot_sd_corr_imag[c])
else:
    savename_base = "%s/baryon_%s"%(kappa_dir,state)
    savename_bases.append(savename_base)
    c = 0
    np.savetxt("%s_av_real.txt"%savename_base, av_corr_real[c])
    np.savetxt("%s_av_imag.txt"%savename_base, av_corr_imag[c])
    np.savetxt("%s_sd_real.txt"%savename_base, sd_corr_real[c])
    np.savetxt("%s_sd_imag.txt"%savename_base, sd_corr_imag[c])
    np.savetxt("%s_boot_sd_real.txt"%savename_base, boot_sd_corr_real[c])
    np.savetxt("%s_boot_sd_imag.txt"%savename_base, boot_sd_corr_imag[c])

@contextlib.contextmanager
def pushd(new_dir):
    previous_dir = os.getcwd()
    os.chdir(new_dir)
    try:
        yield 
    finally:
        os.chdir(previous_dir)


with pushd(kappa_dir):
    if which == "buchoff":
        state_label= "bary_2u2d_Spin0_SHELL_SOURCE_POINT_SINK"
    else:
        state_label = "baryon_%s_Nvec%i"%(state,Nvec)
    
    base_dir = "L%iT%i_beta%s_kappa%s_N%i"%(Lx, Nt, beta, kappa, N_stop - N_start)
    scp_dir = "%s_%s"%(base_dir, state_label)
    if os.path.isdir(scp_dir):
        os.system("rm -r %s"%scp_dir)
    os.system("mkdir %s"%scp_dir)
    for savename_base in savename_bases: 
        os.system("cp %s_av_real.txt %s"%(savename_base, scp_dir))
        os.system("cp %s_av_imag.txt %s"%(savename_base, scp_dir))
        os.system("cp %s_sd_real.txt %s"%(savename_base, scp_dir))
        os.system("cp %s_sd_imag.txt %s"%(savename_base, scp_dir))
        os.system("cp %s_boot_sd_real.txt %s"%(savename_base, scp_dir))
        os.system("cp %s_boot_sd_imag.txt %s"%(savename_base, scp_dir))
        
    os.system("tar -zcf %s.tar.gz %s"%(scp_dir, scp_dir))






##
