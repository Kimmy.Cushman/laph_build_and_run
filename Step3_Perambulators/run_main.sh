#!/bin/bash 

## sources intel compiler variables (don't change this)
#export GCC_VERSION="8.3.1"
#module load gcc/${GCC_VERSION}

#THREADS=$1
#GEOM_X=$2
#GEOM_Y=$3
#GEOM_Z=$4
#GEOM_T=$5

#MACHINE=$6
#CODE_DIR=$7
#NC=$8
#XML_CONFIG=$9
## srun parameters 
#shift 1
#TIME=$9
#shift 1
#QUEUE=$9
#shift 1
#BATCH_OUTPUT_FILE=$9
#shift 1
#TASKS=$9
#shift 1
#NODES=$9
source /p/lustre1/cushman2/laph_build_and_run/Production/run_Step3_perams.sh
echo"" 


#if [ $MACHINE == "LASSEN" ] 
#then
#    HOME="/p/gpfs1"
#    echo "fix me: jsrun + time + queue + output + etc"
#    break 
#    run="jsrun --rs_per_host=32"
#else
#    HOME="/p/lustre1"
#    run="srun -t ${TIME} -p ${QUEUE} -o ${BATCH_OUTPUT_FILE} -n ${TASKS} -N ${NODES}"
#fi 

HOME="/p/lustre1"
run="srun -t ${time_job} -p ${QUEUE} -o ${batch_output} -n ${tasks} -N ${nodes}"
exe="${code_dir}/Nc${NC}_Nd4_install/chroma_Nc/bin/chroma"
geom="-geom ${geom_x} ${geom_y} ${geom_z} ${geom_t}" 
input="-i ${xml_cfg_input}"
outfile="-o ${xml_cfg_input}_start.out > ${xml_cfg_input}_start.stdout 2>&1"
runme="${run} ${exe} ${geom} ${input} ${outfile} < /dev/null"
#exe="${CODE_DIR}/Nc${NC}_Nd4_install/chroma_Nc/bin/chroma"
#geom="-geom ${GEOM_X} ${GEOM_Y} ${GEOM_Z} ${GEOM_T}" 
#input="-i ${XML_CONFIG}"
#outfile="-o ${XML_CONFIG}_start.out > ${XML_CONFIG}_start.stdout 2>&1"
#runme="${run} ${exe} ${geom} ${input} ${outfile} < /dev/null"

export QUDA_RESOURCE_PATH=${HOME}/QUDA_resources_Nc${NC}
#export LD_LIBRARY_PATH=${CODE_DIR}/base_install/libxml2/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=${code_dir}/base_install/libxml2/lib64:$LD_LIBRARY_PATH
echo $LD_LIBRARY_PATH


echo "Before analysis: ", `date`
echo "RUN   : ${run} \n"
echo "EXE   : $exe\n"
echo "INPUT : $input\n"
echo "RUNME : $runme\n"  
time ${runme}  ## runs script here 
echo "After analysis: ", `date`

