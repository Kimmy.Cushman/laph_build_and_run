import numpy as np
import os
import sys 



# physical params 
Nc = int(sys.argv[1])
Lx = int(sys.argv[2])
Nt = int(sys.argv[3])
# preset params 
Nvec_peram = int(sys.argv[4])
kappa = sys.argv[5]  # ex. "0p1250"
mass_or_kappa = sys.argv[6]
action = sys.argv[7] # CLOVER or WILSON
config_type = sys.argv[8]
Step3_dir = sys.argv[9]
# config params 
peram_sdb_output = sys.argv[10]    
config_file = sys.argv[11]
xml_config = sys.argv[12]
vecs_filename = sys.argv[13]


xml_base = "%s/cfg_xmls/distillation_base.xml"%Step3_dir
# start fresh 
try:
    os.system("rm %s"%xml_config)
except Exception as e:
    pass 
base_file = open(xml_base, "r")
xml_config_file = open(xml_config, "w")

lines = base_file.readlines()
for line in lines:
    # mass label
    if "<mass_label></mass_label>" in line:
        pre,post = line.split("><")
        if mass_or_kappa == "mass":
            xml_config_file.write(pre + ">" + "m" + kappa + "<" + post)
        else:  # mass_or_kappa == "kappa":
            xml_config_file.write(pre + ">" + "K" + kappa + "<" + post)
    
    # kappa/mass value 
    elif "<Kappa></Kappa>" in line:
        pre = line.split("K")[0] # get blank space and "<"
        value = "0." + kappa.split("p")[1]
        if mass_or_kappa == "mass":
            xml_config_file.write(pre + "Mass>" + value + "</Mass>\n")
        if mass_or_kappa == "kappa":
            xml_config_file.write(pre + "Kappa>" + value + "</Kappa>\n")
    
    # mass label
    elif "<FermAct></FermAct>" in line:
        pre,post = line.split("><")
        xml_config_file.write(pre + ">" + action + "<" + post)
 
    # Nt 
    elif "<Nt_forward></Nt_forward>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + str(Nt) + "<" + post) 
    
    # Nvec 
    elif "<num_vecs></num_vecs>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + str(Nvec_peram) + "<" + post) 
    
    # Evecs 
    elif "<elem></elem>" in line:
        pre, post = line.split("><")
        for t in range(Nt):
            xml_config_file.write(pre + ">" + "%st%i.mod"%(vecs_filename, t) + "<" + post)
    
    # permabulator output 
    elif "<prop_op_file></prop_op_file>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + peram_sdb_output + "<" + post)
    
    # Latt size 
    elif "<nrow></nrow>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
    
    # input 
    elif "<cfg_file></cfg_file>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config_file + "<" + post)
    
    elif "<cfg_type></cfg_type>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config_type + "<" + post)
    
    else:
        xml_config_file.write(line)

base_file.close()
xml_config_file.close()


