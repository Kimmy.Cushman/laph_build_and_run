import numpy as np
import os
import sys 



# physical params 
Nc = int(sys.argv[1])
Lx = int(sys.argv[2])
Nt = int(sys.argv[3])
Nvec = int(sys.argv[4])
Nnoise = int(sys.argv[5])
run_type = sys.argv[6]
kappa = sys.argv[7]  # ex. "0p1250"
mass_or_kappa = sys.argv[8]
action = sys.argv[9] # CLOVER or WILSON


# config params 
output = sys.argv[10]    
direc = sys.argv[11]
config_file = sys.argv[12]
xml_config = sys.argv[13]
Step3_dir = sys.argv[14]
LapH_vecs_filename = sys.argv[15]
sLapH_vecs_filename = sys.argv[16]
quark_line_type = sys.argv[17]
config_type = sys.argv[18]

if run_type == "distillation":
    xml_base = "%s/cfg_xmls/distillation_base.xml"%Step3_dir
    # start fresh 
    try:
        os.system("rm %s"%xml_config)
    except Exception as e:
        pass 
    base_file = open(xml_base, "r")
    xml_config_file = open(xml_config, "w")

    lines = base_file.readlines()
    for line in lines:
        # mass label
        if "<mass_label></mass_label>" in line:
            pre,post = line.split("><")
            if mass_or_kappa == "mass":
                xml_config_file.write(pre + ">" + "m" + kappa + "<" + post)
            else:  # mass_or_kappa == "kappa":
                xml_config_file.write(pre + ">" + "K" + kappa + "<" + post)
        
        # kappa/mass value 
        elif "<Kappa></Kappa>" in line:
            pre = line.split("K")[0] # get blank space and "<"
            value = "0." + kappa.split("p")[1]
            if mass_or_kappa == "mass":
                xml_config_file.write(pre + "Mass>" + value + "</Mass>\n")
            if mass_or_kappa == "kappa":
                xml_config_file.write(pre + "Kappa>" + value + "</Kappa>\n")
        
        # mass label
        elif "<FermAct></FermAct>" in line:
            pre,post = line.split("><")
            xml_config_file.write(pre + ">" + action + "<" + post)
     
        # Nt 
        elif "<Nt_forward></Nt_forward>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + str(Nt) + "<" + post) 
        
        # Nvec 
        elif "<num_vecs></num_vecs>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + str(Nvec) + "<" + post) 
        
        # Evecs 
        elif "<elem></elem>" in line:
            pre, post = line.split("><")
            # LapH_LapH -> all LapH vecs
            # LapH_sLapH -> t=0 sLapH, t>0 LapH
            # sLapH_sLapH -> all sLapH
            if quark_line_type == "LapH_LapH": 
                for t in range(Nt):
                    xml_config_file.write(pre + ">" + "%st%i.mod"%(LapH_vecs_filename, t) + "<" + post)
            elif quark_line_type == "LapH_sLapH":
                t = 0 # sLapH
                xml_config_file.write(pre + ">" + "%st%i.mod"%(sLapH_vecs_filename, t) + "<" + post)
                for t in range(1,Nt): # LapH
                    xml_config_file.write(pre + ">" + "%st%i.mod"%(LapH_vecs_filename, t) + "<" + post)
            
            else: # quark_line_type == "sLaph_sLapH"
                for t in range(Nt):
                    xml_config_file.write(pre + ">" + "%st%i.mod"%(sLapH_vecs_filename, t) + "<" + post)
        # output 
        elif "<prop_op_file></prop_op_file>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + output + "<" + post)
        
        # Latt size 
        elif "<nrow></nrow>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
        
        # input 
        elif "<cfg_file></cfg_file>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + config_file + "<" + post)
        
        elif "<cfg_type></cfg_type>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + config_type + "<" + post)
        
        else:
            ##print(line)
            #if "<mass_label></mass_label>" in line:
            #    print("GOTCHA!")
            #    print(line)
            xml_config_file.write(line)

    base_file.close()
    xml_config_file.close()




if run_type == "get_random":
    xml_base = "%s/cfg_xmls/weak_field_base.xml"%Step3_dir
    os.system("rm %s"%xml_config)
    print(xml_config)
    base_file = open(xml_base, "r")
    xml_config_file = open(xml_config, "w")

    lines = base_file.readlines()
    for line in lines:
        
        # output 
        if "<file_name></file_name>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + output + "<" + post) 
        
        # Latt size 
        elif "<nrow></nrow>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
        
        # random seed 
        elif "<elem></elem>" in line:
            pre, post = line.split("><")
            r1, r2, r3, r4 = np.random.randint(999,size=4)
            xml_config_file.write(pre + ">" + r1 + "<" + post) 
            xml_config_file.write(pre + ">" + r2 + "<" + post) 
            xml_config_file.write(pre + ">" + r3 + "<" + post) 
            xml_config_file.write(pre + ">" + r4 + "<" + post) 
            
        else:
            xml_config_file.write(line)

    base_file.close()
    xml_config_file.close()
        




if run_type == "meson":
    xml_base = "%s/cfg_xmls/meson_corr_orig_base.xml"%Step3_dir
    os.system("rm %s"%xml_config)
    print(xml_config)
    base_file = open(xml_base, "r")
    xml_config_file = open(xml_config, "w")

    lines = base_file.readlines()
    for line in lines:

        # output 
        if "<xml_file></xml_file>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + output  + "<" + post)
    
        # Latt size 
        elif "<nrow></nrow>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
        
        # input 
        elif "<cfg_file></cfg_file>" in line:
            pre, post = line.split("><")
            xml_config_file.write(pre + ">" + direc + "/" + config_file + "<" + post)
        
        else:
            xml_config_file.write(line)

    base_file.close()
    xml_config_file.close()


