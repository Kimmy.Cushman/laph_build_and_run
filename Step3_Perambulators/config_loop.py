import numpy as np
import os
import sys
import time 

# execute here 
MACHINE = sys.argv[1]
Step3_dir = sys.argv[2]
exe = sys.argv[3] # no executable needed 

# physics
Nc = int(sys.argv[4])
Lx = int(sys.argv[5])
Nt = int(sys.argv[6])
Nvec = int(sys.argv[7]) 
Nnoise = int(sys.argv[8]) 

# job
time_job = int(sys.argv[9])
queue = sys.argv[10]
group = sys.argv[11]

# resources
tasks = int(sys.argv[12])
nodes = int(sys.argv[13])
threads = int(sys.argv[14])

# geom 
geom_x = int(sys.argv[15])
geom_y = int(sys.argv[16])
geom_z = int(sys.argv[17])
geom_t = int(sys.argv[18])

# files 
result_dir = sys.argv[19]
config_file = sys.argv[20]
code_dir = sys.argv[21]
kappa_peram = sys.argv[22]
mass_or_kappa = sys.argv[23]
action = sys.argv[24]
LapH_vecs_filename = sys.argv[25]
sLapH_vecs_filename = sys.argv[26]
config_type = sys.argv[27]
output = sys.argv[28]
quark_line_type = sys.argv[29]


batch_output_file = "%s/perambulators_k%s_Nvec%i"%(result_dir, kappa_peram, Nvec)
xml_config = "%s/Perambulators_k%s_Nvec%i"%(result_dir, kappa_peram, Nvec)
# add Noise and quark line type info to file names 
if quark_line_type != "LapH_LapH":
    xml_config += "_Nnoise%i_Q%s"%(Nnoise, quark_line_type)
    batch_output_file += "_Nnoise%i_Q%s"%(Nnoise, quark_line_type)

xml_config += ".xml"
batch_output_file += "_n%i_N%i_thread%i_gx%i_gy%i_gz%i_RUBY.out"%(tasks, nodes, threads,
                                                             geom_x, geom_y, geom_z)
os.system("rm %s"%batch_output_file)
os.system("rm %s"%output)

# generate xml from base 
run_type = "distillation"
# edited xml config 
xml_args = "%i %i %i %i %i"%(Nc, Lx, Nt, Nvec, Nnoise) 
xml_args += " " + " ".join([run_type, kappa_peram, mass_or_kappa, action,
                            output, result_dir, config_file, xml_config, Step3_dir, 
                            LapH_vecs_filename, sLapH_vecs_filename, quark_line_type,
                            config_type])

os.system("python %s/cfg_xmls/write_xml.py %s"%(Step3_dir, xml_args))
print
print("xml_config written:")
print(xml_config)
print
#submit job
run_main = "sh %s/run_main.sh"%Step3_dir
bash_input = str(threads)
geom = "%i %i %i %i"%(geom_x, geom_y, geom_z, geom_t)
commandline_args = "%s %s %i %s"%(MACHINE, code_dir, Nc, xml_config)
srun_args = "%i %s %s %i %i"%(time_job, queue, batch_output_file, tasks, nodes)

run = " ".join([run_main, bash_input, geom, commandline_args, srun_args])

# submit job 
print("running from config loop python script")
print(run)
print
print("see output")
print(batch_output_file)
print
os.system(run)




