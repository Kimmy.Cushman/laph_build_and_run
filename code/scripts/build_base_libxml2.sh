#!/bin/bash
#
#################
# BUILD LIBXML2
#################
source ./env_base.sh

rm -rf ${SRCDIR}/libxml2
rm -rf ${BUILDDIR}/libxml2
rm -rf ${INSTALLDIR}/libxml2

pushd ${SRCDIR}
git clone https://gitlab.gnome.org/GNOME/libxml2.git
(cd libxml2 && ./autogen.sh)
popd 

mkdir -p ${BUILDDIR}/libxml2
pushd ${BUILDDIR}/libxml2

CMAKEFLAGS="-DCMAKE_INSTALL_PREFIX=${INSTALLDIR}/libxml2 -DLIBXML2_WITH_PYTHON=OFF"
cmake ${SRCDIR}/libxml2 ${CMAKEFLAGS}


#cp Makefile Makefile.bak
#sed -e 's/runsuite\$(EXEEXT)//' Makefile | sed -e 's/runtest\$(EXEEXT)\s\\//' > Makefile.new
#cp Makefile.new Makefile
${MAKE}
${MAKE} install

popd
