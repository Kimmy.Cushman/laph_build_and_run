#!/bin/bash
#
#################
# SOURCE EIGEN
#################
source ./env_base.sh

rm -rf ${SRCDIR}/eigen
if [-d ${SRCDIR}]; then
  echo ""
else
  mkdir ${SRCDIR}
fi

pushd ${SRCDIR}
git clone https://gitlab.com/libeigen/eigen.git 
popd 


