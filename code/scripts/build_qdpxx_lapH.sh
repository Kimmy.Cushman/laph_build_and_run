!/bin/bash
#
#################
# BUILD QDPXX
#################

N_COLORS=${1}
N_DIMS=${2}
PARALLEL=${3}

source ./env_Nc_Nd.sh ${N_COLORS} ${N_DIMS} ${PARALLEL}

new_clone="new"

if [ ${new_clone} = "new" ]; then 
  echo ""
  echo "deleting qdp source"
  echo ""
  rm -rf ${SRCDIR}/qdpxx_lapH
  pushd ${SRCDIR}
    git clone https://gitlab.com/Kimmy.Cushman/qdpxx_laph qdpxx_lapH
    (cd qdpxx_lapH; git submodule update --init --recursive; autoreconf -i -f) # commented out in tar on git 
  popd
else
  echo "already cloned and reconfigured"
fi 


rm -rf ${BUILDDIR}/qdpxx_lapH
rm -rf ${INSTALLDIR}/qdpxx_lapH

pushd ${BUILDDIR}
mkdir  ./qdpxx_lapH
pushd ./qdpxx_lapH


${SRCDIR}/qdpxx_lapH/configure \
	--prefix=${INSTALLDIR}/qdpxx_lapH \
        --enable-parallel-arch=${PARALLEL} \
	--enable-precision=double \
        --enable-Nd=${N_DIMS} \
        --enable-Nc=${N_COLORS} \
	--enable-filedb \
	--disable-generics \
        --enable-largefile \
        --enable-parallel-io \
        --enable-alignment=64 \
	--with-qmp=${BASE_INSTALLDIR}/qmp \
	--with-libxml2=${BASE_INSTALLDIR}/libxml2 \
	CXXFLAGS="${PK_CXXFLAGS} -fpermissive " \
	CFLAGS="${PK_CFLAGS}" \
	CXX="${PK_CXX}" \
	CC="${PK_CC}" \
	--host=${HOST} --build=none \
	${OMPENABLE}
${MAKE}
${MAKE} install

popd
popd
