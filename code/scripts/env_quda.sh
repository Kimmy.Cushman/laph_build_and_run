#!/bin/bash

source env_MACHINE.sh

export N_COLORS=${1}
Nd=${2}

#export CMAKE_BUILD_TYPE="Release"
#export LLVM_VERSION="13.0.0"
#export LLVM_CMAKE_DIR="${BASE_DIR}/install_llvm-${LLVM_VERSION}.src/lib/cmake/llvm"
#export QMP_CMAKE_DIR="${BASE_DIR}/install_qmp/lib/cmake/QMP"


TOPDIR=`pwd`
export SRCDIR=${TOPDIR}/../source
export BUILDDIR=${TOPDIR}/../Nc${N_COLORS}_Nd${Nd}_build
export INSTALLDIR=${TOPDIR}/../Nc${N_COLORS}_Nd${Nd}_install

# make source dir 
if [ ! -d ${SRCDIR} ]; then
  mkdir ${SRCDIR}
fi
# make build dir 
if [ ! -d ${BUILDDIR} ]; then
  mkdir ${BUILDDIR}
fi
# make install dir 
if [ ! -d ${INSTALLDIR} ]; then
  mkdir ${INSTALLDIR}
fi

