#!/bin/bash

###########
# BUILD QMP
###########

# Source the project environment for LASSEN
source ./env_base.sh

# remove any previous installation
rm -rf ${SRCDIR}/qmp
rm -rf ${BUILDDIR}/qmp
rm -rf ${INSTALLDIR}/qmp

pushd ${SRCDIR}
git clone https://github.com/usqcd-software/qmp.git qmp
# Get dependencies and autoconf
(cd qmp; git submodule update --init --recursive; autoreconf -i -f)
popd

pushd ${BUILDDIR}
mkdir  ./qmp
pushd ./qmp

# Configure using the relevant variables
${SRCDIR}/qmp/configure --prefix=${INSTALLDIR}/qmp \
    --with-qmp-comms-type=MPI \
    --with-qmp-comms-cflags="" \
    --with-qmp-comms-ldflags="" \
    --with-qmp-comms-libs="" \
    CC="${PK_CC}" \
    CFLAGS="${PK_CFLAGS}" \
    --host=${HOST} \
    --build=none

    
# Make QMP
${MAKE}

# Install QMP into the install directory specified in env.sh
${MAKE} install

# Return to the directory in which this file was executed
popd
popd
