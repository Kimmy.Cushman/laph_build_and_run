##### 
# SET UP ENVIRONMENT
#

source env_MACHINE.sh

N_COLORS=${1}
N_DIMS=${2}
PARALLEL=${3}

TOPDIR=`pwd`
SRCDIR=${TOPDIR}/../source
BASE_INSTALLDIR=${TOPDIR}/../base_install
BUILDDIR=${TOPDIR}/../Nc${N_COLORS}_Nd${N_DIMS}_${PARALLEL}_build
INSTALLDIR=${TOPDIR}/../Nc${N_COLORS}_Nd${N_DIMS}_${PARALLEL}_install

# make source dir 
if [ ! -d ${SRCDIR} ]; then
  mkdir ${SRCDIR}
fi
# make build dir 
if [ ! -d ${BUILDDIR} ]; then
  mkdir ${BUILDDIR}
fi
# make install dir 
if [ ! -d ${INSTALLDIR} ]; then
  mkdir ${INSTALLDIR}
fi



MAKE="make -j" 
