##### 
# SET UP ENVIRONMENT specific to machine 

# QUARTZ
export GCC_VERSION="10.3.1-magic"  # need 8.1.0 for base ?
export CUDA_VERSION="11.1.0"
MPI="openmpi/4.1.2"
MCPU="broadwell" # -- depreciated in gcc 8.1 
MTUNE="broadwell"
export CMAKE_VERSION="3.26.3" 


# LASSEN 
#export GCC_VERSION="8.3.1"  # need 7.3.1 for base?
#export CUDA_VERSION="11.1.1"
#MPI="spectrum-mpi/rolling-release"
#MCPU="power9" # -- depreciated in gcc 8.1
#MTUNE="power9" 
#export CMAKE_VERSION="3.23.1"


module load gcc/${GCC_VERSION}
module load ${MPI}
module load cmake/${CMAKE_VERSION}
module load cuda/${CUDA_VERSION}

export HOST="x86_64-linux-gnu" 
export PK_CC=mpicc
export PK_CXX=mpicxx

CSTD="gnu99"
CXXSTD="c++14"
OMPFLAGS="-fopenmp"
OMPENABLE="--enable-openmp"

# only needed for arpack 
export FC="gfortran"
export DCMAKE_FORTRAN_FLAGS="-fopenmp"
export DCMAKE_C_FLAGS="-fopenmp"
# only needed for chroma 
export C_OMPFLAGS="-fopenmp"
export CXX_OMPFLAGS="-fopenmp"
export LDFLAGS="-Wl,-zmuldefs"



# FULL COMPILER FLAGS                                                                                                                                                                  
export PK_CFLAGS=${OMPFLAGS}" -g -Ofast -std=${CSTD} -mcpu=${MCPU} -mtune=${MTUNE} "
export PK_CXXFLAGS=${OMPFLAGS}" -g -Ofast -std=${CXXSTD} -mcpu=${MCPU} -mtune=${MTUNE} "


# GCC Install Directory
GCC_HOME=/usr/tce/packages/gcc/gcc-${GCC_VERSION}
LD_LIBRARY_PATH=${GCC_HOME}/lib64:${GCC_HOME}/lib:${LD_LIBRARY_PATH}

