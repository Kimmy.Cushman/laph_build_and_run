#!/bin/bash
#
##############
# BUILD Chroma
##############

N_COLORS=${1}
N_DIMS=${2}
PARALLEL=${3}

source ./env_Nc_Nd.sh ${N_COLORS} ${N_DIMS} ${PARALLEL}

new_clone="new"
CHROMA_DIR=chroma_Nc

if [ ${new_clone} = "new" ]; then 
  rm -rf ${SRCDIR}/${CHROMA_DIR}
  pushd ${SRCDIR}
    git clone https://github.com/JeffersonLab/chroma.git ${CHROMA_DIR}
    (cd ${CHROMA_DIR}; git checkout hotfix/Nc)
    # Fix the submodule addresses
    #sed -i "s/git@github.com:/https:\/\/github.com\//" ${CHROMA_DIR}/.git/config
    # Recursive submodule update
    (cd ${CHROMA_DIR}; git submodule update --init --recursive; aclocal; autoconf -i -f; autoreconf -i -f; automake -i -f)
  popd
else
  echo "already cloned and reconfigured"
fi


# remove any previous installation 
rm -rf ${BUILDDIR}/${CHROMA_DIR}
rm -rf ${INSTALLDIR}/${CHROMA_DIR}

# Move to the build directory as specified in env.sh
pushd ${BUILDDIR}

# Make a CHROMA build directory, enter it.
mkdir  ./${CHROMA_DIR}
cd ./${CHROMA_DIR}

# Configure using the relevant variables
${SRCDIR}/${CHROMA_DIR}/configure --prefix=${INSTALLDIR}/${CHROMA_DIR} \
    --with-qdp=${INSTALLDIR}/qdpxx_lapH \
    --with-qmp=${BASE_INSTALLDIR}/qmp \
    CC="${PK_CC}"  CXX="${PK_CXX}" \
    CXXFLAGS=${CXX_OMPFLAGS} CFLAGS=${C_OMPFLAGS} \
    LDFLAGS=${LDFLAGS} \
    --host=${HOST} --build=none \
    --enable-clover \
    ${OMPENABLE} \
    --with-libxml2=${BASE_INSTALLDIR}/libxml2/lib64 \
    #--with-quda=${INSTALLDIR}/quda \
    #--with-cuda=${CUDA_HOME} \
    --enable-layout=cb2 
    --enable-static-packed-gauge \
    --enable-fused-clover-deriv-loops
    ##--with-mg-proto=${BASE_INSTALLDIR}/mg_proto

# Make CHROMA
${MAKE}

# Install CHROMA into the install directory specified in env.sh
${MAKE} install

# Return to the directory in which this file was executed
popd
