#!/bin/sh

#INSTALLDIR=`pwd`
source env_base.sh

rm -rf ${INSTALLDIR}/arpackpp 
rm -rf ${SRCDIR}/arpackpp

pushd ${SRCDIR}
git clone https://github.com/m-reuter/arpackpp.git
popd

mkdir -p ${INSTALLDIR}/arpackpp
pushd ${INSTALLDIR}/arpackpp

mkdir external
cd external

rm -rf OpenBLAS
git clone https://github.com/xianyi/OpenBLAS.git
cd OpenBLAS

make -j
popd 

pushd ${INSTALLDIR}/arpackpp
cd external

rm -rf arpack-ng
rm -rf arpack-ng-build 

git clone https://github.com/opencollab/arpack-ng.git

mkdir arpack-ng-build
cd arpack-ng-build
BLAS_LIB=${INSTALLDIR}/arpackpp/external/OpenBLAS/libopenblas.a
CMAKE_FLAGS="-DBLAS_openblas_LIBRARY=${BLAS_LIB} -DCMAKE_C_FLAGS=${DCMAKE_C_FLAGS} -DCMAKE_Fortran_FLAGS=${DCMAKE_FORTRAN_FLAGS} \
             -DCMAKE_INSTALL_PREFIX=${INSTALLDIR}/external/arpack-ng-build FC=gfortran"

cmake ${CMAKE_FLAGS} ../arpack-ng

make -j 
make -j install 
