##### 
# SET UP ENVIRONMENT
#
# Setup script for base build
#

source env_MACHINE.sh 


TOPDIR=`pwd`
INSTALLDIR=${TOPDIR}/../base_install

# Source directory
SRCDIR=${TOPDIR}/../source

# Build directory
BUILDDIR=${TOPDIR}/../base_build

MAKE="make -j" 
