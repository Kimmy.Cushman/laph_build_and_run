#!/bin/bash

#####################################
# BUILD SU(N) DISTILL STACK for c=3,4
#####################################

# ensure that you have set the desired TOPDIR path in env.sh
# before running this script, and that you have upacked the 
# source code for qdpxx_lapH


BUILD_BASE="NO"
BUILD_Nc3="YES"  # requires base
BUILD_Nc4="NO"   # requires base 


echo ""
echo "Time for coffee! (about 23 minutes)"
echo ""


LOG_DIR=Logs
mkdir -p ${LOG_DIR}
EIGEN_LOG=${LOG_DIR}/eigen.log 
ARPACK_LOG=${LOG_DIR}/arpack.log 
LIBXML_LOG=${LOG_DIR}/libxml.log
QMP_LOG=${LOG_DIR}/qmp.log

QDP33_SCALAR_LOG=${LOG_DIR}/qdp_Nc3_Nd3_scalar.log
QDP34_SCALAR_LOG=${LOG_DIR}/qdp_Nc3_Nd4_scalar.log
QDP43_SCALAR_LOG=${LOG_DIR}/qdp_Nc4_Nd3_scalar.log
QDP44_SCALAR_LOG=${LOG_DIR}/qdp_Nc4_Nd4_scalar.log

QDP33_PSCALAR_LOG=${LOG_DIR}/qdp_Nc3_Nd3_parscalar.log
QDP34_PSCALAR_LOG=${LOG_DIR}/qdp_Nc3_Nd4_parscalar.log
QDP43_PSCALAR_LOG=${LOG_DIR}/qdp_Nc4_Nd3_parscalar.log
QDP44_PSCALAR_LOG=${LOG_DIR}/qdp_Nc4_Nd4_parscalar.log

CHROMA34_LOG=${LOG_DIR}/chroma_Nc3_Nd4.log
CHROMA44_LOG=${LOG_DIR}/chroma_Nc4_Nd4.log
QUDA34_LOG=${LOG_DIR}/quda_Nc3_Nd4.log
QUDA44_LOG=${LOG_DIR}/quda_Nc4_Nd4.log
#
SUCCESS_EIGEN=../source/eigen/
SUCCESS_ARPACK=../base_install/arpackpp/external/arpack-ng-build/lib/libarpack.so
SUCCESS_LIBXML=../base_install/libxml2/lib64/libxml2.so.2.12.0
SUCCESS_QMP=../base_install/qmp/include/qmp.h

SUCCESS_SCALAR_QDP33=../Nc3_Nd3_scalar_install/qdpxx_lapH/include/
SUCCESS_SCALAR_QDP34=../Nc3_Nd4_scalar_install/qdpxx_lapH/include/
SUCCESS_SCALAR_QDP43=../Nc4_Nd3_scalar_install/qdpxx_lapH/include/
SUCCESS_SCALAR_QDP44=../Nc4_Nd4_scalar_install/qdpxx_lapH/include/

SUCCESS_PSCALAR_QDP33=../Nc3_Nd3_parscalar_install/qdpxx_lapH/include/
SUCCESS_PSCALAR_QDP34=../Nc3_Nd4_parscalar_install/qdpxx_lapH/include/
SUCCESS_PSCALAR_QDP43=../Nc4_Nd3_parscalar_install/qdpxx_lapH/include/
SUCCESS_PSCALAR_QDP44=../Nc4_Nd4_parscalar_install/qdpxx_lapH/include/

SUCCESS_CHROMA34=../Nc3_Nd4_parscalar_install/chroma_Nc/bin/chroma
SUCCESS_CHROMA44=../Nc4_Nd4_parscalar_install/chroma_Nc/bin/chroma
SUCCESS_QUDA34=../Nc3_Nd4_build/quda/lib/libquda.so 
SUCCESS_QUDA44=../Nc4_Nd4_build/quda/lib/libquda.so 


# run build script, and then check if it was successful
build_package() {
  build_it=$1
  log_file=$2
  build_script=$3
  inputNc=$4
  inputNd=$5
  input_parallel=$6
  success=$7
  
  echo ""
  if [ ${build_it} == "YES" ]; then
    echo "building into ${log_file}..."
    ./${build_script} ${inputNc} ${inputNd} ${input_parallel} >& ${log_file} &
    wait
  fi
  
  if [ -f ${success} ]; then 
    echo "DONE"	
  elif [ -d ${success} ]; then
    echo "DONE"
  else
    echo "ERROR! (sorry!)"
    exit 0 
  fi
}


# always check that base success, no input args needed here 
build_package ${BUILD_BASE} \
              ${EIGEN_LOG} \
              "build_base_eigen.sh" 0 0 "N/A" \
              ${SUCCESS_EIGEN}
build_package ${BUILD_BASE} \
              ${ARPACK_LOG} \
              "build_base_arpack.sh" 0 0 "N/A" \
              ${SUCCESS_ARPACK}

build_package ${BUILD_BASE} \
              ${LIBXML_LOG} \
              "build_base_libxml2.sh" 0 0 "N/A" \
              ${SUCCESS_LIBXML}

build_package ${BUILD_BASE} \
              ${QMP_LOG} \
              "build_base_qmp.sh" 0 0 "N/A" \
              ${SUCCESS_QMP}
echo ""
echo " F I N I S H E D   B A S E   B U I L D "
echo ""

# note if editing. Cannot have a space after \ esc. ("\ " does not work, but "\" does)

if [ ${BUILD_Nc3} = "YES" ]; then
  
  build_package ${BUILD_Nc3} \
                ${QDP33_SCALAR_LOG} \
                "build_qdpxx_lapH.sh" 3 3 "scalar" \
                ${SUCCESS_SCALAR_QDP_33}  # needed for Steps 2,4
  
  build_package ${BUILD_Nc3} \
                ${QDP34_SCALAR_LOG} \
                "build_qdpxx_lapH.sh" 3 4 "scalar" \
                ${SUCCESS_SCALAR_QDP_34}  # neded for Step 1
  
  build_package ${BUILD_Nc3} \
                ${QDP34_PSCALAR_LOG} \
                "build_qdpxx_lapH.sh" 3 4 "parscalar" \
                ${SUCCESS_PSCALAR_QDP_34}  # neded for Step 3 (to build chroma)
  
  build_package ${BUILD_Nc3} \
                ${CHROMA34_LOG} \
                "build_chroma_Nc.sh" 3 4 "parscalar" \
                ${SUCCESS_CHROMA34}  # neded for Step 3 
  
  build_package ${BUILD_Nc3} \
                ${QUDA34_LOG} \
                "build_quda_slaph.sh" 3 4 "parscalar" \
                ${SUCCESS_QUDA34} 
  echo ""
  echo " F I N I S H E D   Nc = 3   B U I L D "
  echo ""
fi


if [ ${BUILD_Nc4} = "YES" ]; then
  
  build_package ${BUILD_Nc4} \
                ${QDP43_SCALAR_LOG} \
                "build_qdpxx_lapH.sh" 4 3 "scalar" \
                ${SUCCESS_SCALAR_QDP_43}  # needed for Steps 2,4
  
  build_package ${BUILD_Nc4} \
                ${QDP44_SCALAR_LOG} \
                "build_qdpxx_lapH.sh" 4 4 "scalar" \
                ${SUCCESS_SCALAR_QDP_44}  # neded for Step 1
  
  
  build_package ${BUILD_Nc4} \
                ${QDP44_PSCALAR_LOG} \
                "build_qdpxx_lapH.sh" 4 4 "parscalar" \
                ${SUCCESS_PSCALAR_QDP_44}  # neded for Step 3 (to build chroma)
  
  build_package ${BUILD_Nc4} \
                ${CHROMA44_LOG} \
                "build_chroma_Nc.sh" 4 4 "parscalar" \
                ${SUCCESS_CHROMA44}  # neded for Step 3 
   
  #build_package ${BUILD_Nc4} \
  #              ${QUDA44_LOG} \
  #              "build_quda_slaph.sh" 4 4 "N/A" \
  #              ${SUCCESS_QUDA44} 
  echo ""
  echo " F I N I S H E D   Nc = 4   B U I L D "
  echo ""
fi







#
