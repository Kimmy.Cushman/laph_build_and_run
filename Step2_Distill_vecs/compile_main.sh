#!/bin/bash -l
export GCC_VERSION="8.3.1"
module load gcc/8.3.1

Nc=$1 
Lx=$2
EXE=$3
Step2_dir=$4
HOME=$5

# delete the executable
rm ${EXE}

# this is required because structures (here EvecDBData_t) are compile time defined 
# and rather than refactor the library to be runtime defined, we make this quick HACK
echo "#define Lx ${Lx}" > "${Step2_dir}/global_variables.h"
echo "#define Nc ${Nc}" >> "${Step2_dir}/global_variables.h"

echo "C O M P I L I N G  S T E P  2"
echo `date`

Iqdpxx=${HOME}/code/Nc${Nc}_Nd3_install/qdpxx_lapH/include
Ilibxml=${HOME}/code/base_install/libxml2/include/libxml2
Iqmp=${HOME}/code/base_install/qmp/include/
Iarpack=${HOME}/code/source/arpackpp/include/

arpack=${HOME}/code/base_install/arpackpp/external/arpack-ng-build/lib/libarpack.so
openblas=${HOME}/code/base_install/arpackpp/external/OpenBLAS/libopenblas.a

Lqdpxx=${HOME}/code/Nc${Nc}_Nd3_install/qdpxx_lapH/lib/
Llibxml=${HOME}/code/base_install/libxml2/lib
Lqmp=${HOME}/code/base_install/qmp/lib

Ieigen=${HOME}/code/source/eigen # required for MatrixXd used for V*eta 

mpicxx -std=c++14 -O2 -fopenmp -Wno-deprecated -w -I${Iqdpxx} -I${Ilibxml} -I${Iqmp} -I${Ieigen} -I${Iarpack} ${Step2_dir}/main.cc ${arpack} ${openblas} -L${Lqdpxx} -lqdp -lXPathReader -lxmlWriter -lqio -llime -L${Lqmp} -lxml2 -lm -lqmp -lqmp -lfiledb -lfilehash -lgfortran -o ${EXE}

echo "DONE COMPILING STEP 2"
