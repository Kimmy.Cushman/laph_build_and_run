#pragma once

#include <iostream>
#include <cstdio>
#include <chrono>
#include "qdp.h"
#include "../Matrix/laplacian_mult.h" // definition of matrix problem  
#include "../Data_wrangling/write_solution_io.h"
using namespace QDP;


//requires cmatrixa_kimmy.h
void get_evecs(CompMatrixA_LATT<double> &U,
               int N_c, 
               int L_x, 
               int num_evecs, 
               Subset all, 
               int t, 
               std::string LapH_output_base)
{
  
  std::string which_op("SR"); // search for Smallest Real (SR) evals first!
  int ncvp = 0;
  double tolp = 1e-8;
  int maxitp = 800;
  // Defining what we need: the four eigenvectors of U with largest magnitude.
  // U.Mult_Uv is the function that performs the product w <- U.v.
  QDPIO::cout << "Initializing Solver" << std::endl;
  int nx = N_c*L_x*L_x*L_x;
  ARCompStdEig<double, CompMatrixA_LATT<double>> dprob(nx, 
                                                       num_evecs, 
                                                       &U, 
                                                       &CompMatrixA_LATT<double>::MultMv, 
                                                       which_op, ncvp, tolp, 
                                                       maxitp, NULL, true);
  QDPIO::cout << "Solve Problem" << std::endl;
  dprob.FindEigenvectors();
  QDPIO::cout << "Solved ... writing solution" << std::endl;
  write_solution(U, dprob, N_c, num_evecs, t, LapH_output_base);
} // get_evecs.
