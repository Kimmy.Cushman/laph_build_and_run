/* 
1)  U = Laplacian[u], u is a multi1d list of <LatticeColorMatrix>, with Nd elements  
    U has color indices, spatial indices AKA Lattice Color Matrix  
    the *elements* of U are complex Ts, type T is therefore complex
2)  U is a MatrixWithProduct 
3)  CompMatrixA_LATT to define multiplication on a vector w = U*v
    where w and v are LatticeColorVector  
*/
#pragma once

#include "arcomp.h"
#include "blas1c.h"
#include "arcomp.h"
#include "matprod.h"
#include "qdp.h"
using namespace QDP;

//////////////////////////////////////////////////////////////////////////////////////
template<class T>
class CompMatrixA_LATT: public MatrixWithProduct<arcomplex<T> > 
{
  private:
    int nx;
    int Nd;
    Subset all;
    multi1d<LatticeColorMatrix> u; // doesnt know how big u is yet 
  public:
    void MultMv(arcomplex<T>* v, arcomplex<T>* v_out);
    CompMatrixA_LATT(multi1d<LatticeColorMatrix> &u_, int Nd_, int nxval, Subset all_); //constructor 2  
}; // CompMatrixA_LATT.
//////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////
//void CompMatrixA_LATT<T>::Mult_Uv(multi1d<LatticeColorMatrix> &u, LatticeColorVector &v, LatticeColorVector &v_out)
template<class T>
void CompMatrixA_LATT<T>::MultMv(arcomplex<T>* v, arcomplex<T>* v_out) // must use arcomplex since defined in matprod.h 
{
  // Computes the matrix vector multiplication v_out <- U*v
  // U = Laplacian[u]
  // v_out_a(x) = -6 * v_a(x) + sum_j=1^Nc < u_j,ab(x) v_b(x+j) + u^dagger_j,ab(x-j) v_b(x-j) > 
  // copy values from a pointer to v, which is a arcomplex<T>,
  // to psi which is a LatticeColorVector
  LatticeColorVector psi;
  for(int site=all.start(); site<=all.end(); site++){
    for(int color=0; color < Nc; color++){
      RComplex<REAL64> tmp(std::real(v[site*Nc+color]),std::imag(v[site*Nc+color])); ;
      psi.elem(site).elem().elem(color) = tmp;
    }
  }
  //std::cout << "Inside CompMatrixA_LATT" << std::endl; 
  LatticeColorVector psi_out;
  psi_out= 6.0*psi;  // calculate -Laplacian using def from my thesis and https://arxiv.org/pdf/1104.3870.pdf 
  for(int j=0; j<3; j++){  // 3 here is for x,y,z directions 
    // DEFINTION OF LAPLACIAN HERE
    // since u/v is lattice color matrix/vector, multiplication is 
    // understood to mean for every spatial and color element
    psi_out -=  u[j]                        * shift(psi, FORWARD, j); // - same as ^
    psi_out -=  shift(adj(u[j]),BACKWARD,j) * shift(psi, BACKWARD,j); // - same as ^
  }

  //std::cout << "calculated psi_out" << std::endl; 
  // copy values from a LatticeColorVector psi_out 
  // to pointer to v_out, which is a arcomplex<T>
  for(int site=all.start(); site<=all.end(); site++){
    for(int color=0; color < Nc; color++){
      double re = psi_out.elem(site).elem().elem(color).real();
      double im = psi_out.elem(site).elem().elem(color).imag();
      std::complex<double> tmp(re,im);
      v_out[site*Nc+color] = tmp;
    }
  }
  
  return;
} // MultMv
//////////////////////////////////////////////////////////////////////////////////////


//////////////// constructor 2 ////////////////////////////////////////////////////////
template<class T>
CompMatrixA_LATT<T>::CompMatrixA_LATT(multi1d<LatticeColorMatrix> &u_, int Nd_, int nxval, Subset all_):
  MatrixWithProduct<arcomplex<T> >(nxval)
  //Constructor.
{
  Nd = Nd_;
  u(Nd);
  u = u_;
  nx = nxval;
  all = all_;
} // constructor.
//////////////////////////////////////////////////////////////////////////////////////





