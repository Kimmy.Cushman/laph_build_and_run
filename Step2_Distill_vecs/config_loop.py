import numpy as np
import os 
import sys
import time 

# execute here 
MACHINE = sys.argv[1]
Step2_dir = sys.argv[2]
exe = sys.argv[3]

# physics
Nc = int(sys.argv[4])
Lx = int(sys.argv[5])
Nt = int(sys.argv[6])
Nvec = int(sys.argv[7])
Nnoise = int(sys.argv[8])
# job
time_job = int(sys.argv[9])
queue = sys.argv[10]
group = sys.argv[11]

# resources
tasks = int(sys.argv[12])
nodes = int(sys.argv[13])
threads = int(sys.argv[14])

# geom 
geom_x = int(sys.argv[15])
geom_y = int(sys.argv[16])
geom_z = int(sys.argv[17])
geom_t = int(sys.argv[18]) # don't use this param, Nd=3

# files 
result_dir = sys.argv[19]
splice_output = sys.argv[20]
timeslice_start = int(sys.argv[21])
timeslice_stop = int(sys.argv[22])
LapH_vecs_output_file = sys.argv[23]
sLapH_vecs_output_file = sys.argv[24]

batch_output_file = "%s/vectors_Nvec%i_Nnoise%i_n%i_N%i_thread%i_gx%i_gy%i_gz%i_t%i_%i.out"%(result_dir, Nvec, Nnoise, 
                                                                                     tasks, nodes, threads, 
                                                                                     geom_x, geom_y, geom_z, 
                                                                                     timeslice_start, timeslice_stop)

print(batch_output_file)
os.system("rm %s"%batch_output_file)

input_file = "%s/Nc%i.mod"%(result_dir, Nc)

run_main = "sh %s/run_main.sh"%Step2_dir
bash_input = str(threads)
geom = "%i %i %i"%(geom_x, geom_y, geom_z)
commandline_args = "%i %i %s %s %s %i %i %i %i %i"%(Nc, Lx, splice_output, 
                                                    LapH_vecs_output_file, 
                                                    sLapH_vecs_output_file, 
                                                    Nt, Nvec, Nnoise,
                                                    timeslice_start, timeslice_stop)
srun_args = "%i %s %s %i %i"%(time_job, queue, batch_output_file, tasks, nodes)
executable = exe

run = " ".join([run_main, bash_input, geom, commandline_args, srun_args, executable])

print("running from config loop python script")
print(run)
print
print("see output")
print(batch_output_file)
print
os.system(run)

