#ifndef SPLICER_FUNCS_H
#define SPLICER_FUNCS_H


#include <iostream>
#include <cstdio>
#include <string>
#include "qdp.h"
#include "qdp_iogauge.h"
// database 
#include "qdp_map_obj_disk_Nd3.h"
#include "qdp_disk_map_slice_Nd3.h"


namespace QDP
{
  //****************************************************************************
  //! Prop operator
  struct KeyLatticeColorMatrixTimeSlice_t
  {
    int        t_slice;       /*!< Time slice */
    int        direction;     /*!< direction */
  };

  //----------------------------------------------------------------------------
  // KeyLatticeColorMatrix read
  void read(BinaryReader& bin, KeyLatticeColorMatrixTimeSlice_t& param)
  {
    read(bin, param.t_slice);
    read(bin, param.direction);
  }

  // KeyLatticeColorMatrix write
  void write(BinaryWriter& bin, const KeyLatticeColorMatrixTimeSlice_t& param)
  {
    write(bin, param.t_slice);
    write(bin, param.direction);
  }

  //////////////////////////////////////////////////////////////////////////
  void fail(int line)
  {
    QDPIO::cout << "FAIL: line= " << line << std::endl;
    QDP_finalize();
    exit(1);
  }
}


#endif // SPLICER_FUNCS_H
