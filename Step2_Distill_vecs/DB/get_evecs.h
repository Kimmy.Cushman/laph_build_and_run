#ifndef GET_EVECS_H
#define GET_EVECS_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include <Eigen/Dense>
// from -I${QDP}
#include "qdp.h" 
#include "qdp_map_obj.h"  
#include "qdp_map_obj_disk.h"
#include "qdp_db.h"
#include "qdp_map_obj_disk_multiple.h" // from -I QDP (maybe not needed now, need if perambs stored in multiple files) 
#include "key_prop_matelem.h"// local file
#include "../global_variables.h"
#include "../Data_wrangling/read_write.h" 
using namespace Eigen;
using namespace std; 
using std::vector;
using std::ifstream;
using namespace QDP;
using namespace FILEDB;
using namespace Chroma;



void get_evecs(string filename_base,
               int t,
               multi1d<LatticeColorVector> &Vec,
               int Nvec_file)
{ 
  std::ostringstream oss;
  oss << filename_base << t << ".db";
  const string filename = oss.str();

  EvecDBKey<EvecDBKey_t> Key_read;
  Key_read.key().t_slice = t;

  // ---- READ EVECS ---- //
  BinaryStoreDB< EvecDBKey<EvecDBKey_t>, EvecDBData<EvecDBData_t> > db_read_vecs;
  db_read_vecs.open(filename, O_RDONLY, 0664);
  QDPIO::cout<< filename << std::endl;
  
  QDPIO::cout << "Nvec_file = " << Nvec_file << std::endl;
  for(int vec_num=0; vec_num< Nvec_file; ++vec_num){
    // ************ get values from .db file ************ //
    // KEY
    Key_read.key().colorvec = vec_num;
    // VALUE
    EvecDBData<EvecDBData_t> evector_read;
    // READ 
    db_read_vecs.get(Key_read, evector_read);


    // ************** fill CPP array ************* //
    // loop over spatial points
    for(int site=0; site < Lx*Lx*Lx; site++){
      // loop over color 
      for(int color=0; color < Nc; color++){
        std::complex<double> std_val = evector_read.data().evec_data.at(site*Nc + color);
        RComplex<REAL64> RC_val(std::real(std_val), std::imag(std_val));
        Vec[vec_num].elem(site).elem().elem(color) =RC_val; 
      } // end loop over color 
    } // end loop over sites
  } // end loop over Dist evecs 
}


#endif
