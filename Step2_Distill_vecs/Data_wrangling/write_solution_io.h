#pragma once

// standard libraires
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <complex>
#include <cmath>
#include <typeinfo>
//arpack matrix libraries, not solving here, but need Prop object  
#include "arcomp.h"
#include "blas1c.h"
#include "lapackc.h"
#include "arscomp.h"
// kimmy edited code
#include "../Matrix/matprod.h"
// sorting algorithm
#include "../Data_wrangling/sort_solution.h"
#include "../Data_wrangling/read_write.h" 
// database libraries 
#include "qdp.h"
#include "qdp_map.h"
#include "qdp_map_obj.h"
#include "qdp_map_obj_disk.h"
#include "qdp_map_obj_disk_multiple.h"
#include "qdp_disk_map_slice.h"
#include "qdp_db.h" // source of a bunch of warnings! (silenced) // need
// Including these just to check compilation
#include "qdp_map_obj_memory.h"
#include "qdp_map_obj_null.h"
using namespace QDP;
using namespace FILEDB;


////////////////////////////////////////////////////
// ----------- print system vairables ----------- //
////////////////////////////////////////////////////
template<class ARMATRIX, class ARFLOAT>
void print_system_variables(ARCompStdEig<ARFLOAT, ARMATRIX> &Prob, 
                            int dim, 
                            int nconv)
{
  int                i, mode;
  arcomplex<ARFLOAT> *Ax;
  ARFLOAT            *ResNorm;

  nconv = Prob.ConvergedEigenvalues();
  mode  = Prob.GetMode();

  QDPIO::cout << "\n" << "\n" << "Testing ARPACK++ class ARCompStdEig" << std::endl;
  QDPIO::cout << "Complex eigenvalue problem: A*x - lambda*x" << std::endl;
  switch (mode) {
  case 1:
    QDPIO::cout << "Regular mode \n" << std::endl;
    break;
  case 3:
    QDPIO::cout << "Shift and invert mode \n" << std::endl;
  }
  QDPIO::cout << "Dimension of the system            : " << dim            << std::endl;
  QDPIO::cout << "Number of 'requested' eigenvalues  : " << Prob.GetNev()  << std::endl;
  QDPIO::cout << "Number of 'converged' eigenvalues  : " << nconv          << std::endl;
  QDPIO::cout << "Number of Arnoldi vectors generated: " << Prob.GetNcv()  << std::endl;
  QDPIO::cout << "Number of iterations taken         : " << Prob.GetIter() << std::endl;
  QDPIO::cout << " " << std::endl;
} // print system variables 


////////////////////////////////////////////////////
// ------------- sort evals system  ------------- //
////////////////////////////////////////////////////
template<class ARMATRIX, class ARFLOAT>
void sort_evals_system(ARCompStdEig<ARFLOAT, ARMATRIX> &Prob,
                       std::vector<long unsigned int> &eval_indices_sorted, 
                       int nconv)
{
  std::vector<double> e_vals;
  if (Prob.EigenvaluesFound()) {
    for (int i=0; i<nconv; i++) {
      e_vals.push_back(real(Prob.Eigenvalue(i)));
    }
    eval_indices_sorted = sort_indices(e_vals); 
  }
} // sort evals system


////////////////////////////////////////////////////
// ----------- print system residulas ----------- //
////////////////////////////////////////////////////
template<class ARMATRIX, class ARFLOAT>
void print_system_residuals(ARMATRIX &A, 
                            ARCompStdEig<ARFLOAT, ARMATRIX> &Prob, 
                            int dim, 
                            int nconv)
{
  QDPIO::cout<< "FOUND EIGENVECTORS" << std::endl;
  // Printing the residual norm || A*x - lambda*x ||
  // for the nconv accurately computed eigenvectors.
  arcomplex<ARFLOAT> *Ax;
  ARFLOAT            *ResNorm;
  Ax      = new arcomplex<ARFLOAT>[dim];
  ResNorm = new ARFLOAT[nconv+1];

  for (int i=0; i<nconv; i++) {
    A.MultMv(Prob.RawEigenvector(i),Ax); 
    axpy(dim, -Prob.Eigenvalue(i), Prob.RawEigenvector(i), 1, Ax, 1);
    ResNorm[i] = nrm2(dim, Ax, 1) / lapy2(real(Prob.Eigenvalue(i)),imag(Prob.Eigenvalue(i)));
  }

  for (int i=0; i<nconv; i++) {
    QDPIO::cout << "||A*x(" << (i+1) << ") - lambda(" << (i+1) << ")*x(" << (i+1) << ")||: " << ResNorm[i] << std::endl;
  }
} // print system residuals 


////////////////////////////////////////////////////
// ----- write vecs to lattice array Vecs ------- //
////////////////////////////////////////////////////
template<class ARMATRIX, class ARFLOAT>
void get_vectors_to_array(ARCompStdEig<ARFLOAT, ARMATRIX> &Prob, 
                          multi1d<LatticeColorVector> &Vecs_sorted,
                          std::vector<double> &evals_sorted,
                          int N_c, 
                          int num_vecs,
                          std::vector<long unsigned int> &eval_indices_sorted)
{
  RComplex<REAL64> one(std::real(std::complex<double>(1.0, 0.0)), std::imag(std::complex<double>(1.0, 0.0)));
  RComplex<REAL64> zero(std::real(std::complex<double>(0.0, 0.0)), std::imag(std::complex<double>(0.0, 0.0)));
  //RComplex<REAL64> tmp;
  // note have to use <= all.end() since all.end() = Lx^3 -1 angry face -> (D:<) 
  for(int site=all.start(); site <= all.end(); ++site){
    for(int color=0; color < N_c; color++){
      for(int vec_num=0; vec_num < num_vecs; ++vec_num){
        int index = eval_indices_sorted[vec_num];
        RComplex<REAL64> tmp(std::real(Prob.RawEigenvector(index)[site*N_c+color]),std::imag(Prob.RawEigenvector(index)[site*N_c+color])); // COMMENT BACK
        Vecs_sorted[vec_num].elem(site).elem().elem(color) = tmp;
      } // vec loop
    } // color loop
  } // site loop
  
  for(int vec_num=0; vec_num<num_vecs; ++vec_num){
    int index = eval_indices_sorted[vec_num];
    evals_sorted[vec_num] = std::real(Prob.Eigenvalue(index)); //eval;
  }
} // write vectors to array 


////////////////////////////////////////////////////
// ---  get weights array for db and mod      --- //
////////////////////////////////////////////////////
void get_weights_sorted(multi1d<Real> &weights, 
                        std::vector<double> &evals_sorted,
                        std::string sLapH_or_LapH, 
                        int num_vecs)
{
  for(int vec_num=0; vec_num<num_vecs; ++vec_num){
    if(sLapH_or_LapH == "LapH") weights[vec_num] = evals_sorted[vec_num]; //eval;
    else weights[vec_num] = 1.0;
  }
}


////////////////////////////////////////////////////
// -- write vecs to *.db for observable code --- //
////////////////////////////////////////////////////
void write_vectors_to_db(multi1d<LatticeColorVector> &Vecs_sorted, 
                         std::vector<double> &evals_sorted,
                         int N_c, 
                         int num_vecs,
                         int t,
                         std::string sLapH_or_LapH,
                         std::string output_base)
{
  multi1d<Real> weights(num_vecs); 
  get_weights_sorted(weights, evals_sorted, sLapH_or_LapH, num_vecs);

  // define filename 
  std::ostringstream db_evecs_file_str;
  std::ostringstream db_evals_file_str;
  db_evecs_file_str    <<  output_base   << t << "_evecs.db";  
  db_evals_file_str    <<  output_base   << t << "_evals.db";  
  const std::string  db_evecs_filename =   db_evecs_file_str.str();
  const std::string  db_evals_filename =   db_evals_file_str.str();
  // define db object 
  QDPIO::cout << "\n\n\nSTART: Writing for observable code" << std::endl;
  BinaryStoreDB< EvecDBKey<EvecDBKey_t>, EvecDBData<EvecDBData_t> > db_evecs;
  BinaryStoreDB< EvecDBKey<EvecDBKey_t>, EvecDBData<EvecDBEval_t> > db_evals;
  
  std::string meta_data("random meta string");
  db_evecs.setMaxUserInfoLen(meta_data.size());
  db_evals.setMaxUserInfoLen(meta_data.size());
  
  // ----- fill evals DB   ------ //
  db_evals.open(db_evals_filename, O_RDWR | O_CREAT | O_TRUNC, 0664);
  db_evals.insertUserdata(meta_data);
  for(int vec_num=0; vec_num < num_vecs; vec_num++){
    // define keys
    EvecDBKey<EvecDBKey_t> ValsDBKey;
    ValsDBKey.key().t_slice = t;
    ValsDBKey.key().colorvec = vec_num;
    // fill and insert DBEval
    EvecDBData<EvecDBEval_t> ValsDBEval;
    ValsDBEval.data().eval = weights[vec_num];
    db_evals.insert(ValsDBKey, ValsDBEval);
  }
  db_evals.flush();
  db_evals.close();
  
  // ----- fill evecs DB   ------ //
  db_evecs.open(db_evecs_filename, O_RDWR | O_CREAT | O_TRUNC, 0664);
  db_evecs.insertUserdata(meta_data);
  for(int vec_num=0; vec_num < num_vecs; vec_num++){
    // define keys
    EvecDBKey<EvecDBKey_t> VecsDBKey;
    VecsDBKey.key().t_slice = t;
    VecsDBKey.key().colorvec = vec_num;
    // fill and insert DBData 
    EvecDBData<EvecDBData_t> VecsDBData;
    for(int site=all.start(); site <= all.end(); ++site){
      for(int color=0; color < N_c; color++){
        double v_real = Vecs_sorted[vec_num].elem(site).elem().elem(color).real();
        double v_imag = Vecs_sorted[vec_num].elem(site).elem().elem(color).imag();
        VecsDBData.data().evec_data[site*N_c+color] = complex<double>(v_real, v_imag);
      } // color loop
    } // site loop
    db_evecs.insert(VecsDBKey, VecsDBData);
  } // vec loop
  db_evecs.flush();
  db_evecs.close();
  
  QDPIO::cout << "END: Writing to for observable code" << std::endl;
} // write vectors to db 


////////////////////////////////////////////////////
// -- write vecs to *.mod for perambulators code - //
////////////////////////////////////////////////////
void write_vectors_to_mod(multi1d<LatticeColorVector> &Vecs_sorted, 
                          std::vector<double> &evals_sorted,
                          int N_c, 
                          int num_vecs, 
                          int t,
                          std::string sLapH_or_LapH,
                          std::string output_base)
{
  // define filename 
  std::ostringstream mod_file_str;
  mod_file_str    <<  output_base   << t << ".mod";  
  const std::string  mod_filename =   mod_file_str.str();
  
  multi1d<Real> weights(num_vecs); 
  get_weights_sorted(weights, evals_sorted, sLapH_or_LapH, num_vecs);
  
  // Some metadata
  std::string meta_data;
  XMLBufferWriter file_xml;
  push(file_xml, "MODMetaData");
  write(file_xml, "Weights", weights);
  pop(file_xml);
  meta_data = file_xml.str();
  
  // define map object 
  QDP::MapObjectDisk<KeyTimeSliceColorVec_t, LatticeColorVector> pc_map; 
  pc_map.setDebug(10);
  pc_map.insertUserdata(meta_data);
  pc_map.open(mod_filename, std::ios_base::in | std::ios_base::out | std::ios_base::trunc);
  
  // insert keys, vecs  
  for(int vec_num=0; vec_num< num_vecs; ++vec_num){
    QDPIO::cout << "vec num: " << vec_num << std::endl;
    KeyTimeSliceColorVec_t the_key;
    the_key.t_slice = t;
    the_key.colorvec = vec_num;
    LatticeColorVector Vecs_i(Vecs_sorted[vec_num]);
    if (pc_map.insert(the_key,Vecs_i) != 0){ 
      QDPIO::cout << "ERROR" << std::endl;
      QDPIO::cerr << __func__ << ": error inserting" << std::endl;
      QDP_abort(1);
    }
    else{
      QDPIO::cout << "not error" << std::endl;
    }
  } // vec loop
} // write vectors to mod 


////////////////////////////////////////////////////
// ------ main function to write solution ------- //
////////////////////////////////////////////////////
template<class ARMATRIX, class ARFLOAT>
void write_solution(ARMATRIX &A, 
                    ARCompStdEig<ARFLOAT, ARMATRIX> &Prob, 
                    int N_c, 
                    int num_vecs, 
                    int t, 
                    std::string LapH_output_base)
{
  // print system variables 
  int dim = Prob.GetN();  // dimension of the system
  int nconv = Prob.ConvergedEigenvalues();  // number of converged evecs
  print_system_variables(Prob, dim, nconv);
  // sort indices into index array 
  std::vector<long unsigned int> eval_indices_sorted;
  sort_evals_system(Prob, eval_indices_sorted, nconv);
  if (Prob.EigenvectorsFound()) {
    try 
    {    
      // print residuals 
      print_system_residuals(A, Prob, dim, nconv);   
      
      // save sorted vectors to Lattice array for convenient access 
      multi1d<LatticeColorVector> Vecs_sorted(num_vecs);
      std::vector<double> evals_sorted(num_vecs);
      // get LapH vectors from Prob
      get_vectors_to_array(Prob, Vecs_sorted, evals_sorted, N_c, num_vecs, eval_indices_sorted);
      
      std::string LapH_str("LapH");   // FIXME
      // write *.db for observable code 
      write_vectors_to_db(Vecs_sorted, evals_sorted, N_c, num_vecs, t, LapH_str, LapH_output_base);
      
      // write *.mod for perambulator code 
      write_vectors_to_mod(Vecs_sorted, evals_sorted, N_c, num_vecs, t, LapH_str, LapH_output_base);
      
      QDPIO::cout<< "D O N E      W R I T I N G   to mod " << std::endl;
    } // try    
    catch(const std::string& e) 
    {
      QDPIO::cout << "ERROR" << std::endl;
      QDPIO::cout << "Caught: " << e << std::endl;
    } // catch 
  } // if found 
} // write_solution

