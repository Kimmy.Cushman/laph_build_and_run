#pragma once

#include <iostream>
#include <cstdio>
#include <chrono>
#include "qdp.h"
#include "../DB/qdp_map_obj_disk_Nd3.h"
#include "../DB/qdp_disk_map_slice_Nd3.h"
#include "../DB/splicer_funcs_Nd3.h"
using namespace QDP;
    
void fill_gauge_field_from_mod(multi1d<LatticeColorMatrix> &u, std::string input_mod, int Nd, int t)
{
    /////////////// READ FIELD FROM DB ////////////////////////
    // Read each timeslices  *.mod file 
    std::string map_obj_file(input_mod);
    
    try {
      QDPIO::cout<< "R E A D I N G" << std::endl;
      MapObjectDisk<KeyLatticeColorMatrixTimeSlice_t, LatticeColorMatrix > pc_map;
      pc_map.setDebug(1);
      pc_map.open(map_obj_file, std::ios_base::in);
      
      std::vector<KeyLatticeColorMatrixTimeSlice_t> keys;
      pc_map.keys(keys);
    
      for(int d = 0; d < Nd; d++){
        KeyLatticeColorMatrixTimeSlice_t tmp_key;
        tmp_key.t_slice = t;
        tmp_key.direction = d;
        pc_map.get(tmp_key,u[d]);
      }
      QDPIO::cout<< "D O N E   R E A D I N G" << std::endl;

    }
    catch(const std::string& e) { 
      QDPIO::cout << "ERROR" << std::endl;
      QDPIO::cout << "Caught: " << e << std::endl;
      fail(__LINE__);
    }

}
