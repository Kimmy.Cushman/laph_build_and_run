#pragma once

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <complex>
#include <cmath>
// database libraries 
#include "qdp.h"
#include "qdp_map.h"
#include "qdp_map_obj.h"
#include "qdp_map_obj_disk.h"
#include "qdp_map_obj_disk_multiple.h"
#include "qdp_disk_map_slice.h"
#include "qdp_db.h" // source of a bunch of warnings! (silenced) // need
// Including these just to check compilation
#include "qdp_map_obj_memory.h"
#include "qdp_map_obj_null.h"
#include "../global_variables.h" // temporary file created in run script (deleted upon completion)
using namespace QDP;
using namespace FILEDB;


//****************************************************************************
//! Prop operator REQUIRED for perambulator code .mod 
struct KeyTimeSliceColorVec_t
{
  KeyTimeSliceColorVec_t() {}
  KeyTimeSliceColorVec_t(int t_slice_, int colorvec_) : t_slice(t_slice_), colorvec(colorvec_) {}

  int        t_slice;       /*!< S-ource time slice */
  int        colorvec;      /*!< Colorstd::vector index */
};
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// KeyPropColorVec read  // PERMABULATOR CODE 
void read(BinaryReader& bin, KeyTimeSliceColorVec_t& param)
{
  read(bin, param.t_slice);
  read(bin, param.colorvec);
}
// KeyPropColorVec write// PERMABULATOR CODE
void write(BinaryWriter& bin, const KeyTimeSliceColorVec_t& param)
{
  write(bin, param.t_slice);
  write(bin, param.colorvec);
}
//----------------------------------------------------------------------------


// ---                         KEYS                            --- //
////////////////////////// ****************** /////////////////////////
//---------------------------------------------------------------------
//! Some struct to use
struct EvecDBKey_t
{
  int t_slice;
  int colorvec;
};

//! Reader
void read(BinaryReader& bin, EvecDBKey_t& param)
{
  read(bin, param.t_slice);
  read(bin, param.colorvec);
}

//! Writer
void write(BinaryWriter& bin, const EvecDBKey_t& param)
{
  write(bin, param.t_slice);
  write(bin, param.colorvec);
}
//---------------------------------------------------------------------


//---------------------------------------------------------------------
// Concrete key class
template<typename K>
class EvecDBKey : public DBKey // DBKey defined in FILEDB
{
public:
  //! Default constructor
  EvecDBKey() {} 

  //! Constructor from data
  EvecDBKey(const K& k) : key_(k) {}

  //! Setter
  K& key() {return key_;}

  //! Getter
  const K& key() const {return key_;}

  // Part of Serializable
  //virtual unsigned short serialID (void) const {return 456;}
  const unsigned short serialID (void) const {return 456;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, key());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, key());
  }

  // Part of DBKey
  int hasHashFunc (void) const {return 0;}
  int hasCompareFunc (void) const {return 0;}

  /**
    * Empty hash and compare functions. We are using default functions.
    */
  static unsigned int hash (const void* bytes, unsigned int len) {return 0;}
  static int compare (const FFDB_DBT* k1, const FFDB_DBT* k2) {return 0;}
  
private:
  K  key_;
};
//---------------------------------------------------------------------



// ---                           DATA                            --- //
////////////////////////// ****************** /////////////////////////
//---------------------------------------------------------------------
//! Data structure define
struct EvecDBData_t
{
  std::vector<std::complex<double>> evec_data = std::vector<std::complex<double>>(Nc*Lx*Lx*Lx); 
};
// end define 


//! Reader
void read(BinaryReader& bin, EvecDBData_t& param)
{
  read(bin, param.evec_data);
}

//! Writer
void write(BinaryWriter& bin, const EvecDBData_t& param)
{
  write(bin, param.evec_data);
}


// Concrete data class
template<typename D>
class EvecDBData : public DBData // DBData defined in FILEDB
{
public:
  //! Default constructor
  EvecDBData() {} 

  //! Constructor from data
  EvecDBData(const D& d) : data_(d) {}

  //! Setter
  D& data() {return data_;}

  //! Getter
  const D& data() const {return data_;}

  // Part of Serializable
  //virtual unsigned short serialID (void) const {return 123;}
  const unsigned short serialID (void) const {return 123;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, data());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, data());
  }

private:
  D  data_;
};
//---------------------------------------------------------------------


// ---                      EVALS (overkill)                     --- //
////////////////////////// ****************** /////////////////////////
//! EVALS structure define
struct EvecDBEval_t
{
  Real eval;
};
// end define 


//! Reader
void read(BinaryReader& bin, EvecDBEval_t& param)
{
  read(bin, param.eval);             
}


//! Writer
void write(BinaryWriter& bin, const EvecDBEval_t& param)
{
  write(bin, param.eval);      
}

/*
// Concrete eval class
template<typename D>
class EvecDBEval : public DBData // DBData defined trivially in QDP otherlibs/filedb
{
public:
  //! Default constructor
  EvecDBEval() {} 

  //! Constructor 
  EvecDBEval(const D& e) : eval_(e) {}

  //! Setter
  D& eval() {return eval_;}

  //! Getter
  const D& eval() const {return eval_;}

  // Part of Serializable
  //virtual unsigned short serialID (void) const {return 123;}
  const unsigned short serialID (void) const {return 123;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, eval());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, eval());
  }

private:
  D eval_;
};
*/
