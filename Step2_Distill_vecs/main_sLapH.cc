#include <iostream>
#include <cstdio>
#include <chrono>
// QDP 
#include "qdp.h"
// Eigen needed for get_evecs 
#include <Eigen/Dense>
// local 
#include "DB/get_evecs.h" // load evecs exactly as in Step4
#include "Stochastic/get_sLapH_vecs.h" // calculate noise vecs from 
#include "Data_wrangling/write_solution_io.h" // save noice vecs exactly how evecs are saved 
#include "global_variables.h" // Nc and Lx
#define START_CODE() QDP_PUSH_PROFILE(QDP::getProfileLevel())
#define END_CODE()   QDP_POP_PROFILE()

using namespace QDP;

int main(int argc, char *argv[])
{
/* MUST INCLUDE COMMAND LINE ARGS */
  // // // // // // // // // // // //
  std::string LapH_input_base = argv[1]; // will have ending t7.mod/.db, t8.mod/.db etc
  std::string sLapH_output_base = argv[2]; // will have ending t7.mod/.db, t8.mod/.db etc
  int Nt = atoi(argv[3]); // which timeslice to find e-vecs
  int Nvec_file = atoi(argv[4]); // same as N_vec in global variables 
  int num_evecs = atoi(argv[5]); // num e-vecs
  int num_noise = atoi(argv[6]); // num e-vecs
  int time_start = atoi(argv[7]);
  int time_stop = atoi(argv[8]);
  /*  REQUIRED  AS  ABOVE  */  
  

  START_CODE();
  // Put the machine into a known state
  QDP_initialize(&argc, &argv); // NOTE that this is Nd=3, so LatticeColorVecotr is 2d lattice 
  const int latt_size[] = {Lx,Lx,Lx};
  multi1d<int> nrow(Nd); // 3
  nrow = latt_size;  // Use only Nd elements
  Layout::setLattSize(nrow);
  Layout::create();  
  
  QDPIO::cout << "Nvec_file to load: " << Nvec_file << "\n";
  QDPIO::cout << "Nvec to sum over: " << num_evecs << "\n";
  QDPIO::cout << "Nnoise: " << num_noise << "\n";
  QDPIO::cout << "tstart " << time_start << "\n";
  QDPIO::cout << "tstop " << time_stop << "\n";

  for(int t=time_start; t<time_stop; t++){
    QDPIO::cout << "\n\n\n Calculating noise vecs FOR t = " << t << "\n\n\n";
    
    // load LapH vectors into matrix (get_evecs.h)
    QDPIO::cout << "Reading LapH eigenvector" << std::endl;
    multi1d<LatticeColorVector> evector(Nvec_file); // get all Nvec_file into array, but only use num_evecs of them 
    get_evecs(LapH_input_base, t, evector, Nvec_file);

    // get noise vecs (get_sLapH_vecs.h)
    // FIXME hardwired masking in stochastic.h
    //QDPIO::cout << "Calculating sLapH vectors" << std::endl;
    multi1d<LatticeColorVector> sVecs(num_noise); 
    calc_noise_vectors(evector, sVecs, Nc, num_evecs, num_noise);
    
    // write vectors to file (write_solution_io.h)
    QDPIO::cout << "Writing sLapH vecs to db" << std::endl;
    write_vectors_to_db(sVecs, Nc, num_noise, t, sLapH_output_base);
    QDPIO::cout << "Writing sLapH vecs to mod" << std::endl;
    std::string sLapH_str("sLapH");
    std::vector<double> evals; // function placeholder
    std::vector<long unsigned int> eval_indices_sorted; // function placeholder
    write_vectors_to_mod(evals, sVecs, eval_indices_sorted,
                         Nc, num_noise, t, sLapH_output_base, sLapH_str);
    QDPIO::cout << "\n\n Done writing \n\n" << std::endl;
  } // end t loop
  
  QDP_finalize();
  std::cout << "\n -- FINALIZED -- " << std::endl;
  exit(0);
}
