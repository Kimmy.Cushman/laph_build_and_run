#include <iostream>
#include <cstdio>
#include <chrono>
// QDP 
#include "qdp.h" // Nd defined 
// ARPACK 
#include "arscomp.h"
// local 
#include "Data_wrangling/read_mod.h" // fill gauge field func defined 
#include "Matrix/laplacian_mult.h"  // CompMatrixA_LATT class defined  
#include "Matrix/get_evecs_solution.h" // get_evecs function defined 
#include "global_variables.h" // Lx, Nc
#include "../Step1_Splice_times/precision_check.h"
#define START_CODE() QDP_PUSH_PROFILE(QDP::getProfileLevel())
#define END_CODE()   QDP_POP_PROFILE()

using namespace QDP;

int main(int argc, char *argv[])
{
/* MUST INCLUDE COMMAND LINE ARGS */
  // // // // // // // // // // // //
  std::string input_mod = argv[1];
  std::string LapH_output_base = argv[2]; // will have ending t7.mod/.db, t8.mod/.db etc
  int Nt = atoi(argv[3]); // which timeslice to find e-vecs
  int num_evecs = atoi(argv[4]); // num e-vecs
  int time_start = atoi(argv[5]);
  int time_stop = atoi(argv[6]);
  /*  REQUIRED  AS  ABOVE  */  
  

  START_CODE();
  // Put the machine into a known state
  QDP_initialize(&argc, &argv);
  // Setup the layout
  const int latt_size[] = {Lx,Lx,Lx};
  multi1d<int> nrow(Nd);
  nrow = latt_size;  // Use only Nd elements
  Layout::setLattSize(nrow);
  Layout::create();
  
  QDPIO::cout << "LapH_output_base" << std::endl; 
  QDPIO::cout << LapH_output_base << std::endl; 
  std::string precision_type = "float";
  //check_precision_values(precision_type, Nd);
  
   
  QDPIO::cout << "Nvec " << num_evecs << "\n";
  for(int t=time_start; t<time_stop; t++){
    QDPIO::cout << "\n\n\n RUNNING DISTILLVECS FOR t = " << t << "\n\n\n";
    multi1d<LatticeColorMatrix> u(Nd);

    //fill_gauge_field_from_mod(u, input_mod, Nd, t); // Data_wrangling/read_mod.h
    
    
    // free filed U[mu]^{ab}(x) = identity
    Real one = 1.0;
    LatticeColorMatrix U_free = one;
    u(0) = U_free;
    u(1) = U_free;
    u(2) = U_free;
    

    //store data in another form for matrix solve 
    CompMatrixA_LATT<double> U(u, Nd, Nc*Lx*Lx*Lx, all); // all created with QDP setup

    // compute WORK DONE HERE! 
    std::cout << "\n --- G E T T I N G   E V E C S  ---" << std::endl;
    auto start = std::chrono::high_resolution_clock::now();   
    get_evecs(U, Nc, Lx, num_evecs, all, t, LapH_output_base);            // Matrix/get_evecs_solution.h 
    auto stop = std::chrono::high_resolution_clock::now();  
    auto duration = std::chrono::duration_cast<std::chrono::minutes>(stop - start);
    QDPIO::cout << "time to get evecs in minutes: " << duration.count() << std::endl;   
    QDPIO::cout << "\n --- D O N E --- " << std::endl;
  } // end t loop
  
  QDP_finalize();
  std::cout << "\n -- FINALIZED -- " << std::endl;
  exit(0);
}
