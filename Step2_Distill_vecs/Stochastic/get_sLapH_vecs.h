#pragma once

// standard libraires
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <complex>
#include <cmath>
// v_ind evectors function defined
#include "../DB/get_evecs.h" 
// stochastic noise vectors
#include "../Stochastic/stochastic.h"
// database libraries 
#include "qdp.h"
#include "qdp_map.h"
#include "qdp_map_obj.h"
#include "qdp_map_obj_disk.h"
#include "qdp_map_obj_disk_multiple.h"
#include "qdp_disk_map_slice.h"
#include "qdp_db.h" // source of a bunch of warnings! (silenced) // need
// Including these just to check compilation
#include "qdp_map_obj_memory.h"
#include "qdp_map_obj_null.h"
// local 

using namespace QDP;
using namespace FILEDB;



////////////////////////////////////////////////////
// ------ calculate noise vecs to sVecs --------- //
////////////////////////////////////////////////////
void calc_noise_vectors(multi1d<LatticeColorVector> &Vecs,
                        multi1d<LatticeColorVector> &sVecs,
                        int N_c,
                        int num_vecs, 
                        int num_noise)
{
  // use convention
  // V[x,i] * eta[i,n] where x is spatial and color, i is distillation index, n is noise vector index
  std::vector<complex<double>> eta(num_noise*num_vecs);
  get_noise_vectors(eta, num_noise, num_vecs);
  // sLapH_lcv_array(x,n) = sum_i lcv_array(x,i) * eta(i,n) 
  for(int i=0; i<num_vecs; i++){ // SUM OVER THIS VAL
    for(int n=0; n<num_noise; n++){
      complex<double> noise_val = eta[i*num_noise + n];
      for(int site=all.start(); site <= all.end(); ++site){
        for(int color=0; color < N_c; color++){
          RComplex<REAL64> RC_vec_val(Vecs[i].elem(site).elem().elem(color));
          complex<double> vec_val = complex<double>(RC_vec_val.real(), RC_vec_val.imag());
          complex<double> contract_val = vec_val * noise_val;
          RComplex<REAL64> RC_contract_val(std::real(contract_val), std::imag(contract_val));
          sVecs[n].elem(site).elem().elem(color) += RC_contract_val;
        } // Nnoise loop
      } // Nvec loop
    } // color loop
  } // site loop
}

