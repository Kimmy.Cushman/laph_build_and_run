#ifndef STOCHASTIC_H
#define STOCHASTIC_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
// from -I${QDP}
//#include "qdp.h" 
//#include "qdp_map_obj.h"  
//#include "qdp_map_obj_disk.h"
//#include "qdp_db.h"
//#include "qdp_map_obj_disk_multiple.h" // from -I QDP (maybe not needed now, need if perambs stored in multiple files) 
//#include "qdp_iogauge.h" // prob dont need, only need if want derivative in phi matrices 

// ASSUME Z4 noise 
void get_noise_vectors(std::vector<complex<double>> &eta, int num_noise, int num_vecs)
{
  // Z4 noise = [1,i,-1,i]
  // FIXME hardwired masking, every other noise vec has a mask a vs mask b
  // mask a: (0,*,0,*,...)
  // mask b: (*,0,*,0,...)
  std::vector<std::complex<double>> Z4;
  Z4.push_back(std::complex<double>(1.0,0.0));
  Z4.push_back(std::complex<double>(0.0,1.0));
  Z4.push_back(std::complex<double>(-1.0,0.0));
  Z4.push_back(std::complex<double>(0.0,-1.0));
  
  int num_unmasked = (int)(num_noise/2);
  std::vector<complex<double>> eta_unmasked(num_unmasked*num_vecs);

  // 
  // eta[n * num_evecs + i], ith component of nth noise vector, i = 0, ..., Nvec -1
  // i (LapH index) varies faster 
  // num col = num_evecs
  
  for(int n=0; n<num_unmasked; n++){
    for(int i=0; i<num_vecs; i++){
      int choice = rand() % 4;
      eta_unmasked[n * num_vecs + i] = Z4[choice];
    }
  }
  
  int num_noise_half = (int)(num_noise/2);

  // first half start with 0, even i are 0
  for(int n=0; n<num_noise_half; n++){
    for(int i=0; i<num_vecs; i++){
      if(i%2 == 0)
        eta[n* num_vecs + i] = std::complex<double>(0.0,0.0);
      else
        eta[n* num_vecs + i] = eta_unmasked[n * num_vecs + i];
    }
  }

  // second half start with *, odd i are 0
  for(int n=0; n<num_noise_half; n++){
    for(int i=0; i<num_vecs; i++){
      if(i%2 == 1)
        eta[(n + num_noise_half)* num_vecs + i] = std::complex<double>(0.0,0.0);
      else
        eta[(n + num_noise_half)* num_vecs + i] = eta_unmasked[n * num_vecs + i];
    }
  }


} // get_noise_vectors













#endif
