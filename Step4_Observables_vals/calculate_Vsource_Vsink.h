#ifndef MULT_BARYON_H
#define MULT_BARYON_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
#include <unsupported/Eigen/CXX11/Tensor>
#include "get_perams.h"
#include "get_evecs.h"
#include "get_perams.h"
#include "mult_pion.h"
using namespace Eigen;
using namespace std; 
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;



double levi_civita(vector<int> &epsilon){
  int diff = 1;
  int a = epsilon[0];
  int b = epsilon[1];
  int c = epsilon[2];
  int d = epsilon[3];
  if(a==b || a==c || a==d || b==c || b==d || c==d){
    diff = 0;
  } // end if all different 
  if(diff != 0){
    int cnt=0;
    for(int i=0;i<epsilon.size();i++){
      for(int j=i+1;j<epsilon.size();j++){
        if (epsilon[i]>epsilon[j]) cnt++;
      } // j
    } // i 
    // even -> (0 - 0.5)*-2 = 1
    // odd -> (1 -0.5)*-2 = -1
    return -2.0*((double)(cnt%2)-0.5);
  } 
  else{
    return 0.0;
  }
}


void get_V_source(Eigen::Tensor<std::complex<double>,4> &V_source,
                  vector<MatrixXcd> &v, 
                  int p, int Nvec){
  std::cout << "setting zero" << std::endl;
  V_source.setZero();
  // time loop
  int t=0;
  for(int a=0; a<Nc; a++){
    std::cout << "a = " << a << "/" << Nc << std::endl;
    for(int b=0; b<Nc; b++){
      for(int c=0; c<Nc; c++){
        for(int d=0; d<Nc; d++){
          vector<int> epsilon;
          epsilon.push_back(a);
          epsilon.push_back(b);
          epsilon.push_back(c);
          epsilon.push_back(d);
          complex<double> e = complex<double>(levi_civita(epsilon), 0.0);
          if(e != complex<double>(0.0, 0.0)){
            // *
            // position loop 
            for(int x=0; x<Lx*Lx*Lx; x++){
              // distillation loop 
              for(int i=0; i<Nvec; i++){
                for(int j=0; j<Nvec; j++){
                  for(int k=0; k<Nvec; k++){
                    for(int l=0; l<Nvec; l++){
                      // p = 0, no phase 
                      V_source(i,j,k,l) += e * conj(v[t](i, v_ind(a,x))) 
                                             * conj(v[t](j, v_ind(b,x))) 
                                             * conj(v[t](k, v_ind(c,x))) 
                                             * conj(v[t](l, v_ind(d,x)));
                    } // l
                  } // k 
                } // j 
              } // i 
            } // x 
          } // if 
        } // d
      } // c
    } // b
  } // a
} // get_V_source



void  get_V_sink(std::vector<Eigen::Tensor<std::complex<double>,4>> &V_sink, 
                    vector<MatrixXcd> &v, 
                    int p, int Nvec){
  for(int t=0; t<Nt; t++){
    std::cout << "t = " << t << "/" << Nt << std::endl;
    Eigen::Tensor<std::complex<double>,4> V(Nvec,Nvec,Nvec,Nvec);
    V.setZero();
    for(int a=0; a<Nc; a++){
      std::cout << "    a = " << a << "/" << Nc << std::endl;
      for(int b=0; b<Nc; b++){
        for(int c=0; c<Nc; c++){
          for(int d=0; d<Nc; d++){
            vector<int> epsilon;
            epsilon.push_back(a);
            epsilon.push_back(b);
            epsilon.push_back(c);
            epsilon.push_back(d);
            complex<double> e = complex<double>(levi_civita(epsilon), 0.0);
            if(e != complex<double>(0.0, 0.0)){
              // position loop 
              for(int x=0; x<Lx*Lx*Lx; x++){
                // distillation loop 
                for(int i=0; i<Nvec; i++){
                  for(int j=0; j<Nvec; j++){
                    for(int k=0; k<Nvec; k++){
                      for(int l=0; l<Nvec; l++){
                        // p = 0, no phase 
                        V(i,j,k,l) += e * (v[t](i, v_ind(a,x))) 
                                        * (v[t](j, v_ind(b,x))) 
                                        * (v[t](k, v_ind(c,x))) 
                                        * (v[t](l, v_ind(d,x)));
                      } // l
                    } // k 
                  } // j 
                } // i 
              } // x 
            } // if 
          } // d
        } // c
      } // b
    } // a
    V_sink.push_back(V);
  } // t 
} // T_blocks sink










#endif 
