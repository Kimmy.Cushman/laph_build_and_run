#ifndef MULT_BARYON_UDUD_H
#define MULT_BARYON_UDUD_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
#include <unsupported/Eigen/CXX11/Tensor>
#include "get_perams.h"
#include "get_evecs.h"
#include "get_perams.h"
#include "mult_pion.h"
#include "calculate_Vsource_Vsink.h"
using namespace Eigen;
using namespace std; 
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;

// O = (u Gamma d) (u Gamma d), EXPLICITLY (u_alpha Gamma_alpha,beta d_beta) (u_sigma Gamma_sigma,delta d_delta)
// O^t = (d' Gamma^t' u') (d' Gamma'^t u') EXPLICITLY (d_delta' Gamma_delta'sigma' u_sigma') (d_beta' Gamma_beta'alpha' u_alpha')

// Left diagrams visualized is usual quark ordering from top to bottom, but 
// FIXME
// C(t) = llll - llX - Xll + XX
// u -- u       u -- u
// d -- d       u -- u  llll term
// u -- u       d -- d
// d -- d       d -- d

// u -- u       u -- u
// d \/ d       u -- u  llX term, contributes with - sign due to Wick
// u -- u       d \/ d
// d /\ d       d /\ d

// u \/ u       u \/ u
// d -- d       u /\ u  Xll term, contributes with - sign due to Wick 
// u /\ u       d -- d
// d -- d       d -- d

// u \/ u       u \/ u
// d \/ d       u /\ u  XX term
// u /\ u       d \/ d
// d /\ d       d /\ d

// O = (u Gamma d) (u Gamma d), EXPLICITLY (u_alpha Gamma_alpha,beta d_beta) (u_sigma Gamma_sigma,delta d_delta)
// O^t = (d' Gamma^t' u') (d' Gamma'^t u') EXPLICITLY (d_delta' Gamma_delta'sigma' u_sigma') (d_beta' Gamma_beta'alpha' u_alpha')
//
// source indices are primed, sink is unprimed, 
// for example, perambulator is indeces tau_ii'^alpha,alpha'
//
// C(t) = Vsink[ijkl] * Vsource[i'j'k'l'] 
// * Gamma[alpha,beta] 
// * Gamma[sigma,delta] 
// * Gamma^t[beta',alpha']
// * Gamma^t[delta',sigma']
// * (   tau[i alpha, i' alpha'] 
//     * tau[j beta, j' beta'] 
//     * tau[k sigma, k' sigma'] 
//     * tau[l delta, l' delta'] 
//   + other 3 diagrams )
// -> C(t) = llll - llX - Xll + XX
//
// Compute traces. See spin_traces comments and function 
// llll = Vsink[ijkl] * Vsource[i'j'k'l'] * spinTrace2[ii' jj'] * spinTrace2[kk' ll']
// XX   = Vsink[ijkl] * Vsource[i'j'k'l'] * spinTrace2[ik' jl'] * spinTrace2[ki' lj']
// llX  = Vsink[ijkl] * Vsource[i'j'k'l'] * spinTrace4[ii' jl' kk' lj']
// Xll  = Vsink[ijkl] * Vsource[i'j'k'l'] * spinTrace4[ik' jj' ki' ll']

// Contract source into traces. See 
// llll = Vsink[ijkl] * B_connect_llll [ijkl],
// XX   = Vsink[ijkl] * B_connect_llll [ijkl]
// llX  = Vsink[ijkl] * B_connect_llX [ijkl]
// Xll  = Vsink[ijkl] * B_connect_llX [ijkl]


// ( ************* spin Trace function ************* ) //
// notation: (aa') = tau[alpha,alpha'] goes with (ii') due to tau = q qbar
//
// lll term : spinTrace2{ii'jj'} * spinTrace2{kk'll'}
// llll term (aa')(bb')(cc')(dd') G_ab G_b'a' G_cd G_d'c' 
//       = Tr[aa' (G_b'a')T (bb')T (G_ab)T ] * Tr[ cc' (G_d'c')T (dd')T (G_cd)T ]
//
// XX term : spinTrace2{ik'jl'} * spinTrace2{ki'lj'}
// XX term (ac')(bd')(ca')(db') G_ab G_b'a' G_cd G_d'c'
//      = Tr[ac' (G_d'c')T (bd')T (G_ab)T] * Tr[ca' (G_b'a')T (db')T (G_cd)T]
// 
//    * labeling doesn't matter since both traces given by 
//    * term = Tr[tau G^T tau^T G^T] * Tr[tau G^T tau^T G^T]
//    * so use llll indices in loop below

// Xll term : spinTrace4{ik' jj' ki' ll'}
// Xll term (ac')(bb')(ca')(dd') G_ab G_b'a' G_cd G_d'c' 
//       = Tr[ac' (G_d'c')T (dd')T (G_cd)T ca' (G_b'a')T (bb')T (G_ab)T]
//
// llX term: spinTrace4{ii' jl' kk' lj'}
// llX term (aa')(bd')(cc')(db') G_ab G_b'a' G_cd G_d'c' 
//       = Tr[aa' (G_b'a')T (db')T (G_cd)T cc' (G_d'c')T (bd')T (G_ab)T]
//
//   * labeling doesn't matter since both traces given by 
//   * term = Tr[tau G^T tau^T G^T tau G tau^T G^T]
//   * so use llX term indices in loop below
// ///////////////////////////////////////////////////////////////////////////////////


// ( ************* B_connect function ************* ) //
// with 
// B_connect_llll [ijkl] = sum_{i'j'k'l'} Vsource[i'j'k'l'] * spinTrace2[ii' jj'] * spinTrace2[kk' ll']
//                       == B_connect_llll[ijkl] (definition)
// B_connect_XX [ijkl]   = sum_{i'j'k'l'} Vsource[i'j'k'l'] * spinTrace2[ik' jl'] * spinTrace2[ki' lj']
// -> B_connect_XX[klij] = sum_{i'j'k'l'} Vsource[i'j'k'l'] * spinTrace2[kk' ll'] * spinTrace2[ii' jj'] //relabel indices   
//                       = sum_{i'j'k'l'} Vsource[i'j'k'l'] * spinTrace2[ii' jj'] * spinTrace2[kk' ll'] // * is communative 
//                       = B_connect[ijkl]  -- this is a good check to do 

//B_connect_llX [ijkl]   = sum_{i'j'k'l'} Vsource[i'j'k'l'] * spinTrace4[ii' jl' kk' lj'] 
//                       == B_connect_Xll[ijkl] (definition)
//B_connect_Xll [ijkl]   = sum_{i'j'k'l'} Vsource[i'j'k'l'] * spinTrace4[ik' jj' ki' ll']
// ->B_connect_Xll [ijkl]= sum_{k'l'i'j'} Vsource[k'l'i'j'] * spinTrace4[ii' jl' kk' lj'] // relabel dummy indices 
// but Vsource[k l i j ] = sum_x e^{ipx} epsilon^{abcd} v_k(a,x) v_l(b,x) v_i(c,x) v_j(d,x) // definition
//                       = sum_x e^{ipx} epsilon^{abcd} v_i(c,x) v_j(d,x) v_k(a,x) v_l(b,x) // relabel distillation
//                       = sum_x e^{ipx} epsilon^{cdab} v_i(a,x) v_j(b,x) v_k(c,x) v_l(d,x) // relabel color
//                       = sum_x e^{ipx} epsilon^{abcd} v_i(a,x) v_j(b,x) v_k(c,x) v_l(d,x) // even perm
//                       = Vsource[i j k l]
// so
// B_connect_Xll [ijkl]= sum_{k'l'i'j'} Vsource[i'j'k'l'] * spinTrace4[ii' jl' kk' lj'] 
//                     = B_connect_Xll [ijkl]




void get_spinTrace_udud(std::vector<Eigen::Tensor<std::complex<double>,4>> &spinTrace2, 
                        std::vector<Eigen::Tensor<std::complex<double>,8>> &spinTrace4, 
                        std::vector<MatrixXcd> &tau, 
                        int t_source, int Nvec)
{
  gamma_struct gamma5;
  gamma5.name = "g5";
  gamma_map(gamma5);
  
  gamma_struct gammaC;
  gammaC.name = "C";
  gamma_map(gammaC);

  MatrixXcd Gamma = gammaC.gamma_matrix * gamma5.gamma_matrix;
  //std::cout << Gamma(0,0) << " " << Gamma(0,1) << Gamma(0,2) << Gamma(

  for(int t_sink=0; t_sink<Nt; t_sink++){
    std::cout << "t = " << t_sink << std::endl;
    Eigen::Tensor<std::complex<double>,4> T2(Nvec,Nvec,Nvec,Nvec);
    Eigen::Tensor<std::complex<double>,8> T4(Nvec,Nvec,Nvec,Nvec, Nvec,Nvec,Nvec,Nvec);
    T2.setZero();
    T4.setZero();
    for(int alpha=0; alpha<4; alpha++){
      for(int beta=0; beta<4; beta++){
        if (Gamma(alpha,beta) != std::complex<double>(0.0,0.0)){
          for(int alpha_=0; alpha_<4; alpha_++){
            for(int beta_=0; beta_<4; beta_++){
              if (Gamma(beta_,alpha_) != std::complex<double>(0.0,0.0)){
                for(int i=0; i<Nvec; i++){
                  for(int i_=0; i_<Nvec; i_++){
                    for(int j=0; j<Nvec; j++){
                      for(int j_=0; j_<Nvec; j_++){  
                        // Tr[aa' (G_b'a')T (bb')T (G_ab)T ] 
                        complex<double> term2 = tau[tau_ind(t_sink, t_source, alpha, alpha_)](i,i_) 
                                                * Gamma(beta_,alpha_)
                                                * tau[tau_ind(t_sink, t_source, beta, beta_)](j,j_)
                                                * Gamma(alpha,beta);
                        T2(i,i_,j,j_) += term2;
                        for(int sigma=0;  sigma<4;  sigma++){
                          for(int delta=0;  delta<4;  delta++){
                            if (Gamma(sigma,delta) != std::complex<double>(0.0,0.0)){
                              for(int sigma_=0; sigma_<4; sigma_++){
                                for(int delta_=0; delta_<4; delta_++){
                                  if (Gamma(delta_,sigma_) != std::complex<double>(0.0,0.0)){
                                    for(int k=0;  k<Nvec;  k++){
                                      for(int k_=0; k_<Nvec; k_++){
                                        for(int l=0;  l<Nvec;  l++){
                                          for(int l_=0; l_<Nvec; l_++){  
                                            // Tr[aa' (G_b'a')T (db')T (G_cd)T cc' (G_d'c')T (bd')T (G_ab)T]
                                            complex<double> term4 = tau[tau_ind(t_sink, t_source, alpha, alpha_)](i,i_)
                                                                    * Gamma(beta_,alpha_)
                                                                    * tau[tau_ind(t_sink, t_source, delta, beta_)](l,j_)
                                                                    * Gamma(sigma,delta)
                                                                    * tau[tau_ind(t_sink, t_source, sigma, sigma_)](k,k_)
                                                                    * Gamma(delta_,sigma_)
                                                                    * tau[tau_ind(t_sink, t_source, beta, delta_)](j,l_)
                                                                    * Gamma(alpha,beta);
                                            T4(i,i_, l,j_, k,k_, j,l_) += term4;
                                          } // l_
                                        } // l
                                      } // k_
                                    } // k
                                  } // Gamma(delta_,sigma_)    
                                } // delta_  
                              } // sigma_
                            } // Gamma(sigma_delta)
                          } // delta
                        } // sigma
                      } // j_
                    } // j 
                  } // i_
                } // i
              } // if Gamma(beta_,alpha_)
            } // beta_
          } // alpha_
        } // if Gamma(alpha,beta)
      } // beta
    } // alpha 
    spinTrace2.push_back(T2);
    spinTrace4.push_back(T4);
  } // t
} // get_spin_traces



// Connect B_{i'j'k'l'} = V_{ijkl} * spinTrace_{i'ij'j} * spinTrace_{k'kl'l}
// NOTE that B has different indices for each term -> each contracts differently with final V_sink
void B_connect_udud(std::vector<Eigen::Tensor<std::complex<double>,4>> &B_connect, 
                          std::vector<Eigen::Tensor<std::complex<double>,4>> &spinTrace2, 
                          std::vector<Eigen::Tensor<std::complex<double>,8>> &spinTrace4, 
                          Eigen::Tensor<std::complex<double>,4> &V_source, 
                          string term, int Nvec)
{  
  
  Eigen::array<Eigen::IndexPair<int>, 2> ind1;
  Eigen::array<Eigen::IndexPair<int>, 2> ind2;
  Eigen::array<Eigen::IndexPair<int>, 4> ind4;
  
  for(int t=0; t<Nt; t++){ 
  
    // Temp = V * spinTrace
    Eigen::Tensor<complex<double>,4> Temp(Nvec,Nvec,Nvec,Nvec); 
    Temp.setZero();
    // B = Temp * spinTrace
    Eigen::Tensor<complex<double>,4> B(Nvec,Nvec,Nvec,Nvec);  
    B.setZero();
    
    if(term=="llll" or term=="XX"){
      // B_connect_llll [ijkl] = sum_{i'j'k'l'} Vsource[i'j'k'l'] * spinTrace2[ii' jj'] * spinTrace2[kk' ll']
      // Temp{k'l'ij} = sum{i'j'} V_source[i'j'k'l'] * spinTrace2[ii' jj']
      ind1 = {Eigen::IndexPair<int>(0,1), Eigen::IndexPair<int>(1,3)};
      // B{ijkl} = sum{k'l'} Temp{k'l'ij} * spinTrace2[kk' ll']
      ind2 = {Eigen::IndexPair<int>(0,1), Eigen::IndexPair<int>(1,3)};
      
      Temp = V_source.contract(spinTrace2[t], ind1);
      B = Temp.contract(spinTrace2[t], ind2);
    }
    if(term=="llX" or term=="Xll"){
      //B_connect_llX [ijkl]   = sum_{i'j'k'l'} Vsource[i'j'k'l'] * spinTrace4[ii' jl' kk' lj'] 
      // B{ijkl} = sum_{i'j'k'l' }Vsource{i'j'k'l'} * spinTrace4[ii' jl' kk' lj']
      ind4 = {Eigen::IndexPair<int>(0,1), 
              Eigen::IndexPair<int>(1,7),
              Eigen::IndexPair<int>(2,5),
              Eigen::IndexPair<int>(3,3)};
      B = V_source.contract(spinTrace4[t], ind4);
    }
    B_connect.push_back(B);
  } // t
} // B_connect 


void baryon_udud(std::vector<complex<double>> &corr, 
                 std::vector<MatrixXcd> &v, 
                 std::vector<MatrixXcd> &tau, 
                 int p, 
                 int t_source,
                 string corr_savename_dir,
                 string cfg_num, int Nvec)
{
  // Vsource_{i'j'k'l'} = sum_{x} \sum_{a'b'c'd'} v_i'* v_j'* v_k'* v_l'*
  Eigen::Tensor<complex<double>,4> V_source(Nvec,Nvec,Nvec,Nvec);
  get_V_source(V_source, v, p, Nvec);  

  // Vsink_{i'j'k'l'}(t) = sum_{x} \sum_{abcd} v_i v_j v_k v_l
  std::vector<Eigen::Tensor<complex<double>,4>> V_sink;
  get_V_sink(V_sink, v, p, Nvec);
  
  // Traces
  // spinTrace{ii'jj'} = Gamma * Gamma * tau^{alpha,alpha'}_{ii'} tau^{beta,beta'}_{jj'}
  // see function for breakdown of four terms 
  cout << "\n\nGetting spin Traces" << endl;
  std::vector<Eigen::Tensor<std::complex<double>,4>> spinTrace2; 
  std::vector<Eigen::Tensor<std::complex<double>,8>> spinTrace4; 
  get_spinTrace_udud(spinTrace2, spinTrace4, tau, t_source, Nvec);


  // Connect B_llll{ijkl} = sum_{i'j'k'l'} Vsource_{i'j'k'l'} * spinTrace_{ii'jj'} * spinTrace_{kk'll'}
  cout << "\n\nGetting B connect" << endl;
  std::vector<Eigen::Tensor<std::complex<double>,4>> B_connect_llll;
  std::vector<Eigen::Tensor<std::complex<double>,4>> B_connect_llX;
  B_connect_udud(B_connect_llll, spinTrace2, spinTrace4, V_source, "llll", Nvec);
  B_connect_udud(B_connect_llX,  spinTrace2, spinTrace4, V_source, "llX", Nvec);
  
  // term = V_sink(ijkl) * B_llll(ijkl)
  Eigen::array<Eigen::IndexPair<int>, 4> ind = {Eigen::IndexPair<int>(0,0),   
                                                Eigen::IndexPair<int>(1,1),   
                                                Eigen::IndexPair<int>(2,2),   
                                                Eigen::IndexPair<int>(3,3)};
  
  for(int t=0; t<Nt; t++){
    //std::cout << "B_connect_0[" << t << "](0,1,2,3)" << std::endl;
    //std::cout << B_connect_0[t](0,1,2,3) << std::endl;
    Eigen::Tensor<std::complex<double>,0> llll = V_sink[t].contract(B_connect_llll[t], ind, Nvec); 
    Eigen::Tensor<std::complex<double>,0> XX   = V_sink[t].contract(B_connect_llll[t],   ind, Nvec); 
    Eigen::Tensor<std::complex<double>,0> llX  = V_sink[t].contract(B_connect_llX[t],  ind, Nvec); 
    Eigen::Tensor<std::complex<double>,0> Xll  = V_sink[t].contract(B_connect_llX[t],  ind, Nvec); 
    cout << "llll(" << t << ") = " << llll(0) << endl;
    cout << "  XX(" << t << ") = " << XX(0)   << endl;
    cout << " llX(" << t << ") = " << llX(0)  << endl;
    cout << " Xll(" << t << ") = " << Xll(0)  << endl;
    corr[t] = llll(0) + XX(0) - llX(0) - Xll(0);
    cout << "C(t=" << t<< ") = " <<  corr[t] << "\n\n" <<  endl;
  } // t
  
  string corr_savename = corr_savename_dir + "/baryon_udud_cfg"  + cfg_num + ".txt";
  ofstream myfile;
  myfile.open(corr_savename); 
  cout << "\n\n" << corr_savename << "\n\n" << endl;
  for(int t=0; t < Nt; t++){
    cout << "C(" << t << ") = " << corr[t] << endl;
    myfile << real(corr[t]) << "," << imag(corr[t]) << "\n";
  }
  myfile.close();
  

} // baryon 
                  

  













#endif 
