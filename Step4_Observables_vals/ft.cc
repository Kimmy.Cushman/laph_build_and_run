#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>
#include "FTensor.hpp"
using namespace FTensor;

int main(){

  /// ****************** constants  **************** ///
  const int N = 3;
  Index<'i',2> i2;
  Index<'j',2> j2;
  Index<'k',2> k2;
  
  Index<'i', 2> iN;
  Index<'j', N> jN;
  Index<'k', N> kN;
  Index<'l', N> lN;
  Index<'m', N> mN;
  Index<'n', N> nN;
  Index<'o', N> oN;
  Index<'p', N> pN;

  
  /// ****************** Declare tensors **************** ///
  
  // simple 2x2
  Tensor2<double, 2, 2> A2;
  Tensor2<double, 2, 2> B2;
  Tensor2<double, 2, 2> C2;
  Tensor2<double, 2, 2> C2_loop;
  
  // rank 3 sample
  Tensor3<double, N, N, N> A3;
  Tensor3<double, N, N, N> B3;
  Tensor3<double, N, N, N> C3;
  Tensor3<double, N, N, N> C3_loop;

  // rank 4 full problem 
  Tensor4<double,N,N,N,N> V;
  Tensor4<double,N,N,N,N> T1;
  Tensor4<double,N,N,N,N> T2;
  Tensor4<double,N,N,N,N> Temp;
  Tensor4<double,N,N,N,N> Temp_loop; 
  Tensor4<double,N,N,N,N> B;
  Tensor4<double,N,N,N,N> B_loop;
  
  Tensor1<double, 1> Sum;
  


  /// ****************** fill tensors **************** ///
  
  // Rank 2 
  A2(0,0) = 1.0;
  A2(0,1) = 1.5;
  A2(1,0) = 1.7;
  A2(1,1) = 1.8;
  
  B2(0,0) = 1.2;
  B2(0,1) = 1.3;
  B2(1,0) = 2.0;
  B2(1,1) = 2.2;
  
  // Rank 3+
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      for(int k=0; k<N; k++){
        double a3 = (double) rand()/RAND_MAX;
        double b3 = (double) rand()/RAND_MAX;
        A3(i,j,k) = a3;
        B3(i,j,k) = b3;
        for(int l=0; l<N; l++){
          double a = (double) rand()/RAND_MAX;
          double b = (double) rand()/RAND_MAX;
          double c = (double) rand()/RAND_MAX;
          V(i,j,k,l) = a;
          T1(i,j,k,l) = b;
          T2(i,j,k,l) = c;
        }
      }
    }
  }

  /// ****************** loop calculate **************** ///
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      if(i<2 && j<2){
        Sum(0) += A2(i,j) * B2(i,j);
      }
      for(int k=0; k<N; k++){
        for(int l=0; l<N; l++){   
          for(int i_=0; i<N; i++){
            for(int j_=0; j<N; j++){
              Temp_loop(k,l,i_,j_) += V(i,j,k,l) * T1(i_,i,j_,j);
              for(int k_=0; k<N; k++){
                for(int l_=0; l<N; l++){
                  B_loop(i_,j_,k_,l_) += V(i,j,k,l) * T1(i_,i,j_,j) * T2(k_,k,l_,l);
                }
              }
            }
          }
        }
      }
    }
  }

  
  /// ****************** ft calculate **************** ///
  C3(iN,jN,kN) = A3(iN,jN,kN) * B3(iN,jN,kN); // ( , , ) ( , , )
  //C3(iN,jN,kN) = A3(iN,jN,lN) * B3(lN,jN,kN); // ( , ,O) (O, , )  DOES NOT WORK
  //C3(iN,jN,kN) = A3(iN,jN,lN) * B3(iN,kN,lN); // ( , ,O) ( , ,O) DOES NOT WORK
  
  C3(iN,jN,kN) = A3(iN,jN,lN) * B3(kN,iN,lN);   // ( , ,O) ( , ,O) does not work
  //C3(iN,jN,kN) = A3(iN,jN,kN) * B3(iN,kN,jN); // ( , , ) ( ,( , )^T) does not work
  

  //Temp_ft(k,l,m,n) = V(i,j,k,l) * T1(m,i,n,j); //  no
  //Temp_ft(i,j,k,l) = V(i,j,k,l) * T1(i,j,k,l); //  no
  
  //Temp_sum = V(m,n,o,p) * T1(m,n,o,p); // 
  //Temp1(l) = V(l,m,n,o) * T1(l,m,n,o); // 
  //Temp2(n,m) = V(l,n,o,p) * T1(l,m,o,p); 
  
   
  Sum = A2(i2,j2) * B2(i2,j2);

  /// ****************** CHECK **************** ///
  /*
  double diff = 0.0;
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      for(int k=0; k<N; k++){
        for(int l=0; l<N; l++){
          //diff += B(i,j,k,l) - B_eigen(i,j,k,l); 
          diff += Temp(i,j,k,l) - Temp_eigen(i,j,k,l);
        }
      }
    }
  }
  double d = diff / (N*N*N*N);
  std::cout << d << std::endl;
  */
  

  return 0;

}
