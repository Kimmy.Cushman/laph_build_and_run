#ifndef MULT_PION_H
#define MULT_PION_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
#include "get_perams.h"
#include "get_evecs.h"
#include "spin_matrices.h"
using namespace Eigen;
using namespace std; 
using std::vector;
using std::ifstream;
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;


void conj_tau(std::vector<MatrixXcd> &tau, int t_source, int Nvec){
  // first initialize size and shape of tau_conj 
  gamma_struct gamma5;
  gamma5.name = "g5";
  gamma_map(gamma5);
  for(int t_sink=0; t_sink<Nt; t_sink++){
    for(int alpha=0; alpha < 4; alpha ++){
      for(int delta=0; delta < 4; delta ++){
        if(gamma5.gamma_matrix(alpha, delta) != complex<double>(0.0,0.0)){
          for(int beta=0; beta < 4; beta ++){
            for(int sigma=0; sigma < 4; sigma ++){
              if(gamma5.gamma_matrix(sigma, beta) != complex<double>(0.0,0.0)){
                complex<double> factor =    gamma5.gamma_matrix(alpha, delta)
                                          * gamma5.gamma_matrix(sigma, beta);
                for(int i=0; i < Nvec; i++){
                  for(int j=0; j <Nvec; j++){
                    int get_element = tau_ind(t_sink, t_source, sigma, delta); 
                    int set_element = tau_ind(t_source, t_sink, alpha, beta);  
                    tau[set_element](i,j) += factor * conj(tau[get_element](j,i));
                  } // end j loop
                } // end i loop
              } // end if delta sigma
            } // end sigma loop
          } // end delta loop
        } // end if alpha beta
      } // end beta loop
    } // end alpha loop
  } // end t_sink loop 

  // CHECK that tau(t,t') = tau(t',t) when t'=t
  int t_sink = t_source;
    complex<double> norm;
    for(int alpha=0; alpha < 4; alpha ++){
      for(int beta=0; beta < 4; beta ++){
        for(int i=0; i < Nvec; i++){
          for(int j=0; j <Nvec; j++){
            norm += abs(tau[tau_ind(t_source, t_sink, alpha, beta)](i,j) - tau[tau_ind(t_sink, t_source, alpha, beta)](i,j));
          }// j
        } // i
      } // beta
    } // alpha 
    norm = norm/((double)(4*4*Nvec*Nvec));
    if(norm.real() > 1e-8)
      cout << "\n tau_conj INCORRECT \n\n NORM :" << norm << "\n\n\n"<< endl;
  
} // conj_tau 


void get_point_phi(vector<MatrixXcd> &evectors, vector<MatrixXcd> &phi, int x, int y, int z, int Nvec){
 
  for(int t=0; t<Nt; t++){
    MatrixXcd phi_t = evectors[t].conjugate() * evectors[t].transpose();
    phi.push_back(phi_t);
  }

  /* // use if do not want to project onto zero momentum at source 
  for(int i=0; i<Nvec; i++){
    for(int j=0; j<Nvec; j++){
      for(int a=0; a<Nc; a++){
        phi(i,j) += conj(evector(i, v_ind(a, x*Lx*Lx + y*Lx + z))) * evector(j, v_ind(a, x*Lx*Lx + y*Lx + z));
      } // color
    } // j 
  } // i 
  */
} // get point phi


void pion(vector<complex<double>> &corr, 
          vector<MatrixXcd> &evectors, 
          std::vector<MatrixXcd> &tau, 
          int t_source,
          string corr_savename_dir, 
          string cfg_num, 
          string meson_state, int Nvec)
{
  gamma_struct gamma;
  gamma.name = meson_state; 
  gamma_map(gamma);

  std::cout << meson_state << " Gamma Matrix" << std::endl;
  std::cout << gamma.gamma_matrix << std::endl;

  int x = 0;
  int y = 0; 
  int z = 0;
  vector<MatrixXcd> phi;
  get_point_phi(evectors, phi, x, y, z, Nvec);
    
  // calculation tau(t_source, t_sink) from tau(t_sink, t_source) 
  conj_tau(tau, t_source, Nvec);
  
  // t loop 
  for(int t_sink=0; t_sink<Nt; t_sink++){
    cout << "t = " << t_sink << endl;
    // Dirac loops 
    for(int alpha = 0; alpha < 4; alpha++){
      for(int beta = 0; beta < 4; beta++){
        if(gamma.gamma_matrix(alpha, beta) != complex<double>(0.0, 0.0)){
          for(int alpha_ = 0; alpha_ < 4; alpha_++){
            for(int beta_ = 0; beta_ < 4; beta_++){
              if(gamma.gamma_matrix(beta_, alpha_) != complex<double>(0.0, 0.0)){
                // Distillation loops 
                for(int i=0; i<Nvec; i++){
                  for(int i_=0; i_<Nvec; i_++){
                    for(int j=0; j<Nvec; j++){
                      for(int j_=0; j_<Nvec; j_++){
                        
                        //std::cout << phi[t_sink](i,j) << endl;
                        //std::cout << gamma.gamma_matrix(alpha, beta) << endl;
                        //std::cout << tau[tau_ind(t_sink, t_source, beta, beta_)](j, j_) << endl << endl;

                        complex<double> term = phi[t_sink](i, j) * phi[t_source](j_, i_)
                                               * gamma.gamma_matrix(alpha, beta)
                                               * gamma.gamma_matrix(beta_, alpha_)
                                               * tau[tau_ind(t_source, t_sink, alpha_, alpha)](i_,i)  // tau(t,0)
                                               * tau[tau_ind(t_sink, t_source, beta, beta_)](j,j_); // tau(0,t)
                        //std::cout << term << endl;
                        corr[t_sink] += term;      
                      } // end j loop 
                    } // end j_ loop
                  } // end i_
                } // end i loop
              } // end if beta_ alpha_ != 0
            } // end beta_ loop
          } // end alpha_ loop 
        } // end if alpha beta != 0
      } // end beta loop
    } //  end alpha loop
  } // end tsink loop
  

  string corr_savename = corr_savename_dir + "/" + meson_state + "meson_cfg" + cfg_num + ".txt";
  ofstream myfile;
  myfile.open(corr_savename); 
  cout << "\n\n" << corr_savename << "\n\n" << endl;
  for(int t=0; t < Nt; t++){
    cout << "C(" << t << ") = " << corr[t] << endl;
    myfile << real(corr[t]) << "," << imag(corr[t]) << "\n";
  }
  myfile.close();
} // pion 




#endif
