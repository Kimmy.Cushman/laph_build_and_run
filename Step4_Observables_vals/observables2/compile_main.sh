#!/bin/bash -l


# delete the executable
exe="main_Nc$1.exe"
rm ${exe}

Nc=$1 
Lx=$2
Nt=$3
Nvec=$4
echo "#define Lx ${Lx}" > global_variables.h
echo "#define Nt ${Nt}" >> global_variables.h
echo "#define Nd 4" >> global_variables.h
echo "#define Nvec ${Nvec}" >> global_variables.h
echo "#define Nc ${Nc}" >> global_variables.h

echo "C O M P I L I N G"

Iqdpxx=/p/gpfs1/cushman2/code/Nc${1}_Nd3_install/qdpxx_lapH/include
Ilibxml=/p/gpfs1/cushman2/code/base_install/libxml2/include/libxml2
Iqmp=/p/gpfs1/cushman2/code/base_install/qmp/include/
#Imult=./Mult/
Iarpack=/p/gpfs1/cushman2/code/source/arpackpp/include/


Lqdpxx=/p/gpfs1/cushman2/code/Nc${1}_Nd3_install/qdpxx_lapH/lib/
Llibxml=/p/gpfs1/cushman2/code/base_install/libxml2/lib
Lqmp=/p/gpfs1/cushman2/code/base_install/qmp/lib

Ieigen=/p/gpfs1/cushman2/code/source/eigen
Lchroma=../../code/Nc${1}_Nd4_install/chroma_git_Nc/lib/




mpicxx -std=c++14 -O2 -fopenmp -Wno-deprecated -w -I${Iqdpxx} -I${Ilibxml} -I${Iqmp} -I${Ieigen} main.cpp -lgfortran -L${Lchroma} -L${Lqmp} -L${Lqdpxx} -L${Llibxml} -lchroma -lqdp -lXPathReader -lxmlWriter -lqio -llime -lxml2 -lm -lqmp -lfiledb -lfilehash -o main_Nc${1}.exe


