#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <unsupported/Eigen/CXX11/Tensor>

#include "qdp.h" // need 
#include "qdp_map_obj.h"  // from -I QDP
#include "qdp_map_obj_disk.h" // from -I QDP 
#include "qdp_map_obj_disk_multiple.h" // from -I QDP (maybe not needed now, need if perambs stored in multiple files) 
#include "qdp_db.h" // source of a bunch of warnings! (silenced) // need
#include "qdp_iogauge.h" // prob dont need, only need if want derivative in phi matrices 
#include "db_codes/key_prop_matelem.h"// 

#include "get_evecs.h"
#include "get_perams.h"
#include "mult_pion.h"
//#include "mult_baryon.h"
#include "mult_baryon_tensor.h"
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;
using namespace std;
int main(int argc, char *argv[]){
 
    START_CODE();
    // Put the machine into a known state
    QDP_initialize(&argc, &argv);
   
    std::cout << "   S T A R T   C O D E   " << std::endl;
    std::string perambulator_filename = argv[1];
    std::string fnames_base = argv[2];
    cout << "\n\n" << fnames_base << "\n\n"  << endl;
    int t_stop = atoi(argv[3]); // evecs ASSUMING  perams for t = 0, 1, 2, ..., t_stop -1
    
    std::string corr_savename = argv[4];
    std::string state = argv[5];

    std::vector<int> t_source_list;
    t_source_list.push_back(0);


    std::vector<int> t_sink_list;
    std::vector<int> t_all_list;
    int num_t_all = t_stop;
    for(int t; t < t_stop; t++){
      t_sink_list.push_back(t);
      t_all_list.push_back(t);
    }
      
    // load perambulators 
    std::vector<MatrixXcd> tau;
    get_tau(perambulator_filename, tau);
     
    // load eigen vectors 
    vector<string> fnames(num_t_all);
    for(auto t : t_all_list){
      std::ostringstream oss;
      oss <<  fnames_base + "t" << t << ".db";
      fnames[t] = oss.str();
    }
    
    vector<MatrixXcd> evectors;
    //MatrixXcd<MatrixXcd> evectors_spatial;
    get_evecs(fnames, evectors, t_all_list);
   

    // compute correlation functions! 
    if(state == "baryon"){
      vector<complex<double>> corr(t_stop);
      int p = 0;
      baryon(corr, evectors, tau, p, t_stop);    
    }
    

    // Compute pion and save to file
    // compute correlation functions! 
    if(state == "pion"){
      vector<complex<double>> corr(t_stop);
      pion(corr, evectors, tau, t_stop);

      // get corr file for reading 
      ofstream myfile;
      myfile.open(corr_savename);
      // Read Correlation function result 
      for(int t=0; t < t_stop; t++){
        cout << "C(" << t << ") = " << corr[t] << endl;
        myfile << real(corr[t]) << "," << imag(corr[t]) << "\n";
      }
      myfile.close();
    }

    std::cout << "   E N D  C O D E   ---   S U C C E S S !" << std::endl;
    return 0;
}
























