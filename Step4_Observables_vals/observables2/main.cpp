#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>

#include "qdp.h" // need 
#include "qdp_map_obj.h"  // from -I QDP
#include "qdp_map_obj_disk.h" // from -I QDP 
#include "qdp_map_obj_disk_multiple.h" // from -I QDP (maybe not needed now, need if perambs stored in multiple files) 
#include "qdp_db.h" // source of a bunch of warnings! (silenced) // need
#include "qdp_iogauge.h" // prob dont need, only need if want derivative in phi matrices 
#include "db_codes/key_prop_matelem.h"// 

#include "get_evecs.h"
#include "get_perams.h"
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;
using namespace std;
int main(int argc, char *argv[]){
 
    START_CODE();
    // Put the machine into a known state
    QDP_initialize(&argc, &argv);
   
    std::cout << "   S T A R T   C O D E   " << std::endl;
    std::string perambulator_filename = argv[1];
    std::string fnames_base = argv[2];
    int t_stop = atoi(argv[3]); // evecs ASSUMING  perams for t = 0, 1, 2, ..., t_stop -1
    std::vector<int> t_source_list;
    t_source_list.push_back(0);


    std::vector<int> t_sink_list;
    std::vector<int> t_all_list;
    int num_t_all = t_stop;
    for(int t; t < t_stop; t++){
      t_sink_list.push_back(t);
      t_all_list.push_back(t);
    }
  
    map<string, phi_struct> phi_map;
    
    // load perambulators 
    tau_struct tau;
    get_tau(perambulator_filename, tau);
    
    // load eigen vectors 
    vector<string> fnames(num_t_all);
    for(auto t : t_all_list){
      std::ostringstream oss;
      oss <<  fnames_base + "t" << t << ".db";
      fnames[t] = oss.str();
    }
    vector<v_dist_struct> evectors;
    get_evecs(fnames, evectors, t_all_list);
   

    std:: cout << "\n\n\n\n\n" << " E X A M P L E  R E A D I N G  A R R A Y S" << "\n\n\n\n" << std::endl;

    /*
    /////////////// Read evecs example ///////////////
    for(auto t: t_all_list){// t!=Nt_all; t++){
      // loop over distill vecs, find key value (t, distill_vec)
      for(int vec_num=0; vec_num< Nvec; ++vec_num){
        std::cout << "getting vector at t = " << t << ", vec num i = " << vec_num << std::endl;
        // loop over spatial points
        for(int site=0; site < Lx*Lx*Lx; site++){
          // loop over color 
          for(int color=0; color < Nc; color++){
            std::cout << "t=" << t << ", i=" << vec_num << ", site=" << site << "color=" << color 
            << ", value=" <<evectors[t].v_dist[vec_num](v_ind(color,site)) << std::endl;;
          } // end loop over color 
        } // end loop over sites
      } // end loop over Dist evecs 
    } // end loop over timeslices 
    */

    /*
    /////////// Read perambulators example /////////// 
    for(auto tsink: t_sink_list){
      std::vector<int> t_source_perambulator; 
      // ASSUMES t_source_list = [0], but need 
      // tau(t_source, t_source) tau(t_source, t_sink), tau(t_sink, t_sink)
      // for connected and disconnected parts 
      t_source_perambulator.push_back(0);
      t_source_perambulator.push_back(tsink);   // NOTE tau(t_sink, t_sink) = 0 for all t_sink right now, 
                                                // need to fix this back in the perambulator code
      for(auto tsource: t_source_perambulator){
        for(int alpha=0; alpha<4; alpha++){
          for(int beta=0; beta<4; beta++){
            int index = tau_ind(tsource, tsink, alpha, beta);
            // begin loop over j,k distillation indices 
            for(int j=0; j<Nvec; j++){
              for(int k=0; k<Nvec; k++){
                std::cout << "tsource=" << tsource << ", tsink=" << tsink << ", j=" << j <<  ", k=" << k
                << ", value=" << tau.tau_matrix[index](j,k) << std::endl;
              } // end k loop
            } // end j loop
          } // end beta loop
        } // end alpha loop
      } // end tsource loop
    } // end tsink loop 
    */

    std::cout << "   E N D  C O D E   ---   S U C C E S S !" << std::endl;

    return 0;
}
























