#ifndef GET_EVECS_H
#define GET_EVECS_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
#include "../Step2_Distill_vecs/Data_wrangling/read_write.h"   // needed for DBKey and DBVal 
using namespace Eigen;
using namespace std; 
using std::vector;
using std::ifstream;
#include "global_variables.h"
using namespace QDP;
using namespace FILEDB;
using namespace Chroma;

int v_ind(int color, int site){
    // color * space + position 
    return color*Lx*Lx*Lx + site;
}

void get_evecs(vector<string> &vec_filenames, vector<string> &val_filenames, vector<MatrixXcd> &evectors, MatrixXd &evalues, int Nvec){ 
    // evectors[t](i, x)
    // evalues(t,i)
    for(int t=0; t<Nt; t++){
      evectors.push_back(MatrixXcd::Zero(Nvec, Lx*Lx*Lx*Nc));
    }
    
    
    EvecDBKey<EvecDBKey_t> Key_read;
    // loop over t
    for(int t=0; t<Nt; t++){
      Key_read.key().t_slice = t;       
      
      // ---- READ EVECS  ---- //
      BinaryStoreDB< EvecDBKey<EvecDBKey_t>, EvecDBData<EvecDBData_t> > db_read_vecs;
      db_read_vecs.open(vec_filenames[t], O_RDONLY, 0664);
      for(int vec_num=0; vec_num< Nvec; ++vec_num){
        // KEY
        Key_read.key().colorvec = vec_num;
        // VALUE
        EvecDBData<EvecDBData_t> evector_read;
        // READ 
        db_read_vecs.get(Key_read, evector_read);
        
        // ************** fill CPP array ************* //
        // loop over spatial points
        for(int site=0; site < Lx*Lx*Lx; site++){
          // loop over color 
          for(int color=0; color < Nc; color++){
            std::complex<double> tmp = evector_read.data().evec_data.at(site*Nc + color);
            evectors[t](vec_num, v_ind(color, site)) = tmp;
            //std::cout << evectors[t](vec_num, v_ind(color, site)) << std::endl;
          } // end loop over color 
        } // end loop over sites
      } // end loop over Dist evecs 


      // ---- READ EVALS  ---- //
      BinaryStoreDB< EvecDBKey<EvecDBKey_t>, EvecDBData<EvecDBEval_t> > db_read_vals;
      db_read_vals.open(val_filenames[t], O_RDONLY, 0664);
      for(int vec_num=0; vec_num< Nvec; ++vec_num){
        // KEY
        Key_read.key().colorvec = vec_num;
        // VALUE
        EvecDBData<EvecDBEval_t> evalue_read;
        // READ 
        db_read_vals.get(Key_read, evalue_read);
        // ************** fill CPP array ************* //
        Real tmp_value = evalue_read.data().eval;
        evalues(t, vec_num) = QDP::toFloat(tmp_value);
      } // end loop over Dist evecs 
    } // end loop over timeslices 
}


#endif
