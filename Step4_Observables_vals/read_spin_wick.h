#ifndef READ_SPIN_WICK_H
#define READ_SPIN_WICK_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream> // reads and writes files 
#include <sstream> // get sub strings 
#include <iterator>
#include <algorithm>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include <unsupported/Eigen/CXX11/Tensor>
#include "Eigen/Dense"
#include "spin_matrices.h"
#include "global_variables.h"
using namespace Eigen;
using namespace std; 
using cd = complex<double>;



// simple splitting funtion for " " delimeter 
vector<string> split(string &line) {
  istringstream is_line(line);
  vector<string> words;
  copy(istream_iterator<string>(is_line),
       istream_iterator<string>(),
       back_inserter(words));
  return words;
}


// SPIN MATRIX
// read text file into 4d Eigen tensor 
void spin_matrix_from_file(Eigen::Tensor<cd,4> &matrix, string spin_matrix_file){
  
  /* initialize spin matrix input 
  //Nc = 4 // 4 indices due to 4 colors (4 quarks over which to sum spins)
  //Nd = 4 // number of dimension (Dirac spinors)
  Eigen::Tensor<cd,Nc> matrix(Nd,Nd,Nd,Nd);
  matrix.setZero();
  */

  // Create a text string, which is used to output the text file
  string line;  
  // Read from the text file
  ifstream MyReadFile(spin_matrix_file);
  // // Use a while loop together with the getline() function to read the file line by line
  while (getline (MyReadFile, line)) {
    // split line into words
    string delim(" ");
    vector<string> words = split(line); 
    
    // convert each word to stringstream 
    stringstream alpha_s(words[0]);
    stringstream beta_s(words[1]);
    stringstream sigma_s(words[2]);
    stringstream delta_s(words[3]);
    stringstream value_s(words[4]);
    // convert each stringstream to desired type 
    int alpha;
    int beta;
    int sigma;
    int delta;
    double value_d;
    alpha_s >> alpha;
    beta_s >> beta;
    sigma_s >> sigma;
    delta_s >> delta;
    value_s >> value_d;
    // assume real value 
    cd value = cd(value_d, 0.0);
    
    //cout << "alpha=" << alpha << ", beta=" << beta << ", sigma=" << sigma << ", delta=" << delta << ", value=" << value << endl;
    matrix(alpha,beta,sigma,delta) = value;
  }
  MyReadFile.close();
  
}

void get_spin_matrix_bar(Eigen::Tensor<cd, 4> &spin_matrix_bar, Eigen::Tensor<cd, 4> &spin_matrix){
  gamma_struct gamma4;
  gamma4.name = "g4";
  gamma_map(gamma4);
  for(int a=0; a<Nd; a++){
    for(int b=0; b<Nd; b++){
      for(int c=0; c<Nd; c++){
        for(int d=0; d<Nd; d++){
          if(spin_matrix(a,b,c,d) != cd(0.0,0.0)){
            for(int a_=0; a_<Nd; a_++){
              for(int b_=0; b_<Nd; b_++){
                for(int c_=0; c_<Nd; c_++){
                  for(int d_=0; d_<Nd; d_++){
                    spin_matrix_bar(a,b,c,d) +=  gamma4.gamma_matrix(a_,a)
                                                *gamma4.gamma_matrix(b_,b)
                                                *gamma4.gamma_matrix(c_,c)
                                                *gamma4.gamma_matrix(d_,d)
                                                *spin_matrix(a,b,c,d);
                  } // d_
                } // c_
              } // b_
            } // a_
          } // if 
        } // d
      } // c
    } // b 
  } // a
}


// SPIN MATRIX
// read text file into 4d Eigen tensor 
void spin_matrix_from_file_rank8(Eigen::Tensor<cd,8> &matrix, string spin_matrix_file){
  
  /* initialize spin matrix input 
  //Nc = 4 // 8 indices due to 4 colors for source and sink (4 quarks over which to sum spins)
  //Nd = 4 // number of dimension (Dirac spinors)
  Eigen::Tensor<cd,2*Nc> matrix(Nd,Nd,Nd,Nd, Nd,Nd,Nd,Nd);
  matrix.setZero();
  */

  // Create a text string, which is used to output the text file
  string line;  
  // Read from the text file
  ifstream MyReadFile(spin_matrix_file);
  // // Use a while loop together with the getline() function to read the file line by line
  while (getline (MyReadFile, line)) {
    // split line into words
    string delim(" ");
    vector<string> words = split(line); 
    
    // convert each word to stringstream 
    // sink indices
    stringstream alpha_s(words[0]);
    stringstream beta_s(words[1]);
    stringstream sigma_s(words[2]);
    stringstream delta_s(words[3]);
    // source indices
    stringstream alpha_p_s(words[4]);
    stringstream beta_p_s(words[5]);
    stringstream sigma_p_s(words[6]);
    stringstream delta_p_s(words[7]);
    
    stringstream value_s(words[8]);
    
    // convert each stringstream to desired type 
    int alpha;
    int beta;
    int sigma;
    int delta;

    int alpha_;
    int beta_;
    int sigma_;
    int delta_;
    
    double value_d;
    
    alpha_s >> alpha;
    beta_s  >> beta;
    sigma_s >> sigma;
    delta_s >> delta;
    
    alpha_p_s >> alpha_;
    beta_p_s  >> beta_;
    sigma_p_s >> sigma_;
    delta_p_s >> delta_;
    
    value_s >> value_d;
    
    // assume real value 
    cd value = cd(value_d, 0.0);
    
    //cout << "alpha=" << alpha << ", beta=" << beta << ", sigma=" << sigma << ", delta=" << delta << ", value=" << value << endl;
    matrix(alpha,beta,sigma,delta, alpha_, beta_, sigma_, delta_) = value;
  }
  MyReadFile.close();
  
}

vector<int> get_wick_signs_from_file(string wick_filename){
  vector<int> signs;
  // Read file, each line of file is one Wick contraction
  // Create a text string, which is used to output the text file
  string line;  
  // Read from the text file
  ifstream MyReadFile(wick_filename);
  // // Use a while loop together with the getline() function to read the file line by line
  while (getline (MyReadFile, line)) {
    // split line into words
    string delim(" ");
    vector<string> words = split(line); 
    // convert each word to stringstream 
    stringstream sign_s(words[Nc]);
    // convert each stringstream to desired type 
    int sign_d;
    sign_s >> sign_d;
    // assume real value 
    signs.push_back(sign_d);
  }
  return signs;
}



vector<Eigen::array<Eigen::IndexPair<int>, 4>> wick_indices_from_file(string wick_filename){
  vector<Eigen::array<Eigen::IndexPair<int>, 4>> indices_list;
  
  // Read file, each line of file is one Wick contraction
  // Create a text string, which is used to output the text file
  string line;  
  // Read from the text file
  ifstream MyReadFile(wick_filename);
  // // Use a while loop together with the getline() function to read the file line by line
  while (getline (MyReadFile, line)) {
    // define index pairs to add to vector below
    // split line into words
    string delim(" ");
    vector<string> words = split(line); 
    // convert each word to stringstream 
    stringstream index0_s(words[0]);
    stringstream index1_s(words[1]);
    stringstream index2_s(words[2]);
    stringstream index3_s(words[3]);
    // convert each stringstream to desired type 
    int index0;
    int index1;
    int index2;
    int index3;
    index0_s >> index0;
    index1_s >> index1;
    index2_s >> index2;
    index3_s >> index3;
    
    Eigen::array<Eigen::IndexPair<int>, 4> indices_contraction = {Eigen::IndexPair<int>(0,index0),
                                                                  Eigen::IndexPair<int>(1,index1),
                                                                  Eigen::IndexPair<int>(2,index2),
                                                                  Eigen::IndexPair<int>(3,index3)};
    indices_list.push_back(indices_contraction);
  }
  return indices_list;
}






#endif
