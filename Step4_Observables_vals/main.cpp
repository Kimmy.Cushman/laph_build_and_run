#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <unsupported/Eigen/CXX11/Tensor>
#include <Eigen/Dense>
//#include </p/gpfs1/cushman2/code/source/eigen/unsupported/Eigen/CXX11/Tensor>

// from -I${QDP}
#include "qdp.h" 
#include "qdp_map_obj.h"  
#include "qdp_map_obj_disk.h"
#include "qdp_db.h"
#include "qdp_map_obj_disk_multiple.h" // from -I QDP (maybe not needed now, need if perambs stored in multiple files) 
#include "qdp_iogauge.h" // prob dont need, only need if want derivative in phi matrices 
// local
#include "read_spin_wick.h"
#include "db_codes/key_prop_matelem.h"// local file
#include "global_variables.h" // written and updated by config_loop.py
#include "get_evecs.h"
#include "get_perams.h"
#include "mult_pion.h"
#include "spin_matrices.h"
//#include "mult_baryon_udud.h"
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;
using namespace std;
using cd = complex<double>;


int main(int argc, char *argv[]){
 
    START_CODE();
    // Put the machine into a known state
    QDP_initialize(&argc, &argv);
   
    std::cout << "   S T A R T   C O D E   " << std::endl;
    std::string perambulator_filename_base = argv[1]; // includes specification of t_source
    std::string vecs_dir = argv[2];
    std::string vec_names_base_filename = argv[3];
    int t_stop = Nt;
    string spin_matrix_filename = argv[4];
    string wick_filename = argv[5];
    string corr_savename_dir = argv[6];
    string state = argv[7];
    int cfg_start = atoi(argv[8]);
    int cfg_stop = atoi(argv[9]);
    int cfg_step = atoi(argv[10]);
    int Nvec = atoi(argv[11]);

    // assume all t_sink are calculated, 
    // eg. Nt_backward = t_source, Nt_forward = Nt - t_source
    // see get_perams.h for indexing 
    int t_source = atoi(argv[11]);
    
    for(int cfg=cfg_start; cfg<cfg_stop; cfg+=cfg_step){
      std::string cfg_num=std::to_string(cfg);
      std::string perambulator_filename = perambulator_filename_base + "_cfg" + std::to_string(cfg) + ".sdb";
      //std::string vec_names_base = vecs_dir + "/" + "cfg_" + std::to_string(cfg) + "_xyz_cyclic_inverse_position_xyz_cyclic_inverse_position_xyz_cyclic_inverse_position/" + vec_names_base_filename;
      std::string vec_names_base = vecs_dir + "/" + "cfg_" + std::to_string(cfg) + "/" + vec_names_base_filename;

      std::cout << "STATE: " << state << std::endl;
      std::cout << "corr_savename_dir " << corr_savename_dir << std::endl;
      std::cout << "vec_names_base " << vec_names_base << std::endl;
      std::cout << "Nvec " << Nvec << std::endl;
      
      
      // Both perams and evecs loading assumes all Nt have been calculated 
      // load perambulators 
      std::vector<MatrixXcd> tau;
      get_tau(perambulator_filename, tau, Nvec);
      
      for(int t_sink=0; t_sink<Nt; t_sink++){
        for(int alpha = 0; alpha < 4; alpha++){
          for(int beta = 0; beta < 4; beta++){
            for(int i=0; i<Nvec; i++){
              for(int j=0; j<Nvec; j++){
                std::cout << tau[tau_ind(t_sink, 0, beta, alpha)](i,j) << std::endl;
              }
            }
          }
        }
      }
      
      // get eigen vectors filenames 
      vector<string> vec_names(Nt);
      vector<string> val_names(Nt);
      for(int t=0; t<Nt; t++){
        std::ostringstream oss_vecs;
        std::ostringstream oss_vals;
        //oss_vecs <<  vec_names_base << t << "_evecs.db";
        oss_vecs <<  vec_names_base << t << ".db";
        oss_vals <<  vec_names_base << t << "_evals.db";
        vec_names[t] = oss_vecs.str();
        val_names[t] = oss_vals.str();
      }
      // load evecs 
      vector<MatrixXcd> evectors;  // evectors[t](i,x)
      MatrixXd evalues(Nt, Nvec);  // evalues(t,i)
      get_evecs(vec_names, val_names, evectors, evalues, Nvec); 

      /* 
      for(int t=0; t<Nt; t++){
        QDPIO::cout << "t = " << t << std::endl;
        for(int i=0; i<Nvec; i++){
          QDPIO::cout << evalues(t,i) << std::endl;
          if(i==0 or i==1 or i==2 or i==3 or i==9 or i==12 or i==15 or i==21 or i==27) QDPIO::cout << std::endl;
        }
        QDPIO::cout << std::endl;
      }
    
      string savename = "L16_evals.txt";
      ofstream myfile;
      myfile.open(savename); 
      cout << "\n\n" << savename << "\n\n" << endl;
      for(int t=0; t < Nt; t++){
        myfile <<  "t=" << t << "\n";
        for(int i=0; i<Nvec; i++){
          myfile << evalues(t,i) << "\n";
        }
      }
      myfile.close();
      */
      

      // compute correlation functions! 
      vector<complex<double>> corr(Nt);
      
      if(state == "baryon_udud"){
        int p = 0;
        //baryon_udud(corr, evectors, tau, p, t_source, corr_savename_dir, cfg_num, Nvec);    
      }
      
      // CONNECTED ONLY 
      if(state == "mesons"){
        string meson_state;
        
        meson_state = "g5";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
        meson_state = "g4g5";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
        meson_state = "g1";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
        meson_state = "g2";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
        meson_state = "g3";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
        meson_state = "g4g1";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
        meson_state = "g4g2";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
        meson_state = "g4g3";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
        meson_state = "g4";
        pion(corr, evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state, Nvec); 
      }
      std::cout  << "" << std::endl;
    }
    std::cout << "   E N D  C O D E   ---   S U C C E S S !" << std::endl;
    return 0;
}
























