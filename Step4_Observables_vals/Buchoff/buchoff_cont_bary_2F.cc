#include "mpi.h"
#include "cont_bary_2F.h"
#include "IO.h"
#include "permutations.h"
#include "chroma.h"
//#include <boost/lexical_cast.hpp>
#include <iostream>
#include <string>
#include <sstream>

using namespace QDP;

namespace Chroma 
{ 

  // Fill in the blanks
  namespace InlineContBary2FEnv 
  { 

    // Function to register with a factory
    // This is boilerplate stuff
    AbsInlineMeasurement* createMeasurement(XMLReader& xml_in, 
					    const std::string& path) 
    {
      // Create an Params from the XML
      // Use it to create an InlineContBary2F
      return new InlineContBary2F( Params(xml_in, path) );
    }

    // The name of my measurement for the XML file
    // Change this for each measurement
    const std::string name = "SU4_BARYON_TWO_FLAVOR";

    // Register the measurement in the measurement factory
    namespace { 
      bool registered = false;
    }

    bool registerAll()
    {
      bool success = true;
      if (! registered) { 
         success &= TheInlineMeasurementFactory::Instance().registerObject(name, createMeasurement);
         registered = true;
      }
      return success;
    }



  // Param stuff
//  Params::Params() { frequency = 0; xml_file=""; outdir=""; inBlock1=""; inBlock2=""; inBlock3=""; inBlock4=""; inBlock5="";}
    Params::Params() { frequency = 0; xml_file=""; outdir=""; prop=""; spinN="";}

  Params::Params(XMLReader& xml_in, const std::string& path) 
  {
    //QDPIO::cout << "Params()" << endl;
    try 
    {
      XMLReader paramtop(xml_in, path);

      if (paramtop.count("Frequency") == 1)
	read(paramtop, "Frequency", frequency);
      else
	frequency = 1;

      read(paramtop, "Param/output_dir", outdir);
      //QDPIO::cout << "outdir " << outdir << endl;
      read(paramtop, "Param/prop", prop);
      read(paramtop, "Param/Baryon_Spin", spinN);


      if( paramtop.count("xml_file") != 0 ) { 
	read(paramtop, "xml_file", xml_file);
      }
      else { 
	xml_file == "";
      }
	
    }
    catch(const std::string& e) 
    {
      QDPIO::cout << "Caught Exception reading XML: " << e << endl;
      QDPIO::cerr << "Caught Exception reading XML: " << e << endl;
      QDP_abort(1);
    }

  }


  void
  Params::write(XMLWriter& xml_out, const std::string& path) 
  {
    push(xml_out, path);
    
    QDP::write(xml_out, "output_dir", outdir);
    QDP::write(xml_out, "prop", prop);
    QDP::write(xml_out, "Baryon_Spin", spinN);


    if( xml_file != "" ){ 
      QDP::write(xml_out, "xml_file", xml_file);
    }


    pop(xml_out);
    }
  }

  void 
  InlineContBary2F::operator()(long unsigned int update_no,
			   XMLWriter& xml_out) 
  {

    // This bit merely supports providing an external xml_file 
    // for this measurement
    if ( params.xml_file == "" ) { 
      
      func( update_no, xml_out );
    }
    else { 

      // Hash an XML file name from the user specified string
      // and the update number
      std::string xml_file = makeXMLFileName(params.xml_file, update_no);

      // IN the global output, make a note that the output went
      // to this separate XML file
      push(xml_out, "cont_bary");
      write(xml_out, "update_no", update_no);
      write(xml_out, "xml_file", xml_file);
      pop(xml_out);

      XMLFileWriter xml(xml_file);
      func(update_no, xml);
    }
  }



  void 
  InlineContBary2F::func(unsigned long int update_no, XMLWriter& xml_out)
  {
    START_CODE();

    StopWatch measure_time;
    measure_time.reset();
    measure_time.start();

    // Convert to Dirac-Pauli
    //const SpinMatrixD U = PauliToDRMat();

    // Test that the gauge configuration  and the propagator we need
    // exist in the map.
    XMLBufferWriter prop_xml;

    try
    {
      // Do the same with the propagator 
      TheNamedObjMap::Instance().getData<LatticePropagator>(params.prop);


      // Get its record XML
      TheNamedObjMap::Instance().get(params.prop).getRecordXML(prop_xml);

    }
    catch( std::bad_cast ) 
    {

      // If the object exists and is of the wrong type we will 
      // land in this catch.
      QDPIO::cerr << InlineContBary2FEnv::name << ": caught dynamic cast error" 
                  << endl;
      QDP_abort(1);
    }
    catch (const string& e) 
    {
      // If the object is not in the map we will land in this 
      // catch
      QDPIO::cerr << InlineContBary2FEnv::name << ": map call failed: " << e 
                  << endl;
      QDP_abort(1);
    }

    // If we got here, then the gauge field and prop are both in 
    // the map. Their XML will have been captured.
    // Let us bind the references to a local name so 
    // we don't have to type the long lookup string every time.
    //
    // Note const here means we can't change the field
    const LatticePropagator& prop1 = 
      TheNamedObjMap::Instance().getData<LatticePropagator>(params.prop);

    XMLReader input_prop_xml;
    TheNamedObjMap::Instance().get(params.prop).getRecordXML(input_prop_xml);



    QDPIO::cout << InlineContBary2FEnv::name <<": Beginning " << endl;

    // Boilerplate stuff to the output XML
    push(xml_out, "cont_bary");
    write(xml_out, "update_no", update_no);

    // Write info about the program
    proginfo(xml_out);

    // Write out the input
    params.write(xml_out, "Input");

    //----------------------------------------------------------------------
    // Read in irrep blocks and check for duplicates.
    //
    //
    // 
    //---------------------------------------------------------------------

    QDPIO::cout << "Propagator ID is:" << params.prop << endl;

    multi1d<int> srce_pt;
    read(input_prop_xml,"/SinkSmear/PropSource/Source/t_srce",srce_pt);

    multi1d<int> latsize;
    read(input_prop_xml,"/SinkSmear/Config_info/ChromaHB/HBItr/nrow",latsize);

    std::string traj;
    read(input_prop_xml,"/SinkSmear/Config_info/ChromaHB/MCControl/StartUpdateNum",traj);

    std::string srcesmear;
    read(input_prop_xml,"/SinkSmear/PropSource/Source/SourceType",srcesmear);

    std::string sinksmear;
    read(input_prop_xml,"/SinkSmear/PropSink/Sink/SinkType",sinksmear);


    int Nt=latsize[3];
    int srcet=srce_pt[3];

    QDPIO::cout << "Nx:" << latsize[0] << endl;
    QDPIO::cout << "Ny:" << latsize[1] << endl;
    QDPIO::cout << "Nz:" << latsize[2] << endl;
    QDPIO::cout << "Nt:" << latsize[3] << endl;

    QDPIO::cout << "srceX:" << srce_pt[0] << endl;
    QDPIO::cout << "srceY:" << srce_pt[1] << endl;
    QDPIO::cout << "srceZ:" << srce_pt[2] << endl;
    QDPIO::cout << "srceT:" << srce_pt[3] << endl;


    //Fixed Spin Matricies
    const SpinMatrix Cg5 = Gamma(5)*SpinMatrix(1.0);
    multi1d<SpinMatrix> Cg(4);
    Cg[0]=Cg5*Gamma(15)*Gamma(8);
    Cg[1]=Cg5*Gamma(15)*Gamma(1);
    Cg[2]=Cg5*Gamma(15)*Gamma(2);
    Cg[3]=Cg5*Gamma(15)*Gamma(4);

    //Input Spin Matricies
    SpinMatrix X1;
    SpinMatrix X2;

    if (params.spinN == "0"){
      X1=Cg5;
      X2=Cg5;}

    if (params.spinN == "1"){
      X1=Cg[1];
      X2=Cg5;}

    if (params.spinN == "2"){
      X1=Cg[1];
      X2=Cg[2];}


    SpinMatrix X1t;
    SpinMatrix X2t;

    X1t=transpose(X1);
    X2t=transpose(X2);

    // 2 pt correlation functions
    QDPIO::cerr << "Doing 2 point correlators" << endl;
    //LatticePropagator antiprop1 = Gamma(15)*adj(S)*Gamma(15);
    LatticePropagator transprop1 = transposeSpin(prop1);
    LatticePropagator Sap;

    multi2d<LatticeSpinMatrix> S(4,4);
    //multi2d<LatticeSpinMatrix> Sd(4,4);
    multi2d<LatticeSpinMatrix> St(4,4);
    

    for (int a=0;a<4;a++){
      for (int b=0;b<4;b++){
	S[a][b]=peekColor(prop1,a,b);
	St[a][b]=peekColor(transprop1,a,b);
      }      
    }

    


    
    LatticeComplex traced;
    multi2d<DComplex> h_corr;
    multi1d<DComplex> corr;
    ofstream outre;

    std::string dir=params.outdir;
    std::string file_name;
    std::string contract_type;
    std::string frwd_name="_"+traj;

    SftMom phases(0,true,Nd-1);

    IO_Bary io;
    int type;

    //2 Flavor; 3 u's 1 d
    QDPIO::cout << "SU4_BARYON: 2 Flavor, 3 u's 1 d ,com1" << endl;
    contract_type="bary_3u1d_com1";
    traced=0;
    for (int a=0;a<4;a++){
      for (int b=0;b<4;b++){
	if (a == b) continue;
	for (int c=0;c<4;c++){
	  if (a == c) continue;
	  if (b == c) continue;
	  for (int d=0;d<4;d++){
	    if (a == d) continue;
	    if (b == d) continue;
	    if (c == d) continue;
	    for (int ap=0;ap<4;ap++){
	      for (int bp=0;bp<4;bp++){
		if (ap == bp) continue;
		for (int cp=0;cp<4;cp++){
		  if (ap == cp) continue;
		  if (bp == cp) continue;
		  for (int dp=0;dp<4;dp++){
		    if (ap == dp) continue;
		    if (bp == dp) continue;
		    if (cp == dp) continue;
	      	    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][a]*X1*St[bp][b]*X1)*trace(S[cp][c]*X2*St[dp][d]*X2);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][a]*X1*St[cp][b]*X2t*S[dp][d]*X2t*St[bp][c]*X1);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][b]*X1t*St[bp][a]*X1)*trace(S[cp][c]*X2*St[dp][d]*X2);
		    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][b]*X1t*St[cp][a]*X2t*S[dp][d]*X2t*St[bp][c]*X1);
		    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][c]*X2*St[dp][d]*X2*S[cp][b]*X1t*St[bp][a]*X1);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][c]*X2*St[dp][d]*X2*S[cp][a]*X1*St[bp][b]*X1);
		  }
		}
	      }
	    }
	  }
	}
      }
    }
    file_name=dir+contract_type+"_Spin"+params.spinN+"_"+srcesmear+"_"+sinksmear+frwd_name;
    type=1;
    h_corr=phases.sft(traced);
    corr=h_corr[0];
    io.writeCorrelator(corr,file_name,Nt,srce_pt,type);

    QDPIO::cout << "SU4_BARYON: 2 Flavor, 3 u's 1 d ,com2" << endl;
    contract_type="bary_3u1d_com2";
    traced=0;
    for (int a=0;a<4;a++){
      for (int b=0;b<4;b++){
	if (a == b) continue;
	for (int c=0;c<4;c++){
	  if (a == c) continue;
	  if (b == c) continue;
	  for (int d=0;d<4;d++){
	    if (a == d) continue;
	    if (b == d) continue;
	    if (c == d) continue;
	    for (int ap=0;ap<4;ap++){
	      for (int bp=0;bp<4;bp++){
		if (ap == bp) continue;
		for (int cp=0;cp<4;cp++){
		  if (ap == cp) continue;
		  if (bp == cp) continue;
		  for (int dp=0;dp<4;dp++){
		    if (ap == dp) continue;
		    if (bp == dp) continue;
		    if (cp == dp) continue;
	      	    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][a]*X1*St[dp][d]*X1)*trace(S[cp][c]*X2*St[bp][b]*X2);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][a]*X1*St[dp][d]*X1)*trace(S[cp][b]*X2t*St[bp][c]*X2);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][b]*X2t*St[cp][c]*X2t*S[bp][a]*X1*St[dp][d]*X1);
		    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][b]*X2t*St[bp][c]*X2*S[cp][a]*X1*St[dp][d]*X1);
		    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][c]*X2*St[cp][b]*X2t*S[bp][a]*X1*St[dp][d]*X1);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][c]*X2*St[bp][b]*X2*S[cp][a]*X1*St[dp][d]*X1);
		  }
		}
	      }
	    }
	  }
	}
      }
    }
    file_name=dir+contract_type+"_Spin"+params.spinN+"_"+srcesmear+"_"+sinksmear+frwd_name;
    type=1;
    h_corr=phases.sft(traced);
    corr=h_corr[0];
    io.writeCorrelator(corr,file_name,Nt,srce_pt,type);

    //2 Flavor; 2 u's 2 d
    QDPIO::cout << "SU4_BARYON: 2 Flavor, 2 u's 2 d's ,com3" << endl;
    contract_type="bary_2u2d_com3";
    traced=0;
    for (int a=0;a<4;a++){
      for (int b=0;b<4;b++){
	if (a == b) continue;
	for (int c=0;c<4;c++){
	  if (a == c) continue;
	  if (b == c) continue;
	  for (int d=0;d<4;d++){
	    if (a == d) continue;
	    if (b == d) continue;
	    if (c == d) continue;
	    for (int ap=0;ap<4;ap++){
	      for (int bp=0;bp<4;bp++){
		if (ap == bp) continue;
		for (int cp=0;cp<4;cp++){
		  if (ap == cp) continue;
		  if (bp == cp) continue;
		  for (int dp=0;dp<4;dp++){
		    if (ap == dp) continue;
		    if (bp == dp) continue;
		    if (cp == dp) continue;
	      	    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][a]*X1*St[bp][b]*X1)*trace(S[cp][c]*X2*St[dp][d]*X2);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][a]*X1*St[bp][b]*X1)*trace(S[cp][d]*X2t*St[dp][c]*X2);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][b]*X1t*St[bp][a]*X1)*trace(S[cp][c]*X2*St[dp][d]*X2);
		    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][b]*X1t*St[bp][a]*X1)*trace(S[cp][d]*X2t*St[dp][c]*X2);
		  }
		}
	      }
	    }
	  }
	}
      }
    }
    file_name=dir+contract_type+"_Spin"+params.spinN+"_"+srcesmear+"_"+sinksmear+frwd_name;
    type=1;
    h_corr=phases.sft(traced);
    corr=h_corr[0];
    io.writeCorrelator(corr,file_name,Nt,srce_pt,type);

    QDPIO::cout << "SU4_BARYON: 2 Flavor, 2 u's 2 d's ,com4" << endl;
    contract_type="bary_2u2d_com4";
    traced=0;
    for (int a=0;a<4;a++){
      for (int b=0;b<4;b++){
	if (a == b) continue;
	for (int c=0;c<4;c++){
	  if (a == c) continue;
	  if (b == c) continue;
	  for (int d=0;d<4;d++){
	    if (a == d) continue;
	    if (b == d) continue;
	    if (c == d) continue;
	    for (int ap=0;ap<4;ap++){
	      for (int bp=0;bp<4;bp++){
		if (ap == bp) continue;
		for (int cp=0;cp<4;cp++){
		  if (ap == cp) continue;
		  if (bp == cp) continue;
		  for (int dp=0;dp<4;dp++){
		    if (ap == dp) continue;
		    if (bp == dp) continue;
		    if (cp == dp) continue;
	      	    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][a]*X1*St[cp][c]*X1)*trace(S[bp][b]*X2*St[dp][d]*X2);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][a]*X1*St[dp][c]*X2*S[bp][b]*X2*St[cp][d]*X1);
		    traced -= epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][b]*X2*St[dp][d]*X2*S[bp][a]*X1*St[cp][c]*X1);
		    traced += epsilon(a,b,c,d)*epsilon(ap,bp,cp,dp)*trace(S[ap][b]*X2*St[cp][d]*X1)*trace(S[bp][a]*X1*St[dp][c]*X2);
		  }
		}
	      }
	    }
	  }
	}
      }
    }
    file_name=dir+contract_type+"_Spin"+params.spinN+"_"+srcesmear+"_"+sinksmear+frwd_name;
    type=1;
    h_corr=phases.sft(traced);
    corr=h_corr[0];
    io.writeCorrelator(corr,file_name,Nt,srce_pt,type);

    // End of  your code.
    pop(xml_out);


    measure_time.stop();
    QDPIO::cout << InlineContBary2FEnv::name << ": total time = "
		<< measure_time.getTimeInSeconds() 
		<< " secs" << endl;

    QDPIO::cout << InlineContBary2FEnv::name << ": ran successfully" << endl;
    END_CODE();
  } 

};
