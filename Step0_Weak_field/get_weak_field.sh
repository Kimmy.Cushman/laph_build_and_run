#!/bin/bash 

## sources intel compiler variables (don't change this)
export GCC_VERSION="8.3.1"
module load gcc/${GCC_VERSION}

#Lx=$1
#Nt=$((2 * ${Lx}))
MACHINE="QUARTZ"
CODE_DIR="/p/lustre1/cushman2/laph_build_and_run/code"
NC=4
#XML_CONFIG="/p/lustre1/cushman2/laph_build_and_run/Production/L${Lx}T${Nt}/betaWEAK/kappaWEAK/configs/cfg_1.xml"
XML_CONFIG=$1

if [ $MACHINE == "LASSEN" ] 
then
    HOME="/p/gpfs1"
    run="jsrun --rs_per_host=32"
else
    HOME="/p/lustre1"
    run=""
fi 

run="srun -t 10 -p pbatch -n 1 -N 1"
exe="${CODE_DIR}/Nc${NC}_Nd4_install/chroma_Nc/bin/chroma"
geom="-geom 1 1 1 1" 
input="-i ${XML_CONFIG}"
outfile="-o ${XML_CONFIG}_start.out > ${XML_CONFIG}_start.stdout 2>&1"
runme="${run} ${exe} ${geom} ${input} ${outfile} < /dev/null"

export QUDA_RESOURCE_PATH=${HOME}/QUDA_resources_Nc${NC}
export LD_LIBRARY_PATH=${CODE_DIR}/base_install/libxml2/lib64:$LD_LIBRARY_PATH
echo $LD_LIBRARY_PATH


echo "Before analysis: ", `date`
echo "RUN   : ${run} \n"
echo "EXE   : $exe\n"
echo "INPUT : $input\n"
echo "RUNME : $runme\n"  
time ${runme}  ## runs script here 
echo "After analysis: ", `date`

