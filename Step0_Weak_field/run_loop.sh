#!/bin/bash
Lx=4

for (( i=$1; i<$2; i++ ))
do
  python config_loop.py ${Lx} ${i} >& Lx${Lx}_cfg${i}.log &
done
