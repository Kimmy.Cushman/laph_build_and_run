import numpy as np
import os
import sys 

MACHINE="QUARTZ"

if MACHINE == "LASSEN":
    home = "/p/gpfs1/cushman2/laph_build_and_run"
if MACHINE == "QUARTZ":
    home = "/p/lustre1/cushman2/laph_build_and_run"
code_dir = "%s/code"%home

# physical params 
Nc = 4
Lx = int(sys.argv[1])
Nt = 2*Lx

cfg = int(sys.argv[2]) # config num

weak_field_base = "%s/Step0_Weak_field/weak_field_base.xml"%home

# filename 
Lx_dir = "%s/Production/L%iT%i"%(home, Lx, Nt)
beta_dir = "%s/betaWEAK/"%Lx_dir
k_dir = "%s/kappaWEAK"%beta_dir
configs_dir = "%s/configs"%k_dir

if not os.path.isdir(Lx_dir):
    os.system("mkdir %s"%Lx_dir)
if not os.path.isdir(beta_dir):
    os.system("mkdir %s"%beta_dir)
    print("making beta dir")
if not os.path.isdir(k_dir):
    os.system("mkdir %s"%k_dir)
if not os.path.isdir(configs_dir):
    os.system("mkdir %s"%configs_dir)

xml_config = "%s/Production/L%iT%i/betaWEAK/kappaWEAK/configs/cfg_%i.xml"%(home, Lx, Nt, cfg)
config = "%s/Production/L%iT%i/betaWEAK/kappaWEAK/configs/cfg_%i.lime"%(home, Lx, Nt, cfg)


os.system("rm %s"%xml_config)
print(xml_config)
base_file = open(weak_field_base, "r")
xml_config_file = open(xml_config, "w")

lines = base_file.readlines()
for line in lines:
    
    # output 
    if "<file_name></file_name>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config + "<" + post) 
    
    # Latt size 
    elif "<nrow></nrow>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
    
    # random seed 
    elif "<elem></elem>" in line:
        pre, post = line.split("><")
        r1, r2, r3, r4 = np.random.randint(999,size=4)
        xml_config_file.write(pre + ">" + str(r1) + "<" + post) 
        xml_config_file.write(pre + ">" + str(r2) + "<" + post) 
        xml_config_file.write(pre + ">" + str(r3) + "<" + post) 
        xml_config_file.write(pre + ">" + str(r4) + "<" + post) 
        
    else:
        xml_config_file.write(line)

base_file.close()
xml_config_file.close()

print(xml_config)
