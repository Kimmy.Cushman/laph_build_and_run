#!/bin/bash
export GCC_VERSION="8.3.1"
module load gcc/${GCC_VERSION}
set -u
# get variables from bash script (all CAPS)
source run_ensemble.sh 
#############################################
# Edit here 
#############################################
# parameters of this run 
Nvec_vecs=200 # read from this file
Nnoise_vecs=200 # read from this file 
Nvec_peram=100 # dimension of the perambulator
quarkline_type="LapH"
kappa_peram="1475"
mass_or_kappa_peram="k"
action="WILSON"
time_job=1400 #720 #L32 took 560 min on 2 nodes with 64,2,8, 1,1,8,8
cfg_start=6400
cfg_stop=10400
cfg_step=50
tasks=64
nodes=2
threads=8
geom_x=1
geom_y=1
geom_z=8
geom_t=8
specify_t_source=1
tsource=$1
#############################################
# FILE LOCATIONS
code_dir="${laph_HOME}/code"
exe="${code_dir}/Nc${NC}_Nd4_install/chroma_Nc/bin/chroma"
Step3_dir="${laph_HOME}/Step3_Perambulators" # location of compile script, main program
xml_template="${Step3_dir}/cfg_xmls/distillation_base.xml"

# peram file location depends on kappa, quarkline type, Nvec, Nnoise
peram_dir="${KAPPA_DIR}/Perams/kappa0p${kappa_peram}/${quarkline_type}"
if [ ${quarkline_type} == "LapH" ]
then
  peram_dir="${peram_dir}/Nvec${Nvec_peram}"
else
  peram_dir="${peram_dir}/Nvec${Nvec_vecs}_Nnoise${Nvec_peram}"
fi 

sdb_dir="${peram_dir}/perambulators"
batch_dir="${peram_dir}/batch_output"
xml_dir="${peram_dir}/xml"
mkdir -p ${peram_dir}
mkdir -p ${sdb_dir}
mkdir -p ${batch_dir}
mkdir -p ${xml_dir}
if [ ${specify_t_source} == 1 ]
then
  peram_output_base="perambulators_${mass_or_kappa_peram}0p${kappa_peram}_${quarkline_type}_${Nvec_peram}vecs_tsource${tsource}" # base_cfg${cfg}.xml, base_cfg${cfg}.sdb
  Nt_forward=$((${NT} - ${tsource}))
  Nt_backward=$((${tsource}))
  tsource_xml=${tsource}
else
  peram_output_base="perambulators_${mass_or_kappa_peram}0p${kappa_peram}_${quarkline_type}_${Nvec_peram}vecs" # base_cfg${cfg}.xml, base_cfg${cfg}.sdb
  Nt_forward=${NT}
  Nt_backward=0
  tsource_xml=0
fi
batch_output_filename_base="${peram_output_base}_n${tasks}_N${nodes}_thread${threads}_gx${geom_x}_gy${geom_y}_gz${geom_z}" # base_cfg${cfg}.out


#################### RUN #######################
for (( i=${cfg_start}; i<${cfg_stop}; i+=${cfg_step} ))
do 
  # inputs
  config_file="${CONFIG_FILENAME_BASE}${i}.lime"
  laph_dir="${KAPPA_DIR}/LapH_vecs/Nvec${Nvec_vecs}/cfg_${i}"
  sLapH_dir="${KAPPA_DIR}/sLapH_vecs/Nvec${Nvec_vecs}/Nnoise${Nnoise_vecs}/cfg_${i}"
  
  if [ ${quarkline_type} == "LapH" ]
  then
    vecs_file_t="${laph_dir}/LapH_${Nvec_vecs}vecs_t"
  else
    vecs_file_t="${slaph_dir}/sLapH_${Nvec_vecs}vecs_Nnoise${Nnoise_vecs}_t"
  fi 
  xml_cfg_input="${xml_dir}/${peram_output_base}_cfg${i}.xml"
  
  # outputs
  batch_output="${batch_dir}/${batch_output_filename_base}_cfg${i}.out"
  peram_sdb_outfile="${sdb_dir}/${peram_output_base}_cfg${i}.sdb"

  rm ${peram_sdb_outfile} 2> /dev/null
  
  ############# CHECK FILES EXIST ################
  run_peram=1
  # check that LapH vecs are there 
  for (( t=0; t<${NT}; t++))
  do 
    mod="${vecs_file_t}${t}.mod"
    if [[ ! -f ${mod} || $(stat -c %s ${mod} ) == 0 ]]
    then
      echo "${vecs_file_t}${t}  not done "
      run_peram=0
    fi
  done
  if [ ${run_peram} == 1 ]
  then 
    ############ GENREATE CONFIG XML ################# 
   

    cp ${xml_template} ${xml_cfg_input}
    if [ ${mass_or_kappa_peram} == "m" ]
    then 
      sed -i "s|<Kappa></Kappa>|<Mass>-0.${kappa_peram}</Mass>|g" ${xml_cfg_input}
      find_replace "mass_label" "m0p${kappa_peram}" ${xml_cfg_input}
    else
      find_replace "Kappa"        "0.${kappa_peram}"   ${xml_cfg_input}
      find_replace "mass_label"   "K0p${kappa_peram}"        ${xml_cfg_input}
    fi
    find_replace "FermAct"       ${action}                 ${xml_cfg_input}
    find_replace "t_sources"     ${tsource_xml}            ${xml_cfg_input}
    find_replace "Nt_forward"    ${Nt_forward}             ${xml_cfg_input}
    find_replace "Nt_backward"   ${Nt_backward}            ${xml_cfg_input}
    find_replace "num_vecs"      ${Nvec_peram}             ${xml_cfg_input}
    find_replace "prop_op_file"  ${peram_sdb_outfile}      ${xml_cfg_input}
    find_replace "nrow"          "${LX} ${LX} ${LX} ${NT}" ${xml_cfg_input}
    find_replace "cfg_file"      ${config_file}            ${xml_cfg_input}
    find_replace "cfg_type"      ${CONFIG_TYPE}            ${xml_cfg_input}
    
    # laph vecs file locations
    sed -i "s|<elem></elem>|<elem>${vecs_file_t}0.mod</elem>|g" ${xml_cfg_input}
    for (( t=1; t<${NT}; t++ ))
    do
      last="<elem>${vecs_file_t}$((t-1)).mod</elem>"
      next="<elem>${vecs_file_t}${t}.mod</elem>"
      sed -i "s|${last}|&\n${next}|" ${xml_cfg_input}
    done
    rm ${batch_output} 2> /dev/null
    ############## RUN THIS CONFIG ################### 
    export LD_LIBRARY_PATH=${code_dir}/base_install/libxml2/lib64:$LD_LIBRARY_PATH
    run="srun -t ${time_job} -p ${QUEUE} -o ${batch_output} -n ${tasks} -N ${nodes}"
    geom="-geom ${geom_x} ${geom_y} ${geom_z} ${geom_t}" 
    input="-i ${xml_cfg_input}"
    outfile="-o ${xml_cfg_input}_start.out > ${xml_cfg_input}_start.stdout 2>&1"
    runme="${run} ${exe} ${geom} ${input} ${outfile} < /dev/null"
    echo "Perambulators CONFIG ${i}"
    echo "RUN   : ${run} \n"
    echo "EXE   : $exe\n"
    echo "INPUT : $input\n"
    echo "RUNME : $runme\n"  
    ${runme} &> /dev/null & ## runs script here 
    echo ""
  fi # run peram conditional 
done # config loop 

