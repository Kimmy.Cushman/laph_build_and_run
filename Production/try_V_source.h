
void get_T(Eigen::Tensor<std::complex<double>,4> &T,
           vector<MatrixXcd> &v, 
           int p, int t){
  T.setZero();
  // time loop
  for(int a=0; a<Nc; a++){
    for(int b=0; b<Nc; b++){
      for(int c=0; c<Nc; c++){
        for(int d=0; d<Nc; d++){
          vector<int> epsilon;
          epsilon.push_back(a);
          epsilon.push_back(b);
          epsilon.push_back(c);
          epsilon.push_back(d);
          complex<double> e = complex<double>(levi_civita(epsilon), 0.0);
          if(e != complex<double>(0.0, 0.0)){
            // *
            // position loop 
            for(int x=0; x<Lx*Lx*Lx; x++){
              // distillation loop 
              for(int i=0; i<Nvec; i++){
                for(int j=0; j<Nvec; j++){
                  for(int k=0; k<Nvec; k++){
                    for(int l=0; l<Nvec; l++){
                      // p = 0, no phase 
                      T(i,j,k,l) += e * conj(v[t](i, v_ind(a,x))) 
                                      * conj(v[t](j, v_ind(b,x))) 
                                      * conj(v[t](k, v_ind(c,x))) 
                                      * conj(v[t](l, v_ind(d,x)));
                    } // l
                  } // k
                } // j 
              } // i 
            } // x 
          } // if epsilon 
        } // d
      } // c
    } // b
  } // a
} // get_T
