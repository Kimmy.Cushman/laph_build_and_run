#!/bin/bash
export GCC_VERSION="8.3.1"
set -u
module load gcc/${GCC_VERSION}

# get variables from bash script (all CAPS)
source run_ensemble.sh 
#############################################
# Edit here 
#############################################
# compile or run? 
compile=1
run=1
# parameters of this run 
Nvec=64
num_times_per_job=2
time_job=4 #00 #L32 about 18 min per config per time
cfg_start=55
cfg_stop=60
cfg_step=5
t_start=0
t_stop=${NT}

#############################################
# Rarely edit
Step2_dir="${laph_HOME}/Step2_Distill_vecs" # location of compile script, main program
exe="${Step2_dir}/distill_vecs_Lx${LX}_Nc${NC}.exe"

tasks=1 # couldn't get to parallelize further  
nodes=1
threads=32 # per task
geom_x=1 # can't get nontrivial to work, 
geom_y=1
geom_z=1 # Nd=3, no geom_t 


############### IF COMPILE ####################
if [ ${compile} == 1 ]
then 
  sh ${Step2_dir}/compile_main.sh ${NC} ${LX} ${exe} ${Step2_dir} ${laph_HOME}
  wait 
fi # compile conditional 


############### IF RUN ####################
if [ ${run} == 1 ]
then 
  for (( i=${cfg_start}; i<${cfg_stop}; i+=${cfg_step} ))
  do 
    echo ${i}
    spliced_input_mod="${SPLICED_OUTPUT_BASE}_cfg${i}.mod"
    Nvec_dir="${KAPPA_DIR}/LapH_vecs/Nvec${Nvec}"
    mkdir -p ${Nvec_dir}
    #laph_dir="${Nvec_dir}/cfg_${i}_${ROTATION}_${ROTATION}_${ROTATION}"
    laph_dir="${Nvec_dir}/cfg_${i}"
    mkdir -p ${laph_dir}
    laph_vecs_file_t="${laph_dir}/LapH_${Nvec}vecs_t"
    if [  -f ${spliced_input_mod} ]
    then
      echo "starting"
      # loop over t_start for each job
      for (( t_start_job=${t_start}; t_start_job<${t_stop}; t_start_job+=${num_times_per_job} ))
      do 
        # check if vecs for these timeslices already exist 
        t_range_done=1                                        # change back to 1 
        t_stop_job=$(( t_start_job+ num_times_per_job ))
        for (( t=${t_start_job}; t<${t_stop_job}; t+=1 ))
        do
          if [[ ! -f "${laph_vecs_file_t}${t}.mod" || 
                ! -f "${laph_vecs_file_t}${t}_evecs.db" ]] 
          then 
            echo "${laph_vecs_file_t}${t}"
            t_range_done=0
          fi 
        done # t loop
        
        # if t range done 
        if [ ${t_range_done} == 1 ]
        then
          echo "cfg ${i} vecs from ${t_start_job} to ${t_stop_job} done"
        else
          echo "cfg ${i} vecs from ${t_start_job} t0 ${t_stop_job} ----NOT---- done"
        fi

        # if t range NOT done - do it! 
        if [[ ${t_range_done} == 0 ]]
        then
          batch_output_file="${laph_dir}/vectors_Nvec${Nvec}_tstart${t_start_job}_tstop${t_stop_job}.out"
          rm ${batch_output_file} 2> /dev/null
          ############### RUN SCRIPT HERE ##############################
          export OMP_NUM_THREADS=${threads}
          run="srun -t ${time_job} -p ${QUEUE} -o ${batch_output_file} -n ${tasks} -N ${nodes}"
          geom="-geom ${geom_x} ${geom_y} ${geom_z}"
          input="${spliced_input_mod} ${laph_vecs_file_t} ${NT} ${Nvec} ${t_start_job} ${t_stop_job}"
          runme=" ${run} ${exe} ${input} ${geom} < /dev/null"
          echo "Step 2 CONFIG ${i} tstart:${t_start_job}, tstop:${t_stop_job}"
          ${runme} &> /dev/null & ## runs script here 
          echo "${runme}" 
          echo "" 
        fi 
      done # t start loop
    else
      echo "no splice file for config ${i}"
    fi
  done # config loop
fi # run conditional 


