import numpy as np
import os

for cfg in np.arange(100, 1000, 5):
    filename = "L32T64/beta11p028/kappaQUENCHED/Observables/kappa0p15625/Buchoff/batch_output/spectrum_cfg%i.out"%cfg
    with open(filename, "r") as f:
        lines = [line for line in f]
        if "CHROMA: ran successfully" in lines[-1]:
            print("success. cfg %i"%cfg) 
        else:
            print("-------------- cfg %i not done"%cfg)
        #    os.system("./run_Buchoff.sh %i %i"%(cfg, cfg + 5))
