import numpy as np
import os 


dirname = "L32T64/beta12p0/kappaQUENCHED/Perams/kappa0p1475/LapH/Nvec128/perambulators"



for cfg in np.arange(100, 1000, 5):
    old = "perambulators_k0p1475_LapH_128vecs_cfg%i.sdb"%cfg
    new = "perambulators_k0p1475_LapH_128vecs_tsource0_cfg%i.sdb"%cfg
    command = "mv %s/%s %s/%s"%(dirname, old, dirname, new)
    os.system(command)
