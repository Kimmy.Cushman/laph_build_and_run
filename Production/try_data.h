// ---                           DATA                            --- //
////////////////////////// ****************** /////////////////////////
//! Data structure define
struct EvecDBData_t
{
  std::vector<std::complex<double>> data = std::vector<std::complex<double>>(Nc*Lx*Lx*Lx); 
};
// end define 


//! Reader
void read(BinaryReader& bin, EvecDBData_t& param)
{
  read(bin, param.data);
}

//! Writer
void write(BinaryWriter& bin, const EvecDBData_t& param)
{
  write(bin, param.data);
}


// Concrete data class
template<typename D>
class EvecDBData : public DBData
{
public:
  //! Default constructor
  EvecDBData() {} 

  //! Constructor
  EvecDBData(const D& d) : data_(d) {}

  //! Setter
  D& data() {return data_;}

  //! Getter
  const D& data() const {return data_;}

  // Part of Serializable
  //virtual unsigned short serialID (void) const {return 123;}
  const unsigned short serialID (void) const {return 123;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, data());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, data());
  }

private:
  D  data_;
};
