#!/bin/bash
set -u 

cfg_start=1350
cfg_stop=6000
cfg_step_job=50

time_job=15 #less than 10  mins for L=32, cfg_step_job=20, Nvec8
Nvec_observables=$1

for (( cfg_start_job=${cfg_start}; cfg_start_job<$((${cfg_stop} - ${cfg_step_job} + 1)); cfg_start_job+=${cfg_step_job} ))
do
  cfg_stop_job=$((${cfg_start_job} + ${cfg_step_job}))
  run="./run_Step4_observables.sh ${cfg_start_job} ${cfg_stop_job} ${Nvec_observables} ${time_job} 0"
  outfile="observables_${cfg_start_job}_${Nvec_observables}.log"
  runme="${run}" # &> ${outfile} &"
  echo ${runme}
  ${runme} &> ${outfile} &
done


#for (( tsource=0; tsource<8; tsource+=1 ))
#do
#  for Nvec in 4 
#  do
#    run="./run_Step4_observables.sh ${Nvec} ${tsource}"
#    outfile="mesons_Nvec${Nvec}_tsource${tsource}.log"
#    runme="${run} &> ${outfile} &"
#    echo ${runme}
#    ${runme} &> ${outfile} &
#    done
#done
