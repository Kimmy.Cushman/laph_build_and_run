#!/bin/bash
# get variables from bash script (all CAPS)
set -u
source run_ensemble.sh 
#############################################
# Edit here 
#############################################
# compile or run? 
compile=1
run=1
# parameters of this run 
cfg_start=$1
cfg_stop=$2
cfg_step=5
Nvec_observables=$3
time_job=$4 #L32 about 1 min per config 
Nvec_vecs=64
Nnoise_vecs=64
Nvec_peram=20
specify_t_source=1
tsource=$5

kappa_peram="1515" 
mass_or_kappa_peram="k" #"1520" # 0.1520/0p1520
quarkline_type="LapH"
state="baryon_udud"
spin_matrix_filename="kimmy.txt"
wick_filename="kimmy.txt"
#############################################


#############################################
# FILE LOCATIONS
Step4_dir="${laph_HOME}/Step4_Observables" # location of compile script, main program
exe="${Step4_dir}/observables_Lx${LX}_Nt${NT}_Nvec${Nvec_observables}.exe"
echo "run exe ${exe}"

# peram and corr file location depends on kappa, quarkline type, Nvec, Nnoise
peram_dir="${KAPPA_DIR}/Perams/kappa0p${kappa_peram}/${quarkline_type}"
corr_dir="${KAPPA_DIR}/Observables/kappa0p${kappa_peram}/${state}/${quarkline_type}"
if [ ${quarkline_type} == "LapH" ]
then
  peram_dir="${peram_dir}/Nvec${Nvec_peram}"
  corr_dir="${corr_dir}/Nvec${Nvec_observables}"
else
  peram_dir="${peram_dir}/Nvec${Nvec_vecs}_Nnoise${Nvec_peram}"
  corr_dir="${corr_dir}/Nvec${Nvec_vecs}_Nnoise${Nvec_observables}"
fi 
if [ ${specify_t_source} == 1 ]
then
  peram_filename_base="${peram_dir}/perambulators/perambulators_${mass_or_kappa_peram}0p${kappa_peram}_${quarkline_type}_${Nvec_peram}vecs_tsource${tsource}" # base_cfg${cfg}.sdb
  output_base="${state}_${mass_or_kappa_peram}0p${kappa_peram}_${quarkline_type}_${Nvec_observables}vecs_tsource${tsource}"
else
  peram_filename_base="${peram_dir}/perambulators/perambulators_${mass_or_kappa_peram}0p${kappa_peram}_${quarkline_type}_${Nvec_peram}vecs" # base_cfg${cfg}.sdb
  output_base="${state}_${mass_or_kappa_peram}0p${kappa_peram}_${quarkline_type}_${Nvec_observables}vecs"
fi
# batch: base_cfg${cfg}.out, corr_savename: 
batch_dir="${corr_dir}/batch_output"

mkdir -p ${batch_dir}
mkdir -p ${corr_dir}
# vecs file location depends on cfg number

#############################################

############### IF COMPILE ####################
if [ ${compile} == 1 ]
then 
  sh ${Step4_dir}/compile_main.sh ${NC} ${LX} ${NT} ${Nvec_observables} ${exe} ${Step4_dir} ${laph_HOME}
  wait 
fi # compile conditional 

############### IF RUN ####################
if [ ${run} == 1 ]
then 
  # eigen vectors filename = vecs_file_t${t}.mod/.db 
  if [ ${quarkline_type} == "sLapH" ]
  then
    vecs_dir="${KAPPA_DIR}/sLapH_vecs/Nvec${Nvec_vecs}/Nnoise${Nnoise_vecs}"
    vecs_file_t="sLapH_${Nvec_vecs}vecs_Nnoise${Nnoise_vecs}_t"
  else
    vecs_dir="${KAPPA_DIR}/LapH_vecs/Nvec${Nvec_vecs}"
    vecs_file_t="LapH_${Nvec_vecs}vecs_t"
  fi
  
  # check if vecs for these timeslices already exist 
  # loop over t_start for each job
  missing_vecs=0
  for (( i=${cfg_start}; i<${cfg_stop}; i+=${cfg_step} ))
  do 
    #for (( t=0; t<${NT}; t++ ))
    for (( t=6; t<7; t++ ))
    do 
      vecs_file_t_full="${vecs_dir}/cfg_${i}/${vecs_file_t}"
      #if [ ! -f "${vecs_file_t_full}${t}.db" ]
      if [ ! -f "${vecs_file_t_full}${t}_evecs.db" ]
      #if [ ! -f "${vecs_file_t_full}${t}_evals.db" ]
      then 
        #echo "missing ${vecs_file_t_full}${t}_evecs.db"
        echo "missing ${vecs_file_t_full}${t}.db"
        missing_vecs=1
      fi 
    done # t loop
    
    peram_filename="${peram_filename_base}_cfg${i}.sdb"
    if [[ ! -f ${peram_filename} ]]
    then
      echo "missing ${peram_filename}"
      missing_vecs=0   # FIXME
    fi
  done
  if [ ${missing_vecs} == 1 ]
  then
      echo "missing peram ${peram_filename_base} or vecs ${vecs_dir} ${vecs_file_t}"
  else
    batch_output_file="${batch_dir}/${output_base}_cfg${i}.out"
    rm ${batch_output_file} 2> /dev/null
    ############### RUN SCRIPT HERE ##############################
    run="srun -t ${time_job} -p ${QUEUE} -o ${batch_output_file}"
    input="${peram_filename_base} ${vecs_dir} ${vecs_file_t} ${spin_matrix_filename} ${wick_filename} ${corr_dir} ${state} ${cfg_start} ${cfg_stop} ${cfg_step} ${tsource}"
    runme=" ${run} ${exe} ${input} < /dev/null"
    echo "Step 4 CONFIG ${cfg_start} ${cfg_stop} ${cfg_step}"
    #echo "RUN   : ${run}"   
    #echo "EXE   : ${exe}"  
    #echo "INPUT : ${input}" 
    echo "RUNME : ${runme}"
    ${runme} &> /dev/null & ## runs script here 
    echo "" 
  fi 
fi # run conditional 
