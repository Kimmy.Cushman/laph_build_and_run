#!/bin/bash
set -u
echo "HELLO"
laph_HOME="/p/lustre1/cushman2/laph_build_and_run"

Lx=$1
beta=$2
kappa_cfg=$3
quarkline_type=$4
Nvec=$5
Nnoise=$6

Nt=$(( 2*${Lx} ))

LapH_dir="${laph_HOME}/Production/L${Lx}T${Nt}/beta${beta}/kappa${kappa_cfg}/${quarkline_type}_vecs/Nvec${Nvec}"
vecs_file_t="${quarkline_type}_${Nvec}vecs"
if [ ${quarkline_type} == "sLapH" ]
then
  LapH_dir="${LapH_dir}/Nnoise${Nnoise}"
  vecs_file_t="${vecs_file_t}_Nnoise${Nnoise}"
fi
vecs_file_t="${vecs_file_t}_t"
count_file="${LapH_dir}/count.txt"

echo "${count_file}"

if [[ -d ${LapH_dir} ]]
then

  LapH_vec_count=0
  for dir in ${LapH_dir}/*/
  do
    echo ${dir}
    num_times=0
    for (( t=0; t<${Nt}; t+=1 ))
    do 
      filename="${vecs_file_t}${t}"
      if [[ -f "${dir}/${filename}.mod" && -f "${dir}/${filename}.db"  ]]
      then
        ((num_times++))
      fi
    done
    echo "${num_times}"
    if [ ${num_times} == ${Nt} ]
    then
      ((LapH_vec_count++))
    fi
  done

  echo ${LapH_vec_count} > ${count_file}

fi
#./count_LapH_vecs.sh 100 1000 5 16 11p5 QUENCHED LapH 128 none
