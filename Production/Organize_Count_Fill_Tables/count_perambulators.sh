#!/bin/bash
set -u
laph_HOME="/p/lustre1/cushman2/laph_build_and_run"

Lx=$1
beta=$2
kappa_cfg=$3
quarkline_type=$4
Nvec=$5
Nnoise=$6
kappa=$7

Nt=$(( 2*${Lx} ))

peram_dir="${laph_HOME}/Production/L${Lx}T${Nt}/beta${beta}/kappa${kappa_cfg}/Perams/kappa${kappa}/${quarkline_type}"
if [ ${quarkline_type} == "LapH" ]
then
  peram_dir="${peram_dir}/Nvec${Nvec}/perambulators"
  peram_base="${peram_dir}/perambulators_k${kappa}_${quarkline_type}_${Nvec}vecs"
else
  peram_dir="${peram_dir}/Nvec${Nvec}_Nnoise${Nnoise}/perambulators"
  peram_base="${peram_dir}/perambulators_k${kappa}_${quarkline_type}_${Nnoise}vecs"
fi 

peram_count=$(ls ${peram_base}_cfg*.sdb 2>/dev/null | wc -l)
count_file="${peram_dir}/count.txt"
echo ${peram_count} > ${count_file}


#./count_perambulators.sh 100 1000 5 16 11p5 QUENCHED LapH 128 none 0p1515
