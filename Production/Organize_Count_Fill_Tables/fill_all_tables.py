import numpy as np
import subprocess
import os 
import glob


Production_dir = "/p/lustre1/cushman2/laph_build_and_run/Production"
Observables_dir = "%s/Observables_npy"%Production_dir

Tables_temporary_dir = "%s/Organize_Count_Fill_Tables/Tables"%Production_dir
Final_Tables_dir="/p/lustre1/cushman2/Tables/chrisculver.github.io/su4"

start_row = "        <tr>"
end_row = "</tr>\n"
pre = "<td>"
post = "</td>"


BUCHOFF_TIME_DICT = {"32 11p028 0p1554": 190,
                     "32 11p028 0p15625": 240,
                     "32 11p5 0p1515": 210, 
                     "32 12p0 0p1475": 140,
                     "16 11p028 0p1554": 10,
                     "16 11p028 0p15625": 10,
                     "16 11p5 0p1515": 10, 
                     "16 12p0 0p1475": 10}

# need to specify Nvec for this to make sense 
LAPH_TIME_DICT = {"32 12p0": 18,
                  "32 11p5": 18}


PERAM_TIME_DICT = {"32 12p0 0p1475 128": 900,
                   "32 11p5 0p1515 100": 1400}

MESON_TIME_DICT = {"32 12p0 0p1475 LapH_Nvec4": "&lt 5",
                   "32 12p0 0p1475 LapH_Nvec8": "&lt 5",
                   "32 12p0 0p1475 LapH_Nvec12": "&lt 5",
                   "32 12p0 0p1475 LapH_Nvec16": "&lt 5",
                   "32 12p0 0p1475 LapH_Nvec24": "&lt 5",
                   "32 12p0 0p1475 LapH_Nvec32": "&lt 5",
                   "32 12p0 0p1475 LapH_Nvec48": "&lt 5"}


def get_elements(values):
    all_elements = ""
    for value in values:
        all_elements += pre + str(value) + post 
    return all_elements


def get_LapH_full_row(known_elements, N, time):
    full_row = start_row + known_elements \
                + pre + str(N) + post \
                + pre + str(time) + post \
                + end_row
    return full_row 


def get_num_cfgs_observables(Lx, Nt, beta, kappa_cfg, kappa, state, smearing): 
    savename = "%s/L%iT%i"%(Observables_dir, Lx, Nt)
    savename += "_beta%s_kcfg%s_k%s"%(beta, kappa_cfg, kappa)
    savename += "_%s_%s"%(smearing, state)
    filename = glob.glob("%s*"%savename)
    if len(filename) != 0:
        filename = filename[0] # should only be one file with this name 
        filename_no_npy = filename[:-4]
        N = int(filename_no_npy.split("N")[-1])
        return N
    else:
        return 0

def get_num_cfgs_perams(Lx, Nt, beta, kappa_cfg, kappa, quarkline_type, Nvec, Nnoise):
    count_dir = "%s/L%iT%i/beta%s/kappa%s/Perams/kappa%s"%(Production_dir, Lx, Lx*2, beta, kappa_cfg, kappa)
    count_dir = "%s/%s/Nvec%i"%(count_dir, quarkline_type, Nvec)
    if quarkline_type == "sLapH":
        count_dir = "%s/Nnoise%i"%(count_dir, Nnoise)
    count_file = "%s/perambulators/count.txt"%count_dir
    try:
        N = np.loadtxt(count_file)
        return int(N)
    except IOError:
        print("Need to count peram files for")
        print(Lx, Nt, beta, kappa_cfg, kappa, quarkline_type, Nvec, Nnoise)
        print()
        return 0


def get_num_cfgs_LapH(Lx, Nt, beta, kappa_cfg, quarkline_type, Nvec, Nnoise):
    count_dir = "%s/L%iT%i/beta%s/kappa%s"%(Production_dir, Lx, Lx*2, beta, kappa_cfg)
    count_dir = "%s/%s_vecs/Nvec%i"%(count_dir, quarkline_type, Nvec)
    if quarkline_type == "sLapH":
        count_dir = "%s/Nnoise%i"%(count_dir, Nnoise)
    count_file = "%s/count.txt"%count_dir
    print("count file")
    print(count_file)
    try:
        N = np.loadtxt(count_file)
        return int(N)
    except IOError:
        print("Need to count vec files for")
        print(Lx, Nt, beta, kappa_cfg, quarkline_type, Nvec, Nnoise)
        print()
        return 0


def get_row(table_type, filename, all_args):
    filename_old = "%s/%s.html"%(Final_Tables_dir, filename)
    # process args to pass to bash counting script 
    Lx, beta, kappa_cfg, quarkline_type, \
    Nvec, Nnoise, kappa, state, smearing = all_args
    Nt = 2*Lx

    if table_type == "LapH":
        #<th scope="col">L,T</th>
        #<th scope="col">beta</th>
        #<th scope="col">kappa cfg</th>
        #<th scope="col">(s)LapH</th>
        #<th scope="col">Nvec</th>
        #<th scope="col">Nnoise</th>
        #<th scope="col">N</th>
        #<th scope="col">time (min)</th>    
        if quarkline_type == "LapH":
            Nnoise = "-"
        all_known_elements = get_elements(values=["%i,%i"%(Lx,Nt), beta, kappa_cfg, quarkline_type, Nvec, Nnoise])
        N = get_num_cfgs_LapH(Lx, Nt, beta, kappa_cfg, quarkline_type, Nvec, Nnoise) 
        print("LapH count")
        print(N)
        
    elif table_type == "perambulator":
        #<th scope="col">L,T</th>
        #<th scope="col">beta</th>
        #<th scope="col">kappa cfg</th>
        #<th scope="col">kappa</th>
        #<th scope="col">(s)LapH</th>
        #<th scope="col">Nvec</th>
        #<th scope="col">Nnoise</th>
        #<th scope="col">N</th>
        #<th scope="col">time (min)</th>
        if quarkline_type == "LapH":
            Nnoise = "-"
        all_known_elements = get_elements(values=["%i,%i"%(Lx,Nt), beta, kappa_cfg, kappa, quarkline_type, Nvec, Nnoise])
        N = get_num_cfgs_perams(Lx, Nt, beta, kappa_cfg, kappa, quarkline_type, Nvec, Nnoise) 

    
    elif table_type == "observables":
        #<th scope="col">L,T</th>
        #<th scope="col">beta</th>
        #<th scope="col">kappa cfg</th>
        #<th scope="col">kappa</th>
        #<th scope="col">state</th>
        #<th scope="col">smearing</th>    # LapH 128 OR sLapH 128 24 OR Buchoff POINT_SOURCE_POINT_SINK
        #<th scope="col">N</th>
        #<th scope="col">time (min)</th>
        all_known_elements = get_elements(values=["%i,%i"%(Lx,Nt), beta, kappa_cfg, kappa, state, smearing])
        N = get_num_cfgs_observables(Lx, Nt, beta, kappa_cfg, kappa, state, smearing)

    if N == 0:
        return "None", "None"
    
    else:
        # get new lines to write to html 
        new_lines = []
        time = "?"
        if "Buchoff" in smearing:
            time = BUCHOFF_TIME_DICT[" ".join([str(Lx), beta, kappa])]
        elif table_type == "LapH":
            try:
                time = LAPH_TIME_DICT[" ".join([str(Lx), beta])]
            except KeyError:
                time = "?"
        elif table_type == "perambulator":
            if quarkline_type == "LapH":
                key = " ".join([str(Lx), beta, kappa, str(Nvec)])
            try:
                time = PERAM_TIME_DICT[key]
            except KeyError:
                time = "?"
        elif table_type == "observables":
            meson_list = ["g1meson", "g2meson", "g3meson", "g5meson", "g4g1meson", "g4g2meson", "g4g3meson", "g4g5meson"]
            if state in meson_list:
                key = " ".join([str(Lx), beta, kappa, smearing])
            try:
                time = MESON_TIME_DICT[key]
            except KeyError:
                time = "?"
        else:
            with open(filename_old, "r") as f:
                for line in f:
                    if all_known_elements in line:
                        elements = line.split("</td><td>")
                        time_element = elements[-1]
                        time = time_element.split("<")[0]
         
        full_row = get_LapH_full_row(all_known_elements, N, time)
    return full_row, all_known_elements


def fill_new_from_original(new_rows, new_rows_base, filename):
    file_original = "%s/%s_original.html"%(Tables_temporary_dir, filename)
    file_new = "%s/%s.html"%(Final_Tables_dir, filename)
    
    os.system("cp %s %s"%(file_new, file_original))
    print("file_original") 
    print(file_original) 
    print("file_new") 
    print(file_new)
    print()
    full_lines = []
    with open(file_original, "r") as f:
        for line in f:
            # check if the line needs to be updated, 
            # if yes, append the new line, 
            # and remove that row from the list of new rows 
            updated_line = False
            for new_row, new_row_base in zip(new_rows, new_rows_base):
                if new_row_base in line:
                    full_lines.append(new_row)
                    
                    new_rows.remove(new_row)
                    new_rows_base.remove(new_row_base)

                    updated_line = True 
                    break

            # if didn't just update that line, add the original line 
            # and the rest of the new rows
            if updated_line == False:
                if "</tbody>" in line:
                    full_lines.extend(new_rows)
                    full_lines.append(line)
                else:
                    full_lines.append(line)

    with open(file_new, "w") as f:
        for line in full_lines:
            f.write(line)


def old_new_files(table_type):
    filename_new = "%s/%s_table.html"%(Final_Tables_dir, table_type)
    filename_old = "%s/%s_table_old.html"%(Tables_template_dir, table_type)
    os.system("cp %s %s"%(filename_new, filename_old))
    filename_template = "%s/%s_table_template.html"%(Tables_template_dir, table_type)
    return filename_new, filename_old, filename_template


def get_Nvec_list(ensemble_dir, quarkline_type):
    if quarkline_type == "LapH":
        Nvec_list = []
        Nvec_paths = glob.glob("%s/*/"%ensemble_dir)
        for path in Nvec_paths:
            path = os.path.normpath(path)
            Nvec_dirname = path.split(os.sep)[-1]
            Nvec = int(Nvec_dirname.split("Nvec")[-1])
            Nvec_list.append(Nvec)
        if len(Nvec_list) > 0:
            return [max(Nvec_list)], ["-"]
        else:
            return [], []
    else:
        Nvec_list = []
        Nnoise_list = []
        Nvec_paths = glob.glob("%s/*/"%ensemble_dir)
        for Nvec_path in Nvec_paths:
            Nvec_path = os.path.normpath(Nvec_path)
            Nvec_dirname = Nvec_path.split(os.sep)[-1]
            Nvec = int(Nvec_dirname.split("Nvec")[-1])
            Nvec_list.append(Nvec)
            # get max Nnoise within this Nvec dir by taking max of Nnnoise_list_temp
            Nnoise_list_temp = []
            Nnoise_paths = glob.glob("%s/Nvec%i/*/"%(ensemble_dir, Nvec))
            for Nnoise_path in Nnoise_paths:
                Nnoise_path = os.path.normpath(Nnoise_path)
                Nnoise_dirname = Nnoise_path.split(os.sep)[-1]
                Nnoise = int(Nnoise_dirname.split("Nnoise")[-1])
                Nnoise_list_temp.append(Nnoise)
            if len(Nnoise_list_temp) == 0:
                Nvec_list.remove(Nvec)
                continue
            else:
                Nnoise_list.append(max(Nnoise_list_temp)) 
        return Nvec_list, Nnoise_list 


def fill_LapH_table():
    # make copy of html, (table_old) to read from,
    # will write to filename 
    table_type="LapH"
    filename = "%s_table"%table_type  # .html 
    new_rows = []
    new_rows_base = []
    kappa, state, smearing = "none", "none", "none"
    for quarkline_type in ["LapH", "sLapH"]:
        for Lx in [32]:
            for beta in ["11p028", "11p5", "12p0"]:
                for kappa_cfg in ["QUENCHED"]:
                    kappa_dir="%s/L%iT%i/beta%s/kappa%s"%(Production_dir, Lx, 2*Lx, beta, kappa_cfg)
                    if os.path.isdir(kappa_dir):            
                        # only care about files with the max number of vecs 
                        vec_dir = "%s/%s_vecs"%(kappa_dir, quarkline_type)
                        Nvec_list, Nnoise_list = get_Nvec_list(vec_dir, quarkline_type)
                        for Nvec, Nnoise in zip(Nvec_list, Nnoise_list):
                            all_args = [Lx, beta, kappa_cfg, quarkline_type,
                                        Nvec, Nnoise, 
                                        kappa, state, smearing]
                            new_row, new_row_base = get_row(table_type, filename, all_args)
                            if new_row != "None":
                                new_rows.append(new_row)
                                new_rows_base.append(new_row_base)
    fill_new_from_original(new_rows, new_rows_base, filename)
    

def fill_perambulator_table():
    table_type="perambulator"
    filename = "%s_table"%table_type  # .html 
    new_rows = []
    new_rows_base = []
    state, smearing = "none", "none"
    Nvec_list = [64, 128]
    Nnoise_list = []
    for quarkline_type in ["LapH", "sLapH"]:
        for Lx in [32]:
            Nt = 2*Lx
            for beta in ["11p028", "11p5", "12p0"]:
                for kappa_cfg in ["QUENCHED"]:
                    for kappa in ["0p1475", "0p1515", "0p1554", "0p15625"]:
                        kappa_dir = "%s/L%iT%i/beta%s/kappa%s/Perams/kappa%s"%(Production_dir, Lx, Nt, beta, kappa_cfg, kappa)
                        if os.path.isdir(kappa_dir):            
                            peram_dir = "%s/%s"%(kappa_dir, quarkline_type)
                            # only care about perams with the max number of vecs 
                            Nvec_list, Nnoise_list = get_Nvec_list(peram_dir, quarkline_type)
                            for Nvec, Nnoise in zip(Nvec_list, Nnoise_list):
                                all_args = [Lx, beta, kappa_cfg, quarkline_type,
                                            Nvec, Nnoise, 
                                            kappa, state, smearing]
                                new_row, new_row_base = get_row(table_type, filename, all_args)
                                if new_row != "None":
                                    new_rows.append(new_row)
                                    new_rows_base.append(new_row_base)
    fill_new_from_original(new_rows, new_rows_base, filename)


def get_state_list():
    # LapH states
    for state_type in ["baryon_udud", "mesons"]:
        if state_type == "baryon_udud":
            state_list = ["baryon_udud"]
        elif state_type == "mesons":
            state_list = ["g1meson", "g2meson", "g3meson", "g4g1meson", "g4g2meson", "g4g3meson", "g4g5meson", "g5meson"]
        else:
            break 
    state_list.extend(["axial", "vector", "pion"])
    for spin in [0,1,2]:
        for com in [3,4]:
            state_list.append("bary_2u2d_com%i_Spin%i"%(com, spin))
        for com in [1,2]:
            state_list.append("bary_3u1d_com%i_Spin%i"%(com, spin)) 
    return state_list

def fill_observables_table():
    table_type = "observables"
    filename = "%s_table"%table_type  # .html 
    new_rows = []
    new_rows_base = []
    quarkline_type, Nvec, Nnoise = "none", "none", "none"
    for Lx in [32]:
        for beta in ["11p028", "11p5", "12p0"]:
            for kappa_cfg in ["QUENCHED"]:
                for kappa in ["0p1475", "0p1515", "0p1554", "0p15625"]:
                    if os.path.isdir("%s/L%iT%i/beta%s/kappa%s/Observables/kappa%s"%(Production_dir, Lx, 2*Lx, beta, kappa_cfg, kappa)):            
                        for state in get_state_list():
                            for smearing in ["Buchoff_POINT_SOURCE_POINT_SINK", 
                                             "LapH_Nvec4", 
                                             "LapH_Nvec8", 
                                             "LapH_Nvec12", 
                                             "LapH_Nvec16", 
                                             "LapH_Nvec20", 
                                             "LapH_Nvec24", 
                                             "LapH_Nvec32", 
                                             "LapH_Nvec48", 
                                             "LapH_Nvec64",
                                             "LapH_Nvec72",
                                             "LapH_Nvec88"]:
                                all_args = [Lx, beta, kappa_cfg, quarkline_type,
                                            Nvec, Nnoise, 
                                            kappa, state, smearing]
                                new_row, new_row_base = get_row(table_type, filename, all_args)
                                if new_row != "None":
                                    new_rows.append(new_row)
                                    new_rows_base.append(new_row_base)
    fill_new_from_original(new_rows, new_rows_base, filename)


#fill_LapH_table()
#fill_perambulator_table()
fill_observables_table()






