import numpy as np
import os 
import sys

laph_HOME = "/p/lustre1/cushman2/laph_build_and_run"
save_dir = "%s/Production/Observables_npy"%laph_HOME

distill = True
Buchoff = False

# config params 
Lx = 32
Nt = 2*Lx
beta = "12p0"
kappa_cfg = "QUENCHED"
kappa_observables = "0p1475"

cfg_start = 100
cfg_stop = 6000
cfg_step = 50

# Distillation params
quarkline_type = "LapH"

# file locations 
kappa_dir = "%s/Production/L%iT%i/beta%s/kappa%s"%(laph_HOME, Lx, Nt, beta, kappa_cfg)
observables_dir = "%s/Observables/kappa%s"%(kappa_dir, kappa_observables)


Nvec_list = [8,12,16,24,36]

if distill:
    for Nvec_observables in Nvec_list:
        if quarkline_type == "LapH":
            Nvec_label = "Nvec%i"%Nvec_observables
        else:
            Nvec_label = "Nvec%i_Nnoise%i"%(Nvec, Nvec_observables)
        
        for state_type in ["mesons"]: #, "baryon_udud"]:
            distill_state_dir = "%s/%s/%s/%s"%(observables_dir, state_type, quarkline_type, Nvec_label)
            print(distill_state_dir)
            if state_type == "baryon_udud":
                state_list = ["baryon_udud"]
            elif state_type == "mesons":
                state_list = ["g1meson", "g2meson", "g3meson", "g5meson"] #, "g0g1meson", "g0g2meson", "g0g3meson", "g0g5meson"]
            else:
                break 
            
            for state in state_list:
                corr = []
                for cfg in np.arange(cfg_start, cfg_stop, cfg_step):
                    filename = "%s/%s_cfg%i.txt"%(distill_state_dir, state, cfg)
                    corr_i = np.zeros(Nt, dtype=np.cfloat)
                    if os.path.isfile(filename):
                        with open(filename, "r") as f:
                            for t, line in enumerate(f):
                                re, im = line.split(",")
                                corr_i[t] = float(re) + 1j*float(im)
                        corr.append(corr_i)
                    else:
                        print("no config %i found"%cfg)

                N_corr = len(corr)
                if N_corr > 1:
                    savename = "%s/L%iT%i"%(save_dir, Lx, Nt)
                    savename += "_beta%s_kcfg%s_k%s"%(beta, kappa_cfg, kappa_observables)
                    savename += "_%s_%s"%(quarkline_type, Nvec_label)
                    savename += "_%s"%state
                    savename_N = savename + "_N%i"%N_corr
                    
                    print(savename)
                    os.system("rm %s*"%savename)
                    np.save(savename_N, np.array(corr))

if Buchoff:
    smear = "point"
    if smear == "point":
        source = "POINT_SOURCE_POINT_SINK"
    else:
        source = "SHELL_SOURCE_POINT_SINK"
    #state_dir = "%s/Buchoff/%s/spectrum/raw_output"%(observables_dir, smear)
    state_dir = "%s/Buchoff/point/spectrum/raw_output"%(observables_dir)
    
    state_list = ["vector1", "vector2", "vector3"]
    #state_list += ["axial", "pion", "bary_4u_Spin2"]
    #for spin in [0,1,2]:
    #    for com in [4]:
    #        state_list.append("bary_2u2d_com%i_Spin%i"%(com, spin))
    #    for com in [1,2]:
    #        state_list.append("bary_3u1d_com%i_Spin%i"%(com, spin)) 
    print(state_list)
    for state in state_list:
        print(state)
        corr = []
        for cfg in np.arange(cfg_start, cfg_stop, cfg_step):
            if cfg%1000 == 0:
                print("   %i/%i"%(cfg, cfg_stop))
            filename_re = "%s/cfg%i/%s_%s"%(state_dir, cfg, state, source)
            filename_im = "%s/cfg%i/%s_%s_im"%(state_dir, cfg, state, source)
            corr_i = np.zeros(Nt, dtype=np.cfloat)
            if os.path.isfile(filename_re):
                try:
                    with open(filename_re, "r") as f:
                        for line in f:
                            vals_re = line.split(" ")[4:]
                    with open(filename_im, "r") as f:
                        for line in f:
                            vals_im = line.split(" ")[4:]
                    for t in range(Nt):
                        corr_i[t] = float(vals_re[t]) + 1j*float(vals_im[t])
                    corr.append(corr_i)
                except:
                    pass
        
        N_corr = len(corr)
        print(N_corr)
        if N_corr > 1:
            savename = "%s/L%iT%i"%(save_dir, Lx, Nt)
            savename += "_beta%s_kcfg%s_k%s"%(beta, kappa_cfg, kappa_observables)
            savename += "_Buchoff_%s_%s"%(source, state)
            savename_N = savename + "_N%i"%N_corr
            
            os.system("rm %s*"%savename)
            np.save(savename_N, np.array(corr))



















#
