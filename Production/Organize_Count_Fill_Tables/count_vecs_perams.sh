#!/bin/bash
set -u
count_dir="/p/lustre1/cushman2/laph_build_and_run/Production/Organize_Count_Fill_Tables"

Lx=32
beta="12p0"
kappa_cfg="QUENCHED"
Nvec=256
Nnoise=256
kappa="0p1475"

run="srun -t 10 -p pbatch"
input_LapH="${Lx} ${beta} ${kappa_cfg} LapH ${Nvec} none"
input_sLapH="${Lx} ${beta} ${kappa_cfg} sLapH ${Nvec} ${Nnoise}"
input_LapH_perams="${Lx} ${beta} ${kappa_cfg} LapH ${Nvec} none ${kappa}"
input_sLapH_perams="${Lx} ${beta} ${kappa_cfg} sLapH ${Nvec} ${Nnoise} ${kappa}"

LapH_exe="${count_dir}/count_LapH_vecs.sh"
Peram_exe="${count_dir}/count_perambulators.sh"

echo "${run}"
echo "${LapH_exe}"
echo "${input_LapH}"
LapH_run="${run} ${LapH_exe} ${input_LapH} </dev/null"
sLapH_run="${run} ${LapH_exe} ${input_sLapH} </dev/null"
LapH_perams_run="${run} ${Peram_exe} ${input_LapH_perams} </dev/null"
sLapH_perams_run="${run} ${Peram_exe} ${input_sLapH_perams} </dev/null"

${LapH_run} &> /dev/null &
${sLapH_run} &> /dev/null &
${LapH_perams_run} &> /dev/null &
${sLapH_perams_run} &> /dev/null &


