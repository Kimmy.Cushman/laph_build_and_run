import numpy as np
import os

beta = "12p0"
kappa = "0p1475"

for cfg in np.arange(1000, 1400, 5):
    outfile = "L32T64/beta%s/kappaQUENCHED/Observables/kappa%s/Buchoff/point/batch_output/spectrum_point_cfg%i.out"%(beta, kappa, cfg)
    with open(outfile, "r") as f:
        for i, line in enumerate(f):
            last_line = line
        if "successfully" not in last_line:
            run = "./run_Buchoff.sh %i %i"%(cfg, cfg+5)
            print(run)
            #os.system(run)

