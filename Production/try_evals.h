// ---                          EVALS                            --- //
////////////////////////// ****************** /////////////////////////
//! EVALS structure define
struct EvecDBEvals_t
{
  std::vector<Real> evals = std::vector<Real>(Nc*Lx*Lx*Lx);
};
// end define 


//! Reader
void read(BinaryReader& bin, EvecDBEvals_t& param)
{
  read(bin, param.evals);
}


//! Writer
void write(BinaryWriter& bin, const EvecDBEvals_t& param)
{
  write(bin, param.evals);
}


// Concrete evals class
template<typename D>
class EvecDBEvals : public DBData
{
public:
  //! Default constructor
  EvecDBEvals() {} 

  //! Constructor 
  EvecDBEvals(const D& e) : evals_(e) {}

  //! Setter
  D& evals() {return evals_;}

  //! Getter
  const D& evals() const {return evals_;}

  // Part of Serializable
  //virtual unsigned short serialID (void) const {return 123;}
  const unsigned short serialID (void) const {return 123;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, evals());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, evals());
  }

private:
  D evals_;
};
