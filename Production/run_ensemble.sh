#!/bin/bash
export GCC_VERSION="8.3.1"
module load gcc/${GCC_VERSION}

#### Parameters common to all steps ###
# source'd in run scripts Steps 1 - 5
# note all variables CAPS here 
#---------------------------
# physical parameters 
NC=4
LX=32
BETA="12p0"
KAPPA="QUENCHED" 
CONFIG_TYPE="SZINQIO"
MASS_OR_KAPPA="kappa"
NT=$(( 2*${LX} ))
#---------------------------
# file locations
laph_HOME="/p/lustre1/cushman2/laph_build_and_run"
CONFIGS_DIR="/p/lustre1/cushman2/laph_build_and_run/Production/L${LX}T${NT}/beta${BETA}/kappa${KAPPA}/configs"

#ROTATION="original"
#ROTATION="identity"
#ROTATION="identity_peekpoke"
#ROTATION="u1u1u1u3"
#ROTATION="xyz_relabel"
#ROTATION="xyz_cyclic"
#ROTATION="xyz_cyclic_inverse_position"
#ROTATION="xy3piover2"
#ROTATION="xy3piover2_inverse_position"

CONFIG_FILENAME_BASE="${CONFIGS_DIR}/nc4b${BETA}_cfg_" # + ${config_num} + .lime
#CONFIG_FILENAME_BASE="${CONFIGS_DIR}/cfg_"

LX_DIR="${laph_HOME}/Production/L${LX}T${NT}"
BETA_DIR="${LX_DIR}/beta${BETA}"
if [[ mass_or_kappa == "mass" ]]
then
    KAPPA_DIR="${BETA_DIR}/mass${KAPPA}"
else
    KAPPA_DIR="${BETA_DIR}/kappa${KAPPA}"
fi
#---------------------------
# make sure directory path exists for results 
mkdir -p ${LX_DIR}
mkdir -p ${BETA_DIR}
mkdir -p ${KAPPA_DIR}
#mkdir -p "${KAPPA_DIR}/configs"
#mkdir -p "${KAPPA_DIR}/Perams"
#mkdir -p "${KAPPA_DIR}/Perams"
#---------------------------
# runtime variables
QUEUE="pbatch" 
#QUEUE="pdebug" 
GROUP="latticgc"

# parameters common to Step1 and Step2 
#SPLICED_OUTPUT_BASE="${CONFIGS_DIR}/spliced_times_${ROTATION}_${ROTATION}_${ROTATION}" # base_cfg999.mod 
SPLICED_OUTPUT_BASE="${CONFIGS_DIR}/spliced_times" # base_cfg999.mod 
#SPLICED_OUTPUT_BASE="${CONFIGS_DIR}/spliced_times_factor0p0" # base_cfg999.mod 

# write xml function used to generate input xml
find_replace() { sed -i "s|<${1}></${1}>|<${1}>${2}</${1}>|g" ${3}; }








#
