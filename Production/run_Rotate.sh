#!/bin/bash
# get variables from bash script (all CAPS)
set -u
source run_ensemble.sh 
#############################################
# Edit here 
#############################################
# compile or run? 
compile=1
run=1
# parameters of this run 
cfg_start=200
cfg_stop=205
cfg_step=5
time_job=5 
#rotation="identity"
#rotation="identity_peekpoke"
#rotation="u1u1u1u3"
#rotation="xyz_relabel"
#rotation="xyz_cyclic"
rotation="xyz_cyclic_inverse_position"
#rotation="xy3piover2"
#rotation="xy3piover2_inverse_position"

#############################################

# FILE LOCATIONS
Step1_dir="${laph_HOME}/Step1_Splice_times" # location of compile script, main program
exe="${Step1_dir}/rotate_Nc${NC}.exe"

read_lime_xml_base="${CONFIGS_DIR}/read_lime" # filename = base_cfg${cfg}.xml
batch_output_file="${CONFIGS_DIR}/rotate_${rotation}_${cfg_start}_${cfg_stop}.out"
tasks=8 # relatively fast program, don't need parallel  
nodes=1
threads=6 # per task 
geom_x=1
geom_y=1
geom_z=1
geom_t=8


############### IF COMPILE ####################
if [ ${compile} == 1 ]
then 
  sh ${Step1_dir}/compile_rotate.sh ${NC} ${exe} ${Step1_dir} ${laph_HOME}
  wait 
fi 
############### IF RUN ####################
if [ ${run} == 1 ]
then 
    # rm old batch output if exists 
    rm ${batch_output_file}
    # make sure result directories exist, generate read_lime_cfg${cfg}.xml
    for (( i=${cfg_start}; i<${cfg_stop}; i+=${cfg_step} ))
    do
      cfg_filename="${CONFIG_FILENAME_BASE}${i}.lime"
      echo ${cfg_filename}
      read_lime_xml="${read_lime_xml_base}_cfg${i}.xml"
      echo "<ildglat>" > ${read_lime_xml}
      echo "<ILDG_file>${cfg_filename}</ILDG_file>" >> ${read_lime_xml}
      echo "<nrow>${LX} ${LX} ${LX} ${NT}</nrow>" >> ${read_lime_xml}
      echo "</ildglat>" >> ${read_lime_xml}
    done
    
    echo "running rotate from ${cfg_start} to ${cfg_stop}, ${cfg_step}"
    output_lime_base=${CONFIG_FILENAME_BASE}
    ############### RUN SCRIPT HERE ##############################
    export OMP_NUM_THREADS=${threads}
    run="srun -t ${time_job} -p ${QUEUE} -o ${batch_output_file} -n ${tasks} -N ${nodes}"
    geom="-geom ${geom_x} ${geom_y} ${geom_z} ${geom_t}"
    input="${LX} ${NT} ${read_lime_xml_base} ${rotation} ${output_lime_base} ${cfg_start} ${cfg_stop} ${cfg_step}"
    runme="${run} ${exe} ${input} ${geom} < /dev/null"

    echo "Step 2 CONFIG ${cfg_start} to CONFIG ${cfg_stop}"
    echo "RUN   : ${run}";   echo ""
    echo "EXE   : ${exe}";   echo ""
    echo "INPUT : ${input}"; echo ""
    echo "RUNME : ${runme}"; echo ""  
    ${runme} &> /dev/null &  ## runs script here 
    echo ""
    ################ RUN SCRIPT DONE ############################
fi # run conditional 







#
