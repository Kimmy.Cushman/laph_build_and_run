import numpy as np

Nvec_list = np.arange(2,54)
Nt = 32

corrs = np.zeros((len(Nvec_list), Nt))

for n, Nvec in enumerate(Nvec_list):
    with open("L16T32/beta11p5/kappaQUENCHED/Observables/kappa0p1515/mesons/LapH/Nvec%i/g5meson_cfg400.txt"%Nvec, "r") as f:
        for t, line in enumerate(f):
            corrs[n,t] = float(line.split(",")[0])

print(corrs[4, :5])
print(corrs[7, :5])
print(corrs[12, :5])

np.save("corrs_compare_Nvec.npy", corrs)
