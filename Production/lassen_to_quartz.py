import numpy as np
import os 

beta="12p0"

from_dir = "/p/gpfs1/culver5/su4_hmc/4flavor_quenched/L32T64/beta%s/configs"%beta
file_base = "nc4b%s_cfg_"%beta

to_dir = "L32T64/beta%s/kappaQUENCHED/configs/"%beta

for cfg in np.arange(10000, 10450, 50):
    command = "rsync -auv -e ssh lassen:%s/%s%i.lime %s"%(from_dir, file_base, cfg, to_dir)
    print(command)
    os.system(command)
