#!/bin/bash
# get variables from bash script (all CAPS)
set -u
source run_ensemble.sh 
#############################################
# Edit here 
#############################################
# physical parameters 
kappa_observables="1475" 
mass_or_kappa_peram="k" #"1520" # 0.1520/0p1520
smear="point"
# parameters of this run 
cfg_start=$1
cfg_stop=$2
cfg_step=50
time_job=240  # per config 
nodes=1
tasks=32
THREADS=8
GEOM_X=1
GEOM_Y=1
GEOM_Z=4
GEOM_T=8
#############################################
# FILE LOCATIONS
code_dir="${laph_HOME}/Buchoff_code/build/4c_baryons"
exe="${code_dir}/chroma_4c_baryons"

Buchoff_dir="${laph_HOME}/Step4_Observables/Buchoff" # location of compile script, main program

xml_template="${Buchoff_dir}/buchoff_in_point_${smear}_spectrum_SU4.xml"
corr_dir="${KAPPA_DIR}/Observables/kappa0p${kappa_observables}/Buchoff/${smear}"
xml_dir="${corr_dir}/xml"
batch_dir="${corr_dir}/batch_output"
spectrum_dir="${corr_dir}/spectrum/raw_output"
mkdir -p ${corr_dir}
mkdir -p ${xml_dir}
mkdir -p ${batch_dir}
mkdir -p ${spectrum_dir}


for (( i=${cfg_start}; i<${cfg_stop}; i+=${cfg_step} ))
do
  output_dir="${spectrum_dir}/cfg${i}/"
  rm -r ${output_dir}
  mkdir -p ${output_dir}
  cfg_file="${CONFIG_FILENAME_BASE}${i}.lime"
  echo ${cfg_file}
  ############ GENREATE CONFIG XML ################# 
  cfg_xml="${xml_dir}/spectrum_${smear}_cfg${i}.xml"
  cp ${xml_template} ${cfg_xml}

  find_replace "Kappa"         "0.${kappa_observables}"        ${cfg_xml}
  find_replace "LxLyLzNt"      "${LX} ${LX} ${LX} ${NT}" ${cfg_xml}
  find_replace "nrow"          "${LX} ${LX} ${LX} ${NT}" ${cfg_xml}
  find_replace "cfg_file"      ${cfg_file}            ${cfg_xml}
  find_replace "cfg_type"      ${CONFIG_TYPE}            ${cfg_xml}
  find_replace "output_dir"    ${output_dir}           ${cfg_xml}
  ######
  echo ${cfg_xml}
  batch_output_file="${batch_dir}/spectrum_${smear}_cfg${i}.out"
  rm ${batch_output_file}
  # parscalar qdp++ install, MPI doesn't work
  run="srun -t ${time_job} -p ${QUEUE} -o ${batch_output_file} -n ${tasks} -N ${nodes}"
  geom="-geom ${GEOM_X} ${GEOM_Y} ${GEOM_Z} ${GEOM_T}" 
  input="-i ${cfg_xml}"
  outfile="-o ${cfg_xml}_start.out > ${cfg_xml}_start.stdout 2>&1"
  runme="${run} ${exe} ${geom} ${input} ${outfile} < /dev/null"
  echo "Before analysis: ", `date`
  echo "RUN   : ${run} \n"
  echo "EXE   : $exe\n"
  echo "GEOM  : ${geom} \n"
  echo "INPUT : $input\n"
  echo "RUNME : $runme\n"  
  ${runme} &> /dev/null & ## runs script here 
  echo "After analysis: ", `date`

done

