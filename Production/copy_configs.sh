#!/bin/bash
set -u

beta="12p0"
Lx=32
Nt=64

FROM_DIR="/p/gpfs1/culver5/su4_hmc/4flavor_quenched/L${Lx}T${Nt}/beta${beta}/configs"
FILE_BASE="nc4b${beta}_cfg_"

TO_DIR="/p/lustre1/cushman2/laph_build_and_run/Production/L${Lx}T${Nt}/beta${beta}/kappaQUENCHED/configs/"


for(( i=1400; i<6000; i+=50 ))
do
  FILE_SEND="${FILE_BASE}${i}.lime"
  rsync -auv -e ssh lassen:${FROM_DIR}/${FILE_SEND} ${TO_DIR} 
  wait
  echo "${i} done"
done
