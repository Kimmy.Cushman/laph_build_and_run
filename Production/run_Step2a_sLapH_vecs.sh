#!/bin/bash
export GCC_VERSION="8.3.1"
module load gcc/${GCC_VERSION}
# get variables from bash script (all CAPS)
source run_ensemble.sh 
#############################################
# Edit here 
#############################################
# compile or run? 
compile=1
run=0
# parameters of this run 
Nvec_file=200
Nvec=16
Nnoise=8
num_times_per_job=16
time_job=20 #L32 about 55 min per config per time
cfg_start=100
cfg_stop=150
cfg_step=50
t_start=0
t_stop=${NT}

#############################################
# FILE LOCATIONS
Step2_dir="${laph_HOME}/Step2_Distill_vecs" # location of compile script, main program
exe="${Step2_dir}/sLapH_vecs_Lx${LX}_Nc${NC}_Nvecfile${Nvec_file}.exe"

sLapH_Nvec_dir="${KAPPA_DIR}/sLapH_vecs/Nvec${Nvec}"
mkdir -p ${sLapH_Nvec_dir}
sLapH_Nnoise_dir="${KAPPA_DIR}/sLapH_vecs/Nvec${Nvec}/Nnoise${Nnoise}"
mkdir -p ${sLapH_Nnoise_dir}

tasks=1 # couldn't get to parallelize further  
nodes=1
threads=32 # per task
geom_x=1 # can't get nontrivial to work, 
geom_y=1
geom_z=1 # Nd=3, no geom_t 


############### IF COMPILE ####################
if [ ${compile} == 1 ]
then 
  sh ${Step2_dir}/compile_main_sLapH.sh ${NC} ${LX} ${exe} ${Step2_dir} ${laph_HOME}
  wait 
fi # compile conditional 


############### IF RUN ####################
if [ ${run} == 1 ]
then 
  echo "run"
  for (( i=${cfg_start}; i<${cfg_stop}; i+=${cfg_step} ))
  do 
    laph_dir="${KAPPA_DIR}/LapH_vecs/Nvec${Nvec}/cfg_${i}"
    slaph_dir="${sLapH_Nnoise_dir}/cfg_${i}"
    mkdir -p ${slaph_dir}

    laph_vecs_file_t="${laph_dir}/LapH_${Nvec}vecs_t"
    slaph_vecs_file_t="${slaph_dir}/sLapH_${Nvec}vecs_Nnoise${Nnoise}_t"
    # make sure LapH files are there 
    run_sLapH=1
    for (( t=0; t<${t_stop}; t++))
    do 
      mod="${laph_vecs_file_t}${t}.mod"
      db="${laph_vecs_file_t}${t}_evecs.db"
      if [[ ! -f ${mod} || $(stat -c %s ${mod} ) == 0 ||
            ! -f ${db}  || $(stat -c %s ${db}  ) == 0 ]]
      then
        echo "vec cfg ${i}, t=${t} not done "
        run_sLapH=0
      fi
    done # t loop
  if [ ${run_sLapH} == 1 ]
  then 
    # loop over t_start for each job
    for (( t_start_job=${t_start}; t_start_job<${t_stop}; t_start_job+=${num_times_per_job} ))
    do 
      # check if vecs for these timeslices already exist 
      t_range_done=1 # CHANGE BACK TO 1                                                 # FIXME
      t_stop_job=$(( t_start_job+ num_times_per_job ))
      batch_output_file="${slaph_dir}/vectors_Nvec${Nvec}_tstart${t_start_job}_tstop${t_stop_job}.out"
      rm ${batch_output_file} 2> /dev/null
      ############### RUN SCRIPT HERE ##############################
      export OMP_NUM_THREADS=${threads}
      run="srun -t ${time_job} -p ${QUEUE} -o ${batch_output_file} -n ${tasks} -N ${nodes}"
      geom="-geom ${geom_x} ${geom_y} ${geom_z}"
      input="${laph_vecs_file_t} ${slaph_vecs_file_t} ${NT} ${Nvec_file} ${Nvec} ${Nnoise} ${t_start_job} ${t_stop_job}"
      runme=" ${run} ${exe} ${input} ${geom} < /dev/null"
      #echo "Step 2 CONFIG ${i} tstart:${t_start_job}, tstop:${t_stop_job}"
      #echo "RUN   : ${run}"   
      #echo "EXE   : ${exe}"  
      #echo "INPUT : ${input}" 
      echo "RUNME : ${runme}"
      ${runme} &> /dev/null & ## runs script here 
      echo "" 
    done # t start loop
    fi # if vecs files exist 
  done # config loop
fi # run conditional 


