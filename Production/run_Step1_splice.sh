#!/bin/bash
# get variables from bash script (all CAPS)
set -u
source run_ensemble.sh 
#############################################
# Edit here 
#############################################
# compile or run? 
compile=0
run=1
# parameters of this run 
cfg_start=$1 # this range specifies one single job
cfg_stop=$2 #210
cfg_step=50
time_job=$3 #L32 about 1 min per config * number of configs
#############################################

# FILE LOCATIONS
Step1_dir="${laph_HOME}/Step1_Splice_times" # location of compile script, main program
exe="${Step1_dir}/splice_times_Nc${NC}.exe"

read_lime_xml_base="${CONFIGS_DIR}/read_lime" # filename = base_cfg${cfg}.xml

batch_output_file="${CONFIGS_DIR}/splice_${cfg_start}_${cfg_stop}.out"
tasks=4 # relatively fast program, don't need parallel  
nodes=1
threads=6 # per task 
geom_x=1
geom_y=1
geom_z=1
geom_t=4 #8 changed for Lx=6


############### IF COMPILE ####################
if [ ${compile} == 1 ]
then 
  sh ${Step1_dir}/compile_main.sh ${NC} ${exe} ${Step1_dir} ${laph_HOME}
  wait 
fi 


############### IF RUN ####################
if [ ${run} == 1 ]
then 

  # check if this range already done 
  splice_done=1
  for (( i=${cfg_start}; i<${cfg_stop}; i+=${cfg_step} ))
  do
    spliced_mod="${SPLICED_OUTPUT_BASE}_cfg${i}.mod"
    if [ ! -f ${spliced_mod} ]
    then
      splice_done=0
    else
      echo "cfg ${i} already spliced"
    fi
  done
  
  # if splice done
  if [[ ${splice_done} == 1 ]]
  then
    echo "splice already done from cfg ${cfg_start} to cfg${cfg_stop}"
  fi 

  # if splice not done, run it now!   
  if [[ ${splice_done} == 0 ]] 
  then 
    # rm old batch output if exists 
    rm ${batch_output_file}
    # make sure result directories exist, generate read_lime_cfg${cfg}.xml
    for (( i=${cfg_start}; i<${cfg_stop}; i+=${cfg_step} ))
    do
      cfg_filename="${CONFIG_FILENAME_BASE}${i}.lime"
      read_lime_xml="${read_lime_xml_base}_cfg${i}.xml"
      echo "<ildglat>" > ${read_lime_xml}
      echo "<ILDG_file>${cfg_filename}</ILDG_file>" >> ${read_lime_xml}
      echo "<nrow>${LX} ${LX} ${LX} ${NT}</nrow>" >> ${read_lime_xml}
      echo "</ildglat>" >> ${read_lime_xml}
    done
    
    echo "running splice from ${cfg_start} to ${cfg_stop}, ${cfg_step}"
    
    ############### RUN SCRIPT HERE ##############################
    export OMP_NUM_THREADS=${threads}
    run="srun -t ${time_job} -p ${QUEUE} -o ${batch_output_file} -n ${tasks} -N ${nodes}"
    geom="-geom ${geom_x} ${geom_y} ${geom_z} ${geom_t}"
    input="${LX} ${NT} ${read_lime_xml_base} ${SPLICED_OUTPUT_BASE} ${cfg_start} ${cfg_stop} ${cfg_step}"
    runme="${run} ${exe} ${input} ${geom} < /dev/null"

    echo "Step 2 CONFIG ${cfg_start} to CONFIG ${cfg_stop}"
    echo "RUN   : ${run}";   echo ""
    echo "EXE   : ${exe}";   echo ""
    echo "INPUT : ${input}"; echo ""
    echo "RUNME : ${runme}"; echo ""  
    ${runme} &> /dev/null &  ## runs script here 
    echo ""
    ################ RUN SCRIPT DONE ############################
  fi
fi # run conditional 







#
