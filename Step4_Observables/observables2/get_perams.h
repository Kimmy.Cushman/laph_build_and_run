#ifndef GET_PERAMS_H
#define GET_PERAMS_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
using namespace Eigen;
using namespace std; 
using std::vector;
using std::ifstream;
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;


// get the index of phi from alpha, beta, t
int tau_ind(int t_sink, int t_source, int alpha, int beta){
    return t_sink*Nt*Nd*Nd + t_source*Nd*Nd + alpha*Nd  + beta;
}


struct tau_struct{
    string tau_file;
    // for a given t, alpha, beta, tau is a Nvec x Nvec matrix of complex doubles
    Matrix<complex<double>, Dynamic, Dynamic, RowMajor> tau_matrix[Nt*Nt*Nd*Nd];
};



void get_tau(string filename, tau_struct &tau){
    for(int i=0; i!=Nt*Nt*Nd*Nd; i++){  // loop over t and Dirac indices
        tau.tau_matrix[i].resize(Nvec,Nvec);
    }
    BinaryStoreDB<SerialDBKey<KeyPropElementalOperator_t>, SerialDBData<ValPropElementalOperator_t>> qdp_db;
    qdp_db.open(filename, O_RDONLY, 0600); // 0600  = identifier for permissions on file 
    std::vector< SerialDBKey<KeyPropElementalOperator_t> > keys;
    qdp_db.keys(keys);
    for(int i=0; i<keys.size(); i++){ // start loop over t and Dirac indices -- only as many timeslices as there are
      SerialDBData<ValPropElementalOperator_t> tmp;
      auto id = qdp_db.get(keys[i], tmp);
      int tsource = keys[i].key().t_source;
      int tsink = keys[i].key().t_slice;
      int alpha = keys[i].key().spin_src;
      int beta = keys[i].key().spin_snk;
      int index = tau_ind(tsource, tsink, alpha, beta);
      std::cout << "getting perambulator at"
                <<" t_source = " << tsource 
                << ", tsink = " << tsink 
                << ", alpha = " << alpha 
                << ", beta = " << beta << std::endl;
    
      for(int j=0; j<Nvec; j++){
        for(int k=0; k<Nvec; k++){
          double value_real = tmp.data().mat(j,k).elem().elem().elem().real(); // scalar indices can be popped
          double value_imag = tmp.data().mat(j,k).elem().elem().elem().imag(); // out of the lattice with elems
          tau.tau_matrix[index](j,k) = complex<double>(value_real, value_imag);
        }
      }
    
    } // end loop over t and Dirac indices
}

#endif
