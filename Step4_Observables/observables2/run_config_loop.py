import numpy as np
import os
import sys

Nc = int(sys.argv[1])
Lx = int(sys.argv[2])
Nt = int(sys.argv[3])
Nvec = int(sys.argv[4])
Nsink = int(sys.argv[5])

os.system("./compile_main.sh %i %i %i %i"%(Nc, Lx, Nt, Nvec))

for config in np.arange(2000, 2010, 5):
    direc = "../../Distillation/Splice_times/cfg_%i/"%config
    peram = direc + "perambulators_nvec%i_Nt%i.sdb"%(Nvec, Nsink)
    evec_base = direc + "Nc4_24vecs_"
    #os.system("bsub run_main.sh %i %i %i %i %i"%(Nc, Lx, Nt, Nvec, Nsink))
    os.system("./main_Nc%i.exe %i %i %i %i %i"%(Nc, Nc, Lx, Nt, Nvec, Nsink))
os.system("rm global_variables.h")


# Explicit run example
# python run_config_loop.py 4 16 32 24 32
