#!/bin/bash -l
#BSUB -W 1
#BSUB -P latticgc 
#BSUB -q pbatch

## sources gnu compiler variables (don't change this)
export GCC_VERSION="7.3.1"
module load gcc/7.3.1

export LD_LIBRARY_PATH=/p/gpfs1/cushman2/code/base_install/arpackpp/external/arpack-ng-build/lib64/libarpack.so:${LD_LIBRARY_PATH}


export OMP_NUM_THREADS=1
run="./"
input="$5 $6 $7"
# perambulator file, evec base name 
runme="${run}${exe} ${input} < /dev/null"

echo "Before analysis: ", `date`
echo "RUN   : ${run} \n"
echo "EXE   : $exe\n"
echo "INPUT : $input\n"
echo "RUNME : $runme\n"  
time ${runme}  ## runs script here 
echo "After analysis: ", `date`


# 1 = Nc
# 2 = Lx
# 3 = Nt
# 4 = Nvec
# 5 = perambulator file
# 6 = evec name base 
# 7 = t_stop (assumes evecs for t = 0,1,2,...,t_stop-1

# Explicit job submission example
# bsub run_main.sh <Nc> <Lx> <Nt> <Nvec> <perambulator file> <evec name base> <t_stop>
# bsub run_main.sh 4 16 32 16 Perambulator_files/Nc4_tstop16_Nvec16.sdb Evec_files/Nc4_nvecs16_ 16

