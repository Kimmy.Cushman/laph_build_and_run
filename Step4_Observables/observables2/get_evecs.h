#ifndef GET_EVECS_H
#define GET_EVECS_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
using namespace Eigen;
using namespace std; 
using std::vector;
using std::ifstream;
#include "global_variables.h"
using namespace QDP;
using namespace FILEDB;
using namespace Chroma;


//---------------------------------------------------------------------
//! Some struct to use
struct EvecDBKey_t
{
  int t_slice;
  int colorvec;
};

//! Reader
void read(BinaryReader& bin, EvecDBKey_t& param)
{
  read(bin, param.t_slice);
  read(bin, param.colorvec);
}

//! Writer
void write(BinaryWriter& bin, const EvecDBKey_t& param)
{
  write(bin, param.t_slice);
  write(bin, param.colorvec);
}


//---------------------------------------------------------------------
//! Some struct to use
struct EvecDBData_t
{
  std::vector<std::complex<double>> data = std::vector<std::complex<double>>(Nc*Lx*Lx*Lx); 
};

//! Reader
void read(BinaryReader& bin, EvecDBData_t& param)
{
  read(bin, param.data);
}

//! Writer
void write(BinaryWriter& bin, const EvecDBData_t& param)
{
  write(bin, param.data);
}


//---------------------------------------------------------------------
// Simple concrete key class
template<typename K>
class EvecDBKey : public DBKey // DBKey defined in FILEDB
{
public:
  //! Default constructor
  EvecDBKey() {} 

  //! Constructor from data
  EvecDBKey(const K& k) : key_(k) {}

  //! Setter
  K& key() {return key_;}

  //! Getter
  const K& key() const {return key_;}

  // Part of Serializable
  const unsigned short serialID (void) const {return 456;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, key());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, key());
  }

  // Part of DBKey
  int hasHashFunc (void) const {return 0;}
  int hasCompareFunc (void) const {return 0;}

  /**
    * Empty hash and compare functions. We are using default functions.
    */
  static unsigned int hash (const void* bytes, unsigned int len) {return 0;}
  static int compare (const FFDB_DBT* k1, const FFDB_DBT* k2) {return 0;}
  
private:
  K  key_;
};


//---------------------------------------------------------------------
// Simple concrete data class
template<typename D>
class EvecDBData : public DBData // DBData defined in FILEDB
{
public:
  //! Default constructor
  EvecDBData() {} 

  //! Constructor from data
  EvecDBData(const D& d) : data_(d) {}

  //! Setter
  D& data() {return data_;}

  //! Getter
  const D& data() const {return data_;}

  // Part of Serializable
  const unsigned short serialID (void) const {return 123;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, data());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, data());
  }

private:
  D  data_;
};

//****************************************************************************
//! Prop operator
struct KeyTimeSliceColorVec_t
{
  KeyTimeSliceColorVec_t() {}
  KeyTimeSliceColorVec_t(int t_slice_, int colorvec_) : t_slice(t_slice_), colorvec(colorvec_) {}

  int        t_slice;       /*!< Source time slice */
  int        colorvec;      /*!< Colorstd::vector index */
};


//----------------------------------------------------------------------------
// KeyPropColorVec read
void read(BinaryReader& bin, KeyTimeSliceColorVec_t& param)
{
  read(bin, param.t_slice);
  read(bin, param.colorvec);
}

// KeyPropColorVec write
void write(BinaryWriter& bin, const KeyTimeSliceColorVec_t& param)
{
  write(bin, param.t_slice);
  write(bin, param.colorvec);
}



int v_ind(int a, int x){
    // color * space + position 
    return a*Lx*Lx*Lx + x;
}

struct v_dist_struct{
    //string vectors_file;
    // define the array of distillation vectors ( NOTE these come from a different code, so here initialize to random)
    //static const size_t size_v = Nt*Nvec*Nc; 
    // note the v_dist is an array of Nvec eigen vectors of the Laplacian 
    // each vector of length Lx^3 in the Eigen class, 
    // for each value of t and color index a (Nt*Nc), a vector is specified 
    //Matrix<complex<double>,Lx^3,1> v_dist[Nvec*Nc];            
    Matrix<complex<double>,Dynamic,1> v_dist[Nvec];            

};


// define phi structure 
struct phi_struct{
    
    // for a given alpha, beta, t, phi is a Nvec x Nvec matrix of complex doubles 
    // phi_matrix is an array of Nvec x Nvec matrices 
    //Matrix<complex<double>, Nvec,Nvec, RowMajor> Phi_matrix;
    Matrix<complex<double>, Dynamic, Dynamic, RowMajor> Phi_matrix;
};


void get_evecs(vector<string> &filenames, vector<v_dist_struct> &evectors, vector<int> &t_all_list){ 
    for(auto t: t_all_list){// t!=Nt_all; t++){
        v_dist_struct evector;
        for(int i=0; i!=Nvec; i++){
            evector.v_dist[i].resize(Lx*Lx*Lx*Nc,1);
        }
        evectors.push_back(evector);
    }
    
    // loop over t
    for(auto t: t_all_list){// t!=Nt_all; t++){
      typedef BinaryStoreDB< EvecDBKey<EvecDBKey_t>, EvecDBData<EvecDBData_t> > DBType_t_read;
      DBType_t_read db_read;
      const std::string dbase_read = filenames[t];
      std::cout << " F I L E N A M E" << std::endl;
      std::cout << dbase_read << std::endl;
      db_read.open(dbase_read, O_RDONLY, 0600);

      // loop over distill vecs, find key value (t, distill_vec)
      for(int vec_num=0; vec_num< Nvec; ++vec_num){
        std::cout << "getting vector at t = " << t << ", vec num i = " << vec_num << std::endl;
        // ************ get values from .db file ************ //
        EvecDBData<EvecDBData_t> testDBData_read;
        //testDBData_read.data().data.at(7) = std::complex<double>(1.2, 3.4);
        
        EvecDBKey<EvecDBKey_t> testDBKey_read;
         
        testDBKey_read.key().t_slice = t;       // key to read 
        testDBKey_read.key().colorvec = vec_num;// key to read
      
        // get value from key, store in testDBData_read 
        db_read.get(testDBKey_read, testDBData_read);
        
        //************** fill CPP array *************//
        // loop over spatial points
        for(int site=0; site < Lx*Lx*Lx; site++){
          // loop over color 
          for(int color=0; color < Nc; color++){
            std::complex<double> tmp = testDBData_read.data().data.at(site*Nc + color);
            evectors[t].v_dist[vec_num](v_ind(color,site)) = tmp;
          } // end loop over color 
        } // end loop over sites
      } // end loop over Dist evecs 
    } // end loop over timeslices 

}

#endif
