#ifndef MULT_PION_H
#define MULT_PION_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
using namespace Eigen;
using namespace std; 
using std::vector;
using std::ifstream;
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;


// define properties of a gamma matrix
struct gamma_struct{
  string name;    
  Matrix<int, Nd,2>  nonzero_Dirac; // ex. {{0,0}, {1,1}, {2,2}, {3,3}} for identity
  Matrix<complex<double>, Nd, Nd, RowMajor> gamma_matrix;
};

void gamma_map(gamma_struct &gamma){
  if(gamma.name == "I"){
    complex<double>c1(1.0,0.0);
    complex<double>c0(0.0,0.0);
    gamma.gamma_matrix << c1, c0, c0, c0,
                          c0, c1, c0, c0,
                          c0, c0, c1, c0, 
                          c0, c0, c0, c1;
    gamma.nonzero_Dirac << 0,0, 1,1, 2,2, 3,3;

  if(gamma.name == "5"){
    complex<double>c1(1.0,0.0);
    complex<double>c2(-1.0,0.0);
    complex<double>c0(0.0,0.0);
    gamma.gamma_matrix << c1, c0, c0, c0,
                          c0, c1, c0, c0,
                          c0, c0, c2, c0, 
                          c0, c0, c0, c2;
    gamma.nonzero_Dirac << 0,0, 1,1, 2,2, 3,3;



void get_phi(int t, gamma_struct gamma, v_dist_struct &vectors, phi_struct &phi, int Nvec){
 phi_struct Phi;
 for(int i=0; i!=Nvec; i++){
  for(int j=0; j!=Nvec; j++){
    // get i,j element of Phi_matrix and set to 0
    complex<double> &phi_element = phi.Phi_matrix(i,j);
    phi_element = complex<double>(0.0,0.0);
    //Eigen matrix multiplication over spatial indices 
    complex<double> prod = vectors.v_dist[i].adjoint()  * vectors.v_dist[j];
    phi_element += prod;                                                
    } // end j loop
  } // end i loop 
}



vector<complex<double>> corr(Nsink, complex<double>(0.0, 0.0));

void pion(vector<comple<double>> &corr, v_dist_struct &vectors, tau_struct &tau, int Nsink, int Nvec){
  gamma_struct gamma5;
  gamma5.name = "5";
  
  // get phi from v and gamma5 at source
  phi_struct phi_source;
  get_phi(0, gamma5, vectors, phi_source, Nvec);
  
  for(int tsink=0; tsink < Nsink; tsink++){
    // get phi from v and gamma5 at this time 
    phi_struct phi;
    get_phi(t, gamma5, vectors, phi, Nvec);
    for(int alpha = 0; alpha < 4; alpha++){
      for(int beta = 0; beta < 4; beta++){
        if(gamma5.gamma_matrix(alpha, beta) != complex<double>(0.0, 0.0)){
          for(int delta = 0; delta < 4; delta++){
            for(int sigma = 0; sigma < 4; sigma++){
              if(gamma5.gamma_matrix(delta, sigma) != complex<double>(0.0, 0.0)){
                for(int i=0; i<Nvec; i++){
                  for(int j=0; j<Nvec; j++){
                    for(int k=0; k<Nvec; k++){
                      for(int l=0; l<Nvec; l++){
                          corr[t] += phi.Phi_matrix(i,j) 
                                      * gamma5.gamma_matrix(alpha,beta) 
                                      * tau.tau_matrix[tau_index(0, tsink, beta, delta)](j,k)
                                      * phi.Phi_matrix(k,l) 
                                      * gamma5.gamma_matrix(delta, sigma)
                                      * tau.tau_matrix[tau_index(tsink, 0, delta, sigma)](l,i) 
                      } // end l loop
                    } // end k loop
                  } // end j loop
                } // end i loop 
              } // end if delta sigma != 0
            } // end sigma loop
          } // end delta loop 
        } // end if alpha beta != 0
      } // end beta loop
    } //  end alpha loop
  } // end tsink loop
}









#endif
