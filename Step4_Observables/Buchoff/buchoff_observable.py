import numpy as np
import os
import sys 

MACHINE="QUARTZ"

if MACHINE == "LASSEN":
    home = "/p/gpfs1/cushman2/laph_build_and_run"
if MACHINE == "QUARTZ":
    home = "/p/lustre1/cushman2/laph_build_and_run"
code_dir = "%s/code"%home
Step4_Observable_dir = "%s/Step4_Observables"%home


# physical params 
Nc = 4
Lx = int(sys.argv[1])
time_job = int(sys.argv[2])
queue = "pbatch"
Nt = Lx*2
beta = sys.argv[3]
kappa_num = sys.argv[4]
kappa_cfg = sys.argv[5]
i = int(sys.argv[6]) # config num

config_type = "SZINQIO"
xml_template = "%s/Buchoff/buchoff_in_point_point_baryon_SU4.xml"%Step4_Observable_dir

# filename 
Lx_dir = "%s/Production/L%iT%i"%(home, Lx, Nt)
beta_dir = "%s/beta%s"%(Lx_dir, beta)
if kappa_cfg == "WEAK" or kappa_cfg == "QUENCHED":
    kappa_dir = "%s/kappa%s"%(beta_dir, kappa_cfg) ## FIX ME
else:
    kappa_dir = "%s/kappa0p%s"%(beta_dir, kappa_cfg) ## FIX ME
config_dir = "%s/cfg_%i"%(kappa_dir,i)


if not os.path.isdir(Lx_dir):
    os.system("mkdir %s"%Lx_dir)
if not os.path.isdir(beta_dir):
    os.system("mkdir %s"%beta_dir)
if not os.path.isdir(kappa_dir):
    os.system("mkdir %s"%kappa_dir)
if not os.path.isdir(config_dir):
    os.system("mkdir %s"%config_dir)
print(config_dir)


baryon_xml = "%s/baryon_kappa0p%s_point_point_baryon.xml"%(config_dir, kappa_num)
#config = "%s/configs/nc4b%skappa0p%s_cfg_%i.lime"%(kappa_dir, beta, kappa_num, i)
if beta == "WEAK":
    config = "%s/configs/cfg_%i.lime"%(kappa_dir, i)
else:
    config = "%s/configs/nc4b%s_cfg_%i.lime"%(kappa_dir, beta, i)


print("writing %s"%baryon_xml)
xml_template_file = open(xml_template, "r")
xml_config_file = open(baryon_xml, "w")

lines = base_file.readlines()
for line in lines:
    
    # output 
    if "<output_dir></output_dir>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config_dir + "/<" + post) # need "/" !
    
    # Latt size 
    elif "<nrow></nrow>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
    elif "<LxLyLzNt></LxLyLzNt>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + "%i %i %i %i"%(Lx,Lx,Lx,Nt) + "<" + post) 
    
    # kappa   
    elif "<Kappa></Kappa>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + "0." + kappa_num + "<" + post) 
    
    # type     
    elif "<cfg_type></cfg_type>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config_type + "<" + post) 
    
    elif "<cfg_file></cfg_file>" in line:
        pre, post = line.split("><")
        xml_config_file.write(pre + ">" + config + "<" + post) 

    else:
        xml_config_file.write(line)

base_file.close()
xml_config_file.close()


# run the xml to get the config
batch_output_file = "%s/baryon_point_point.out"%config_dir
os.system("rm %s"%batch_output_file)
run = "%s/run_buchoff.sh"%Step4_Observable_dir
args = "%s %s %i %s"%(baryon_xml, batch_output_file, time_job, queue)
print("%s %s"%(run, args))
os.system("%s %s"%(run, args))





