#ifndef GET_EVECS_H
#define GET_EVECS_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
#include "../Step2_Distill_vecs/Data_wrangling/read_write.h"   // needed for DBKey and DBVal 
using namespace Eigen;
using namespace std; 
using std::vector;
using std::ifstream;
#include "global_variables.h"
using namespace QDP;
using namespace FILEDB;
using namespace Chroma;
/*
//---------------------------------------------------------------------
//! Some struct to use
struct EvecDBKey_t
{
  int t_slice;
  int colorvec;
};

//! Reader
void read(BinaryReader& bin, EvecDBKey_t& param)
{
  read(bin, param.t_slice);
  read(bin, param.colorvec);
}

//! Writer
void write(BinaryWriter& bin, const EvecDBKey_t& param)
{
  write(bin, param.t_slice);
  write(bin, param.colorvec);
}


//---------------------------------------------------------------------
//! Some struct to use
struct EvecDBData_t
{
  std::vector<std::complex<double>> data = std::vector<std::complex<double>>(Nc*Lx*Lx*Lx); 
};

//! Reader
void read(BinaryReader& bin, EvecDBData_t& param)
{
  read(bin, param.data);
}

//! Writer
void write(BinaryWriter& bin, const EvecDBData_t& param)
{
  write(bin, param.data);
}


//---------------------------------------------------------------------
// Simple concrete key class
template<typename K>
class EvecDBKey : public DBKey // DBKey defined in FILEDB
{
public:
  //! Default constructor
  EvecDBKey() {} 

  //! Constructor from data
  EvecDBKey(const K& k) : key_(k) {}

  //! Setter
  K& key() {return key_;}

  //! Getter
  const K& key() const {return key_;}

  // Part of Serializable
  const unsigned short serialID (void) const {return 456;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, key());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, key());
  }

  // Part of DBKey
  int hasHashFunc (void) const {return 0;}
  int hasCompareFunc (void) const {return 0;}

  //
    * Empty hash and compare functions. We are using default functions.
    //
  static unsigned int hash (const void* bytes, unsigned int len) {return 0;}
  static int compare (const FFDB_DBT* k1, const FFDB_DBT* k2) {return 0;}
  
private:
  K  key_;
};


//---------------------------------------------------------------------
// Simple concrete data class
template<typename D>
class EvecDBData : public DBData // DBData defined in FILEDB
{
public:
  //! Default constructor
  EvecDBData() {} 

  //! Constructor from data
  EvecDBData(const D& d) : data_(d) {}

  //! Setter
  D& data() {return data_;}

  //! Getter
  const D& data() const {return data_;}

  // Part of Serializable
  const unsigned short serialID (void) const {return 123;}

  void writeObject (std::string& output) const throw (SerializeException) {
    BinaryBufferWriter bin;
    write(bin, data());
    output = bin.str();
  }

  void readObject (const std::string& input) throw (SerializeException) {
    BinaryBufferReader bin(input);
    read(bin, data());
  }

private:
  D  data_;
};

// ****************************************************************************
//! Prop operator
struct KeyTimeSliceColorVec_t
{
  KeyTimeSliceColorVec_t() {}
  KeyTimeSliceColorVec_t(int t_slice_, int colorvec_) : t_slice(t_slice_), colorvec(colorvec_) {}

  int        t_slice;       // *!< Source time slice * //
  int        colorvec;      // *!< Colorstd::vector index * //
};


//----------------------------------------------------------------------------
// KeyPropColorVec read
void read(BinaryReader& bin, KeyTimeSliceColorVec_t& param)
{
  read(bin, param.t_slice);
  read(bin, param.colorvec);
}

// KeyPropColorVec write
void write(BinaryWriter& bin, const KeyTimeSliceColorVec_t& param)
{
  write(bin, param.t_slice);
  write(bin, param.colorvec);
}
*/


int v_ind(int color, int site){
    // color * space + position 
    return color*Lx*Lx*Lx + site;
}

void get_evecs(vector<string> &vec_filenames, vector<string> &val_filenames, vector<MatrixXcd> &evectors, MatrixXd &evalues){ 
    // evectors[t](i, x)
    // evalues(t,i)
    for(int t=0; t<Nt; t++){
      evectors.push_back(MatrixXcd::Zero(Nvec, Lx*Lx*Lx*Nc));
    }
    
    
    EvecDBKey<EvecDBKey_t> Key_read;
    // loop over t
    for(int t=0; t<Nt; t++){
      Key_read.key().t_slice = t;       
      
      // ---- READ EVECS  ---- //
      BinaryStoreDB< EvecDBKey<EvecDBKey_t>, EvecDBData<EvecDBData_t> > db_read_vecs;
      db_read_vecs.open(vec_filenames[t], O_RDONLY, 0664);
      for(int vec_num=0; vec_num< Nvec; ++vec_num){
        // KEY
        Key_read.key().colorvec = vec_num;
        // VALUE
        EvecDBData<EvecDBData_t> evector_read;
        // READ 
        db_read_vecs.get(Key_read, evector_read);
        
        // ************** fill CPP array ************* //
        // loop over spatial points
        for(int site=0; site < Lx*Lx*Lx; site++){
          // loop over color 
          for(int color=0; color < Nc; color++){
            std::complex<double> tmp = evector_read.data().evec_data.at(site*Nc + color);
            if(color==5){ // always false
              printf("tmp should be real part of complex double\n");
              printf(".8e    %.8e \n", tmp.real());
              printf(".12e  %.12e \n", tmp.real());
              float my_float = 1.2345670;
              printf("my_float should be float, has 11 digits originally\n");
              printf(".8e    %.8e \n", my_float);
              printf(".12e  %.12e \n", my_float);
              double my_double = 1.234567890123456;
              printf("my_double should be double, has 16 digits originally\n");
              printf(".8e    %.8e \n", my_double);
              printf(".12e  %.12e \n", my_double);
            }
            evectors[t](vec_num, v_ind(color, site)) = tmp;
            //std::cout << evectors[t](vec_num, v_ind(color, site)) << std::endl;
          } // end loop over color 
        } // end loop over sites
      } // end loop over Dist evecs 


      // ---- READ EVALS  ---- //
      /*
      BinaryStoreDB< EvecDBKey<EvecDBKey_t>, EvecDBData<EvecDBEval_t> > db_read_vals;
      db_read_vals.open(val_filenames[t], O_RDONLY, 0664);
      for(int vec_num=0; vec_num< Nvec; ++vec_num){
        // KEY
        Key_read.key().colorvec = vec_num;
        // VALUE
        EvecDBData<EvecDBEval_t> evalue_read;
        // READ 
        db_read_vals.get(Key_read, evalue_read);
        // ************** fill CPP array ************* //
        Real tmp_value = evalue_read.data().eval;
        evalues(t, vec_num) = QDP::toFloat(tmp_value);
      } // end loop over Dist evecs
      */
     // end loop over timeslices 
  }
}


#endif
