#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <unsupported/Eigen/CXX11/Tensor>
#include <Eigen/Dense>
//#include </p/gpfs1/cushman2/code/source/eigen/unsupported/Eigen/CXX11/Tensor>

// from -I${QDP}
#include "qdp.h" 
#include "qdp_map_obj.h"  
#include "qdp_map_obj_disk.h"
#include "qdp_db.h"
#include "qdp_map_obj_disk_multiple.h" // from -I QDP (maybe not needed now, need if perambs stored in multiple files) 
#include "qdp_iogauge.h" // prob dont need, only need if want derivative in phi matrices 
// local
#include "read_spin_wick.h"
#include "db_codes/key_prop_matelem.h"// local file
#include "global_variables.h" // written and updated by config_loop.py
#include "get_evecs.h"
#include "get_perams.h"
#include "mult_pion.h"
#include "spin_matrices.h"
#include "mult_baryon_udud.h"
using namespace QDP; 
using namespace FILEDB;
using namespace Chroma;
using namespace std;
using cd = complex<double>;


int main(int argc, char *argv[]){
 
    START_CODE();
    // Put the machine into a known state
    QDP_initialize(&argc, &argv);
   
    std::cout << "   S T A R T   C O D E   " << std::endl;
    std::string perambulator_filename_base = argv[1]; // includes specification of t_source
    std::string vecs_dir = argv[2];
    std::string vec_names_base_filename = argv[3];
    int t_stop = Nt;
    string spin_matrix_filename = argv[4];
    string wick_filename = argv[5];
    string corr_savename_dir = argv[6];
    string state = argv[7];
    int cfg_start = atoi(argv[8]);
    int cfg_stop = atoi(argv[9]);
    int cfg_step = atoi(argv[10]);
    // assume all t_sink are calculated, 
    // eg. Nt_backward = t_source, Nt_forward = Nt - t_source
    // see get_perams.h for indexing 
    int t_source = atoi(argv[11]);
    
    for(int cfg=cfg_start; cfg<cfg_stop; cfg+=cfg_step){
      std::string cfg_num=std::to_string(cfg);
      std::string perambulator_filename = perambulator_filename_base + "_cfg" + std::to_string(cfg) + ".sdb";
      std::string vec_names_base = vecs_dir + "/" + "cfg_" + std::to_string(cfg) + "/" + vec_names_base_filename;

      std::cout << "STATE: " << state << std::endl;
      std::cout << "corr_savename_dir " << corr_savename_dir << std::endl;
      std::cout << "vec_names_base " << vec_names_base << std::endl;
      
      
      // Both perams and evecs loading assumes all Nt have been calculated 
      
      
      // get eigen vectors filenames 
      vector<string> vec_names(Nt);
      vector<string> val_names(Nt);
      for(int t=0; t<Nt; t++){
        std::ostringstream oss_vecs;
        std::ostringstream oss_vals;
        oss_vecs <<  vec_names_base << t << "_evecs.db";
        //oss_vecs <<  vec_names_base << t << ".db";
        oss_vals <<  vec_names_base << t << "_evals.db";
        vec_names[t] = oss_vecs.str();
        val_names[t] = oss_vals.str();
      }
      // load evecs 
      vector<MatrixXcd> evectors;   // evectors[t](i,x)
      MatrixXd evalues(Nt, Nvec);  // evals(t,i)
      get_evecs(vec_names, val_names, evectors, evalues); 
      /* 
      int vec_max = 8;
      int pos = 6*Lx*Lx + 6*Lx + 6;
      string savename = "L" + std::to_string(Lx) +  "_wavefunction"  + std::to_string(vec_max) + ".txt";
      ofstream myfile;
      myfile.open(savename); 
      cout << "\n\n" << savename << "\n\n" << endl;
      for(int t=0; t < Nt; t++){
        MatrixXcd vec = evectors[t];
        MatrixXcd vec_conj = evectors[t].conjugate();

        myfile <<  "t=" << t << "\n";
        for(int x=0; x<Lx*Lx*Lx; x++){
          //complex<double> density = complex<double>(0.0,0.0);
          //for(int vec_num=0; vec_num < vec_max; vec_num++){  
          //  density +=   vec(vec_num, v_ind(0,x)) * vec_conj(vec_num, v_ind(0,x))
          //             + vec(vec_num, v_ind(1,x)) * vec_conj(vec_num, v_ind(1,x))
          //             + vec(vec_num, v_ind(2,x)) * vec_conj(vec_num, v_ind(2,x))
          //             + vec(vec_num, v_ind(3,x)) * vec_conj(vec_num, v_ind(3,x));
          //}
          // psi(x,a) = sum_i sum_y sum_b V^dagger(xa,i) V(i,yb) psi(y,b) 
          // with psi(y,b) = delta_y,pos
          // psi(x,a) = sum_i sum_b V^dagger(xa,i) V(i,pos b) 
          vector<complex<double>> wavefunction;
          for(int a=0; a<Nc; a++){
            wavefunction.push_back(complex<double>(0.0, 0.0));
            for(int vec_num=0; vec_num < vec_max; vec_num++){  
              wavefunction[a] +=   vec_conj(vec_num, v_ind(a, x)) * vec(vec_num, v_ind(0, pos))
                                 + vec_conj(vec_num, v_ind(a, x)) * vec(vec_num, v_ind(1, pos))
                                 + vec_conj(vec_num, v_ind(a, x)) * vec(vec_num, v_ind(2, pos))
                                 + vec_conj(vec_num, v_ind(a, x)) * vec(vec_num, v_ind(3, pos));
            }
          }
          // p(x) = sum_a psi(x,a) psi^dagger(x,a)
          complex<double> density =   wavefunction[0] * conj(wavefunction[0]) 
                                    + wavefunction[1] * conj(wavefunction[1])
                                    + wavefunction[2] * conj(wavefunction[2])
                                    + wavefunction[3] * conj(wavefunction[3]);

          myfile << density.real() << "\n";
        }
      }
      myfile.close();
      */

      /* write eigenvalues to file 
      string savename = "L" + std::to_string(Lx) +  "_evals.txt";
      ofstream myfile;
      myfile.open(savename); 
      cout << "\n\n" << savename << "\n\n" << endl;
      for(int t=0; t < Nt; t++){
        myfile <<  "t=" << t << "\n";
        for(int i=0; i<Nvec; i++){
          myfile << evalues(t,i) << "\n";
        }
      }
      myfile.close();
      */

      // load perambulators 
      std::vector<MatrixXcd> tau;
      get_tau(perambulator_filename, tau);

      // compute correlation functions! 
      
      if(state == "baryon_udud"){
        int p = 0;
        baryon_udud(evectors, tau, p, t_source, corr_savename_dir, cfg_num);    
      }
    
      // CONNECTED ONLY 
      if(state == "mesons"){
        std::vector<string> meson_list;
        meson_list.push_back("g5");
        meson_list.push_back("g0g5");
        meson_list.push_back("g1");
        meson_list.push_back("g2");
        meson_list.push_back("g3");
        meson_list.push_back("g0g1");
        meson_list.push_back("g0g2");
        meson_list.push_back("g0g3");
        for(int i = 0; i < meson_list.size(); i++){
          string meson_state = meson_list.at(i);
          pion(evectors, tau, t_source, corr_savename_dir, cfg_num, meson_state); 
        }
      }
    
      std::cout  << "" << std::endl;
    }
    std::cout << "   E N D  C O D E   ---   S U C C E S S !" << std::endl;
    return 0;
}
























