#!/bin/bash -l
export GCC_VERSION="8.3.1"
module load gcc/8.3.1


Nc=$1 
echo "Nc ${Nc}"
Lx=$2
echo "Lx ${Lx}"
Nt=$3
echo "Nt ${Nt}"
Nvec=$4
echo "Nvec ${Nvec}"
EXE=$5
echo "exe ${EXE}"
Step4_dir=$6
echo "Step4 dir ${Step4_dir}"
laph_HOME=$7
echo "home ${laph_HOME}"


MAIN="${Step4_dir}/main.cpp"
# delete the executable
rm ${Step4_dir}/global_variables.h

echo "#define Lx ${Lx}" > ${Step4_dir}/global_variables.h
echo "#define Nt ${Nt}" >> ${Step4_dir}/global_variables.h
echo "#define Nd 4" >> ${Step4_dir}/global_variables.h
echo "#define Nvec ${Nvec}" >> ${Step4_dir}/global_variables.h
echo "#define Nc ${Nc}" >> ${Step4_dir}/global_variables.h

echo "C O M P I L I N G  S T E P  4"
echo `date`

Iqdpxx=${laph_HOME}/code/Nc${1}_Nd3_install/qdpxx_lapH/include
Ilibxml=${laph_HOME}/code/base_install/libxml2/include/libxml2
Iqmp=${laph_HOME}/code/base_install/qmp/include/
Iarpack=${laph_HOME}/code/source/arpackpp/include/


Lqdpxx=${laph_HOME}/code/Nc${1}_Nd3_install/qdpxx_lapH/lib/
Llibxml=${laph_HOME}/code/base_install/libxml2/lib
Lqmp=${laph_HOME}/code/base_install/qmp/lib

Ieigen=${laph_HOME}/code/source/eigen
Lchroma=${laph_HOME}/code/Nc${1}_Nd4_install/chroma_Nc/lib/


# with eigen 
mpicxx -std=c++14 -O2 -fopenmp -Wno-deprecated -w -I${Iqdpxx} -I${Ilibxml} -I${Iqmp} -I${Ieigen} ${MAIN} -lgfortran -L${Lchroma} -L${Lqmp} -L${Lqdpxx} -L${Llibxml} -lchroma -lqdp -lXPathReader -lxmlWriter -lqio -llime -lxml2 -lm -lqmp -lfiledb -lfilehash -o ${EXE}


