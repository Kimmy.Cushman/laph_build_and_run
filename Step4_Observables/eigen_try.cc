#include <fstream>
#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <unsupported/Eigen/CXX11/Tensor>
#include <Eigen/Dense>
using namespace Eigen;
using cd = std::complex<double>;


int main(){
  Eigen::array<Eigen::IndexPair<int>, 2> ind1;
  // for comparing loop to eigen results 
  double diff;
  double diff_A;
  double diff_Temp;
  double d;
  
  int N = 4;
  
  // WORKs //
  // A_{ijkl} = B_{ijmn} * C_{mnkl}   // randomly chosen 
  // Temp(i,j,k,l) += B(m,n,i,j) * C(k,m,l,n)

  // ^^ note that Temp is needed for correlation function calculation
  // C(t) = T^sink_{i'j'k'l'} T^source_{ijkl} [ B_{i'ij'j} B_{k'kl'l} + other contractions ]
  // with Temp_{kli'j'} = sum_{ij} T^source_{ijkl} * B_{i'ij'j}
  // note that above, sum is over mn, kl here is ij above, i'j' here is kl above
  // and  Temp2{i'j'k'l'} = sum_{kl} Temp_{kli'j'} * B_{k'kl'l}
  // then C(t) = T^sink_{i'j'k'l} * Temp2_{i'j'k'l'}
  
  Eigen::Tensor<cd,4> A(N,N,N,N);
  Eigen::Tensor<cd,4> Temp(N,N,N,N); 
  Eigen::Tensor<cd,4> B(N,N,N,N);
  Eigen::Tensor<cd,4> C(N,N,N,N);
  Eigen::Tensor<cd,4> A_eigen(N,N,N,N);
  Eigen::Tensor<cd,4> Temp_eigen(N,N,N,N); 
  A.setZero();
  B.setZero();
  C.setZero();
  A_eigen.setZero();
  
  // fill B and C randomly 
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      for(int k=0; k<N; k++){
        for(int l=0; l<N; l++){
          cd b = cd((double) rand()/RAND_MAX, (double) rand()/RAND_MAX);
          cd c = cd((double) rand()/RAND_MAX, (double) rand()/RAND_MAX);
          //double b = (double) rand()/RAND_MAX;
          //double c = (double) rand()/RAND_MAX;
          B(i,j,k,l) = b;
          C(i,j,k,l) = c;
        }
      }
    }
  }
  // compute via loop
  // A_{ijkl} = B_{ijmn} * C_{mnkl}
  // Temp(i,j,k,l) += B(m,n,i,j) * C(k,m,l,n) 
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      for(int k=0; k<N; k++){
        for(int l=0; l<N; l++){
          for(int m=0; m<N; m++){
            for(int n=0; n<N; n++){
              // sum over m,n
              A(i,j,k,l) += B(i,j,m,n) * C(m,n,k,l);
              // sum over 
              Temp(i,j,k,l) += B(m,n,i,j) * C(k,m,l,n);
            }
          }
        }
      }
    }
  }
  
  // compute via Eigen 
  // A_{ijkl} = B_{i j m   n} * C_{ m  n k l} //
  //                   ^2  ^3       ^0 ^1
  ind1 = {Eigen::IndexPair<int>(2,0), Eigen::IndexPair<int>(3,1)};
  A_eigen = B.contract(C, ind1);
  
  // Temp(i,j,k,l) += B(m,  n, i, j) * C(k, m, l, n) \/
  //                    ^0  ^1              ^1    ^3
  ind1 = {Eigen::IndexPair<int>(0,1), Eigen::IndexPair<int>(1,3)};
  Temp_eigen = B.contract(C, ind1);

  for(int i=0; i<1; i++){
    for(int j=0; j<1; j++){
      for(int k=0; k<1; k++){
        for(int l=0; l<N; l++){
          diff_A += abs(A(i,j,k,l) - A_eigen(i,j,k,l)); 
          diff_Temp += abs(Temp(i,j,k,l) - Temp_eigen(i,j,k,l)); 
        }
      }
    }
  }
  d = diff_A / (N*N*N*N);
  std::cout << "A_{ijkl} = B_{ijmn} * C_{mnkl}" << std::endl; 
  std::cout << d << std::endl;
  
  d = diff_Temp / (N*N*N*N);
  std::cout << "Temp(i,j,k,l) += B(m,n,i,j) * C(k,m,l,n)" << std::endl;
  std::cout << d << std::endl;
  //
  // same over last index!
  std::cout << "specific elements" << std::endl;
  for(int l=0; l<N; l++){
    std::cout << Temp(0,0,0,l) << std::endl;
    std::cout << Temp_eigen(0,0,0,l) << std::endl;
    std::cout << std::endl;
    std::cout << Temp(0,0,1,l) << std::endl;
    std::cout << Temp_eigen(0,0,1,l) << std::endl;
    std::cout << std::endl;
  }

  for(int k=0; k<N; k++){
    std::cout << Temp(0,0,k,0) << std::endl;
    std::cout << Temp_eigen(0,0,k,0) << std::endl;
    std::cout << std::endl;
  }
  //
  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  /* WORKS */
  /* A2_{ij} = B3_{kil} C3_{jkl} */ 
  /*
  Eigen::Tensor<double,2> A2(N,N);
  Eigen::Tensor<double,2> A2_eigen(N,N);
  Eigen::Tensor<double,3> B3(N,N,N);
  Eigen::Tensor<double,3> C3(N,N,N);
  A2.setZero();
  A2_eigen.setZero();
  B3.setZero();
  C3.setZero();
  
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      for(int k=0; k<N; k++){
        double b = (double) rand()/RAND_MAX;
        double c = (double) rand()/RAND_MAX;
        B3(i,j,k) = b;
        C3(i,j,k) = c;
      }
    }
  }
  
  // A2_{ij} = B3_{kil} C3_{jkl} // 
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      for(int k=0; k<N; k++){
        for(int l=0; l<N; l++){
          A2(i,j) += B3(k,i,l) * C3(j,k,l);      
        }
      }
    }
  }

  
  // A2_{ij} = B3_{k i l} C3_{j k   l } // 
  //               ^0  ^1       ^1  ^2
  ind1 = {Eigen::IndexPair<int>(0,1), Eigen::IndexPair<int>(2,2)};
  A2_eigen = B3.contract(C3, ind1);
  
  for(int i=0; i<N; i++){
    for(int j=0; j<N; j++){
      diff += A2(i,j) - A2_eigen(i,j); 
    }
  }
  d = diff / (N*N);
  std::cout << "\n\n" << std::endl;
  std::cout << "Rank 2  = 3 * 3" << std::endl;  
  std::cout << d << std::endl;

  */ 
  


  return 0;
}









//
