#ifndef SPIN_MATRICES_H
#define SPIN_MATRICES_H

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <fstream>
#include <complex>
#include <vector> 
#include <set>
#include <map>
#include <unordered_set>
#include <string>
#include "Eigen/Dense"
#include "global_variables.h"
using namespace Eigen;
using namespace std; 
using std::vector;
using std::ifstream;


// define properties of a gamma matrix
struct gamma_struct{
  string name;    
  Matrix<complex<double>, Nd, Nd, RowMajor> gamma_matrix;
};

void gamma_map(gamma_struct &gamma){
  complex<double>p1(1.0,0.0);
  complex<double>n1(-1.0,0.0);
  complex<double>pi(0.0,1.0); // positive i 
  complex<double>ni(0.0,-1.0); // negaive i 
  complex<double>c0(0.0,0.0);

  Matrix<complex<double>, Nd, Nd, RowMajor> GI;
  Matrix<complex<double>, Nd, Nd, RowMajor> G0;
  Matrix<complex<double>, Nd, Nd, RowMajor> G1;
  Matrix<complex<double>, Nd, Nd, RowMajor> G2;
  Matrix<complex<double>, Nd, Nd, RowMajor> G3;
  Matrix<complex<double>, Nd, Nd, RowMajor> G5;
  Matrix<complex<double>, Nd, Nd, RowMajor> GC;

  GI << p1, c0, c0, c0,
        c0, p1, c0, c0,
        c0, c0, p1, c0, 
        c0, c0, c0, p1;
  
  // this matrix is called gamma0 in qdp manual, but corresponds to g3 for vector meson - verified 
  G3 << c0, c0, c0, pi,
        c0, c0, pi, c0,
        c0, ni, c0, c0,
        ni, c0, c0, c0;
  
  G1 << c0, c0, c0, n1,
        c0, c0, p1, c0,
        c0, p1, c0, c0,
        n1, c0, c0, c0;
  
  G2 << c0, c0, pi, c0,
        c0, c0, c0, ni,
        ni, c0, c0, c0, 
        c0, pi, c0, c0;
  
  // this matrix is called gamma3 in qdp manual, but acts like g0/g4?
  G0 << c0, c0, p1, c0,
        c0, c0, c0, p1,
        p1, c0, c0, c0, 
        c0, p1, c0, c0;
  
  G5 << p1, c0, c0, c0,
        c0, p1, c0, c0,
        c0, c0, n1, c0, 
        c0, c0, c0, n1;
 
  // FIXME C should be gamma_2 gamma_4 in Euclidean 
  // RECOMPUTE // i gamma_2 gamma_4 // C^T = -C, C = C^-1, C gamma_mu C^-1 = -gamma_mu^T
  GC << c0, ni, c0, c0,
        pi, c0, c0, c0,
        c0, c0, c0, pi, 
        c0, c0, ni, c0;

  if(gamma.name == "I"){
    gamma.gamma_matrix = GI;
  }
  if(gamma.name == "g0"){
    gamma.gamma_matrix = G0;
  }
  if(gamma.name == "g1"){
    gamma.gamma_matrix = G1;
  }
  if(gamma.name == "g2"){
    gamma.gamma_matrix = G2;
  }
  if(gamma.name == "g3"){
    gamma.gamma_matrix = G3;
  }
  if(gamma.name == "g5"){
    gamma.gamma_matrix = G5;
  }
  if(gamma.name == "C"){
    gamma.gamma_matrix = GC;
  }
  if(gamma.name == "g0g5"){
    gamma.gamma_matrix = G0 * G5;
  }
  if(gamma.name == "g0g1"){
    gamma.gamma_matrix = G0 * G1;
  }
  if(gamma.name == "g0g2"){
    gamma.gamma_matrix = G0 * G2;
  }
  if(gamma.name == "g0g3"){
    gamma.gamma_matrix = G0 * G3;
  }
  if(gamma.name == "g1g5"){
    gamma.gamma_matrix = G1 * G5;
  }
  if(gamma.name == "g2g5"){
    gamma.gamma_matrix = G2 * G5;
  }
  if(gamma.name == "g3g5"){
    gamma.gamma_matrix = G3 * G5;
  }

} // gamma_map

#endif 
