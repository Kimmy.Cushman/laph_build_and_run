import os 
import sys
import numpy as np

# execute here 
MACHINE = sys.argv[1]
Step1_dir = sys.argv[2]
exe = sys.argv[3]

# physics
Nc = int(sys.argv[4])
Lx = int(sys.argv[5])
Nt = int(sys.argv[6])
Nvec = int(sys.argv[7]) # don't need to know Nvec here
Nnoise = int(sys.argv[8]) # don't need to know Nnoise here
# job
time_job = int(sys.argv[9])
queue = sys.argv[10]
group = sys.argv[11]

# resources
tasks = int(sys.argv[12])
nodes = int(sys.argv[13])
threads = int(sys.argv[14])

# geom 
geom_x = int(sys.argv[15])
geom_y = int(sys.argv[16])
geom_z = int(sys.argv[17])
geom_t = int(sys.argv[18])

# files 
result_dir_base = sys.argv[19]    # ".../Production/.../cfg"  finish name of dir with cfg num
config_file_base = sys.argv[20]   # ".../Production/.../config_" specify path below using configs_dir
splice_output_base = sys.argv[21]      # "splice.out" specify path below using result_dir 

cfg_start = int(sys.argv[22])
cfg_stop = int(sys.argv[23])
cfg_step = int(sys.argv[24])
configs_dir = sys.argv[25]

# print job io here 
batch_output_file = "%s/splice_%i_%i.out"%(configs_dir, cfg_start, cfg_stop)
os.system("rm %s"%batch_output_file)

# write xml for reading lime in main.cpp
for cfg in np.arange(cfg_start, cfg_stop, cfg_step):
    result_dir = "%s_%i"%(result_dir_base, cfg)
    config_file = "%s%i.lime"%(config_file_base, cfg)
    os.system("sh %s/xml_bash.sh %i %i %s %s"%(Step1_dir, Lx, Nt, result_dir, config_file))

input_xml_base = "read_lime.xml" # location "%s/%s"%(result_dir, input_xml_base) specified in main.cc

#submit job
run_main = "sh %s/run_main.sh"%Step1_dir
bash_input = str(threads)
geom = "%i %i %i %i"%(geom_x, geom_y, geom_z, geom_t)
commandline_args = "%i %i %i %s %s %s %i %i %i"%(Nc, Lx, Nt, result_dir_base, input_xml_base, splice_output_base, cfg_start, cfg_stop, cfg_step)
srun_args = "%i %s %s %i %i"%(time_job, queue, batch_output_file, tasks, nodes)
executable = exe

run = " ".join([run_main, bash_input, geom, commandline_args, srun_args, executable])

print("running from config loop python script")
print
print(run)
print
print("see batch output file")
print(batch_output_file)
print
os.system(run)
