#pragma once 
#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
using namespace QDP;
using namespace RNG;

void get_site(multi1d<int> &check_site){
  check_site[0] = 2;
  check_site[1] = 3;
  check_site[2] = 0;
}


void check_precision_values(std::string precision_type, int num_dimensions){
  multi1d<int> check_site(num_dimensions);
  get_site(check_site);
  
  if(precision_type == "float"){
    QDPIO::cout << "FLOAT" << std::endl;
    LatticeColorMatrixF sample_lattice;
    initDefaultRNG();
    gaussian(sample_lattice);
   
    QDPIO::cout << "peeking site" << std::endl;
    ColorMatrixF sample_matrix = peekSite(sample_lattice, check_site);
    
    QDPIO::cout << "CHECKING PRECISION" << std::endl;
    for(int a=0; a<Nc; a++){
      for(int b=0; b<Nc; b++){
        ComplexF sample_val = peekColor(sample_matrix, a, b);
        QDPIO::cout << sample_val << std::endl;
      }
    }
  }  
  
  else if(precision_type == "double"){
    QDPIO::cout << "DOUBLE" << std::endl;
    LatticeColorMatrixD sample_lattice;
    initDefaultRNG();
    gaussian(sample_lattice);
   
    QDPIO::cout << "peeking site" << std::endl;
    ColorMatrixD sample_matrix = peekSite(sample_lattice, check_site);
    
    QDPIO::cout << "CHECKING PRECISION" << std::endl;
    for(int a=0; a<Nc; a++){
      for(int b=0; b<Nc; b++){
        ComplexD sample_val = peekColor(sample_matrix, a, b);
        QDPIO::cout << sample_val << std::endl;
      }
    }
  }  
  
  else{
    QDPIO::cout << "DEFAULT" << std::endl;
    LatticeColorMatrix sample_lattice;
    initDefaultRNG();
    gaussian(sample_lattice);
    
    QDPIO::cout << "peeking site" << std::endl;
    ColorMatrix sample_matrix = peekSite(sample_lattice, check_site);
    
    QDPIO::cout << "CHECKING PRECISION" << std::endl;
    for(int a=0; a<Nc; a++){
      for(int b=0; b<Nc; b++){
        Complex sample_val = peekColor(sample_matrix, a, b);
        QDPIO::cout << sample_val << std::endl;
      }
    }
  }  

}
