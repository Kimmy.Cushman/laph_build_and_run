#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include "qdp.h"
#include "Include/qdp_iogauge.h"
#include "Include/examples.h"
// database 
#include "qdp_map_obj_disk.h"
#include "qdp_disk_map_slice.h"
#include "Include/splicer_funcs.h"
// Including these just to check compilation
//#include "qdp_map_obj_memory.h"
//#include "qdp_map_obj_null.h"
#include "precision_check.h"

using namespace QDP;

typedef struct { 
  std::string ILDG_file_name;
} UserInput;


int main(int argc, char *argv[])
{
  START_CODE();
  // Put the machine into a known state
  QDP_initialize(&argc, &argv);// at this point, Nd and all get dynamically defined
  QDPIO::cout << "Initializing lattice \n";
  
  
  /* MUST INCLUDE COMMAND LINE ARGS */
  // // // // // // // // // // // //
  int Lx = atoi(argv[1]);
  int Nt = atoi(argv[2]);
  std::string read_lime_xml_base = argv[3];
  std::string spliced_mod_base = argv[4];
  int cfg_start = atoi(argv[5]);
  int cfg_stop = atoi(argv[6]);
  int cfg_step = atoi(argv[7]);
  
  /*  REQUIRED  AS  ABOVE  */  
  
  const int latt_size[] = {Lx,Lx,Lx,Nt};
  multi1d<int> nrow(Nd);
  nrow = latt_size;  // Use only Nd elements
  Layout::setLattSize(nrow);
  Layout::create(); 
  
  QDPIO::cout << "Lx " << Lx << std::endl; 
  QDPIO::cout << "Nt " << Nt << std::endl; 
  QDPIO::cout << "xml " << read_lime_xml_base << std::endl; 
  QDPIO::cout << "mod " << spliced_mod_base << std::endl; 
  QDPIO::cout << cfg_start << std::endl; 
  QDPIO::cout << cfg_stop << std::endl; 
  QDPIO::cout << cfg_step << std::endl; 

  
  std::string precision_type = "default";
  QDPIO::cout << precision_type << std::endl;
  check_precision_values(precision_type, Nd);

  for(int i=cfg_start; i<cfg_stop; i+=cfg_step){
    std::string input_xml = read_lime_xml_base;
    input_xml += "_cfg";
    input_xml += std::to_string(i);
    input_xml += ".xml";
    QDPIO::cout << "input: " << input_xml <<  std::endl;   

    std::string output_mod = spliced_mod_base;
    output_mod += "_cfg";
    output_mod += std::to_string(i);
    output_mod += ".mod";
    QDPIO::cout << "output: " << output_mod <<  std::endl;   
    
    /////////////////////////////////////////////////////////////////////////
    QDPIO::cout << "reading input \n";
    UserInput p;
    try { 
      XMLReader param(input_xml);
      XMLReader paramtop(param, "/ildglat");
      read(paramtop, "ILDG_file", p.ILDG_file_name);
      read(paramtop, "nrow", nrow);

    } catch(const std::string& e) { 
      QDPIO::cout << "Caught exception while reading XML: " << e << "\n";
      QDP_abort(1);
    }
    
    QDPIO::cout << "check5" << std::endl;   
    // Try to read the NERSC Archive file
    multi1d<LatticeColorMatrix> u(Nd);

    XMLReader record_in_xml;
    XMLReader file_in_xml;
    // Set QIO Verbosity to DEBUG
    //QIO_verbose(QIO_VERB_DEBUG);
    // ^ this commented out gets rid of a bunch of slowwww print statements!
    QDPFileReader ildg_reader(file_in_xml, p.ILDG_file_name, QDPIO_SERIAL);
    ildg_reader.read(record_in_xml, u);
    
    
    // Check that the gauge field is being read correctly 
    Double w_plaq, s_plaq, t_plaq, link;
    MesPlq(u, w_plaq, s_plaq, t_plaq, link);
    QDPIO::cout << "w_plaq " << w_plaq << "\n";
    QDPIO::cout << "link " << link << "\n";
    //////////////////////////////////////////////////////////////////////////
    // Params to create a map object disk
    std::string map_obj_file(output_mod);

    // Some metadata
    std::string meta_data;
    {
      XMLBufferWriter file_xml;
      push(file_xml, "DBMetaData");
      write(file_xml, "id", std::string("LatticeColorMatrix"));
      write(file_xml, "lattSize", QDP::Layout::lattSize());
      pop(file_xml);
      meta_data = file_xml.str();
    }
    multi1d<LatticeColorMatrix> lcm_array(Nd); // u field has 4 components,  
    for(int i=0; i < lcm_array.size(); i++) { // but we are only on one timelsice 
      //QDPIO::cout << "timeslice? " << i << std::endl;
      lcm_array[i] = u[i];
    }
    try {
      QDPIO::cout<< "W R I T I N G" << std::endl;
      QDPIO::cout << "\n\n\nTest DB with time-slices" << std::endl;

      MapObjectDisk<KeyLatticeColorMatrixTimeSlice_t, TimeSliceIO<LatticeColorMatrix> > pc_map;
      
      pc_map.setDebug(1);
      pc_map.insertUserdata(meta_data);
      pc_map.open(map_obj_file, std::ios_base::in | std::ios_base::out | std::ios_base::trunc);
      
      int Lt = Layout::lattSize()[Nd-1];
      testMapKeyLatticeColorMatrixInsertionsTimeSlice(pc_map, lcm_array); // insertion happens here
      testMapKeyLatticeColorMatrixLookupsTimeSlice(pc_map, lcm_array);
      QDPIO::cout << std::endl << "OK" << std::endl;

      QDPIO::cout<< "D O N E      W R I T I N G    C O N F I G  " << i  << std::endl;
    } catch(const std::string& e) { 
      QDPIO::cout << "ERROR" << std::endl;
      QDPIO::cout << "Caught: " << e << std::endl;
      fail(__LINE__);
    }  
  } 
  QDP_finalize();
  exit(0);
} // main































//
