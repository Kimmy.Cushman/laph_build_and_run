#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
#include "qdp.h"
#include "Include/qdp_iogauge.h"
#include "Include/examples.h"
#include "rotate_funcs.h"
// database 
//#include "qdp_map_obj_disk.h"
//#include "qdp_disk_map_slice.h"

using namespace QDP;

int get_site(int Lx, int x, int y, int z, int t)
{
  int site = x*Lx*Lx*Lx + y*Lx*Lx + z*Lx + t;
  return site;
}



typedef struct { 
  std::string ILDG_file_name;
} UserInput;


int main(int argc, char *argv[])
{
  START_CODE();
  // Put the machine into a known state
  QDP_initialize(&argc, &argv);// at this point, Nd and all get dynamically defined
  QDPIO::cout << "Initializing lattice \n";
  
  
  /* MUST INCLUDE COMMAND LINE ARGS */
  // // // // // // // // // // // //
  int Lx = atoi(argv[1]);
  int Nt = atoi(argv[2]);
  std::string read_lime_xml_base = argv[3];
  std::string rotation_type = argv[4];
  std::string rotated_config_base = argv[5];
  int cfg_start = atoi(argv[6]);
  int cfg_stop = atoi(argv[7]);
  int cfg_step = atoi(argv[8]);
  
  /*  REQUIRED  AS  ABOVE  */  
  
  const int latt_size[] = {Lx,Lx,Lx,Nt};
  multi1d<int> nrow(Nd);
  nrow = latt_size;  // Use only Nd elements
  Layout::setLattSize(nrow);
  Layout::create(); 
  
  QDPIO::cout << "Lx " << Lx << std::endl; 
  QDPIO::cout << "Nt " << Nt << std::endl; 
  QDPIO::cout << "in xml  " << read_lime_xml_base << std::endl; 
  QDPIO::cout << "rotation type " << rotation_type << std::endl; 
  QDPIO::cout << cfg_start << std::endl; 
  QDPIO::cout << cfg_stop << std::endl; 
  QDPIO::cout << cfg_step << std::endl; 
  
  for(int i=cfg_start; i<cfg_stop; i+=cfg_step){
    QDPIO::cout << "LOOP cfg " << i << std::endl;
    std::string input_xml     = read_lime_xml_base;
    std::string output_lime     = rotated_config_base;
    output_lime   += "rotate_";
    output_lime   += rotation_type;
    
    input_xml     += "_cfg";
    output_lime   += "_cfg";

    input_xml     += std::to_string(i);
    output_lime   += std::to_string(i);
    
    input_xml     += ".xml";
    output_lime   += ".lime";
    
    QDPIO::cout << "input: "       << input_xml   <<  std::endl;   
    QDPIO::cout << "output lime: " << output_lime <<  std::endl;   

    /////////////////////////////////////////////////////////////////////////
    UserInput p_in;
    try { 
      XMLReader param_in(input_xml);

      XMLReader paramtop_in(param_in, "/ildglat");
      
      read(paramtop_in,  "ILDG_file", p_in.ILDG_file_name);
      
      read(paramtop_in, "nrow", nrow);

    } catch(const std::string& e) { 
      QDPIO::cout << "Caught exception while reading XML: " << e << "\n";
      QDP_abort(1);
    }
    std::string infilename = p_in.ILDG_file_name;
    QDPIO::cout << "p_in.ILDG_file_name" << std::endl;
    QDPIO::cout << p_in.ILDG_file_name << std::endl;
    QDPIO::cout << "infilename" << std::endl;
    QDPIO::cout << infilename << std::endl;

    multi1d<LatticeColorMatrix> u(Nd);
    //read_cfg(u, infilename);
    XMLReader record_in_xml;
    XMLReader file_in_xml;
    QDPFileReader ildg_reader(file_in_xml, infilename, QDPIO_SERIAL);
    
    ildg_reader.read(record_in_xml, u);
    ildg_reader.close();
    //////////////////////////////////////////////////////////////////////////
    multi1d<LatticeColorMatrix> U_rot(Nd); 
    
    if( rotation_type == "identity"){
      QDPIO::cout << "copying u as is" << std::endl;
      U_rot[0] = u[0];
      U_rot[1] = u[1];
      U_rot[2] = u[2];
      U_rot[3] = u[3];
    }
    
    if( rotation_type == "identity_peekpoke"){
      QDPIO::cout << "peeking and poking identity" << std::endl;
      identity_rotation(U_rot, u,  Lx);
    }
    
    if( rotation_type == "xyz_relabel"){
      QDPIO::cout << "shuffling labels xyz" << std::endl;
      U_rot[0] = u[1];
      U_rot[1] = u[2];
      U_rot[2] = u[0];
      U_rot[3] = u[3];
    }
    
    if( rotation_type == "xyz_cyclic"){
      QDPIO::cout << "running cyclic rotation xyz" << std::endl;
      cyclic_rotate_lattice_xyz(U_rot, u,  Lx);
    }
    
    if( rotation_type == "xy3piover2"){
      QDPIO::cout << "running rotation in xy plane" << std::endl;
      rotate_lattice_xy_3piover2(U_rot, u,  Lx);
    }
    
    


    // Dump config
    // Open output file
    //QDPFileWriter ildg_out(file_out, outfilename, QDPIO_SINGLEFILE, QDPIO_SERIAL,p.dataLFN);
    // Keep file and record XMLs
    //file_out << file_in_xml;
    //record_out << record_in_xml;
    
    XMLBufferWriter record_out_xml;
    XMLBufferWriter file_out_xml;

    file_out_xml << file_in_xml;
    record_out_xml << record_in_xml;

    std::string outfilename = output_lime;
    QDPFileWriter ildg_out(file_out_xml, outfilename, QDPIO_SINGLEFILE, QDPIO_SERIAL);
    
    ildg_out.write(record_out_xml, U_xyz);
    ildg_out.close();
 
    QDPIO::cout << "Just wrote U_rot to lime" << std::endl;
    
    // Reread the ILDG File
    //XMLReader record_back_in_xml;
    //XMLReader file_back_in_xml;
    //QDPFileReader ildg_back_in(file_back_in_xml, outfilename, QDPIO_SERIAL);
    //multi1d<LatticeColorMatrix> u_back_in(Nd);
    //ildg_back_in.read(record_back_in_xml, u_back_in);
    //ildg_back_in.close();

    multi1d<LatticeColorMatrix> u_back_in(Nd);
    std::string back_in_filename = outfilename;
    read_cfg(u_back_in, back_in_filename);

    //record_in_xml.print(cout);
    //cout.flush();
    Double w_plaq_back;
    Double s_plaq_back;
    Double t_plaq_back;
    Double link_back;

    MesPlq(u_back_in, w_plaq_back, s_plaq_back, t_plaq_back, link_back);
    QDPIO::cout << "Read Back Plaquette " << w_plaq_back << std::endl;
  } 
  QDP_finalize();
  exit(0);
} // main































//
