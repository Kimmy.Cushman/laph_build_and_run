#pragma once 
#include <iostream>
#include <cstdio>
#include <string>
#include <cstring>
using namespace QDP;

//QDPFileReader ildg_reader(file_in_xml, p_in.ILDG_file_name, QDPIO_SERIAL);


void read_cfg(multi1d<LatticeColorMatrix> &u, std::string filename){
  XMLReader record_in_xml;
  XMLReader file_in_xml;
  QDPFileReader ildg_reader(file_in_xml, filename, QDPIO_SERIAL);
  
  ildg_reader.read(record_in_xml, u);
  ildg_reader.close();
}

void identity_rotation(multi1d<LatticeColorMatrix> &U_rot, const multi1d<LatticeColorMatrix> &u, int Lx){
  // Rotation matrix 
  LatticeColorMatrix U0;
  LatticeColorMatrix U1;
  LatticeColorMatrix U2;
  LatticeColorMatrix U3;

  for(int x=0; x<Lx; x++){
    QDPIO::cout << x << std::endl;
    for(int y=0; y<Lx; y++){
      for(int z=0; z<Lx; z++){
        for(int t=0; t<2*Lx; t++){
          multi1d<int> site_rot(4);
          site_rot[0] = x;      
          site_rot[1] = y;  
          site_rot[2] = z;
          site_rot[3] = t;

          ColorMatrix u_0 = peekSite(u[0], site_rot);
          ColorMatrix u_1 = peekSite(u[1], site_rot);   
          ColorMatrix u_2 = peekSite(u[2], site_rot);   
          ColorMatrix u_3 = peekSite(u[3], site_rot);   

          multi1d<int> site(4);
          site[0] = x;
          site[1] = y;
          site[2] = z;
          site[3] = t;
           
          U0 = pokeSite(U0, u_0, site);   
          U1 = pokeSite(U1, u_1, site);  
          U2 = pokeSite(U2, u_2, site);   
          U3 = pokeSite(U3, u_3, site);   
       
        } // t loop
      } // z loop
    } // y loop
  } // x loop 
  
  U_rot[0] = U0;
  U_rot[1] = U1;
  U_rot[2] = U2;
  U_rot[3] = U3;


}


void cyclic_rotate_lattice_xyz(multi1d<LatticeColorMatrix> &U_rot, const multi1d<LatticeColorMatrix> &u, int Lx, std::string inverse){
  // Rotation matrix 
  // |  0 1 0 0 |
  // |  0 0 1 0 |
  // |  1 0 0 0 |
  // |  0 0 0 1 |
  // R [U_mu(x)] = u_{R mu}(R x) 
  // -> U_rot_0(x,y,z,t) = u_1 (y,z,x,t)
  // -> U_rot_1(x,y,z,t) = u_2 (y,z,x,t) 
  // -> U_rot_2(x,y,z,t) = u_0 (y,z,x,t)
  // -> U_rot_3(x,y,z,t) = u_3 (y,z,x,t)
  // R [U_mu(x)] = u_{R mu}(R^-1 x) 
  // -> U_rot_0(x,y,z,t) = u_1 (z,x,y,t)
  // -> U_rot_1(x,y,z,t) = u_2 (z,x,y,t) 
  // -> U_rot_2(x,y,z,t) = u_0 (z,x,y,t)
  // -> U_rot_3(x,y,z,t) = u_3 (z,x,y,t)
  LatticeColorMatrix U0;
  LatticeColorMatrix U1;
  LatticeColorMatrix U2;
  LatticeColorMatrix U3;

  for(int x=0; x<Lx; x++){
    QDPIO::cout << x << std::endl;
    for(int y=0; y<Lx; y++){
      for(int z=0; z<Lx; z++){
        for(int t=0; t<2*Lx; t++){
          multi1d<int> site_rot(4);
          if(inverse == "false"){
            site_rot[0] = y;      
            site_rot[1] = z;  
            site_rot[2] = x;
            site_rot[3] = t;
          }
          else{
            site_rot[0] = z;      
            site_rot[1] = x;  
            site_rot[2] = y;
            site_rot[3] = t;
          }

          ColorMatrix u_0 = peekSite(u[0], site_rot);
          ColorMatrix u_1 = peekSite(u[1], site_rot);   
          ColorMatrix u_2 = peekSite(u[2], site_rot);   
          ColorMatrix u_3 = peekSite(u[3], site_rot);   

          multi1d<int> site(4);
          site[0] = x;
          site[1] = y;
          site[2] = z;
          site[3] = t;
           
          U0 = pokeSite(U0, u_1, site);   
          U1 = pokeSite(U1, u_2, site);  
          U2 = pokeSite(U2, u_0, site);   
          U3 = pokeSite(U3, u_3, site);   
       
        } // t loop
      } // z loop
    } // y loop
  } // x loop 
  
  U_rot[0] = U0;
  U_rot[1] = U1;
  U_rot[2] = U2;
  U_rot[3] = U3;


}



void rotate_lattice_xy_3piover2(multi1d<LatticeColorMatrix> &U_rot, const multi1d<LatticeColorMatrix> &u, int Lx, std::string inverse){
  // Rotation matrix 
  // |  0 1 0 0 |
  // | -1 0 0 0 |
  // |  0 0 1 0 |
  // |  0 0 0 1 |
  // R [U_mu(x)] = u_{R mu}(R x) 
  // -> U_rot_0(x,y,z,t) = u_1   (y,-x,z,t)
  // -> U_rot_1(x,y,z,t) = u_{-0}(y,-x,z,t) = [ u_0(y-1,-x,z,t) ]^dagger 
  // -> U_rot_2(x,y,z,t) = u_2   (y,-x,z,t)
  // -> U_rot_3(x,y,z,t) = u_3   (y,-x,z,t)
  // R [U_mu(x)] = u_{R mu}(R^-1 x) 
  // -> U_rot_0(x,y,z,t) = u_1   (-y,x,z,t)
  // -> U_rot_1(x,y,z,t) = u_{-0}(-y,x,z,t) = [ u_0(-y-1,x,z,t) ]^dagger 
  // -> U_rot_2(x,y,z,t) = u_2   (-y,x,z,t)
  // -> U_rot_3(x,y,z,t) = u_3   (-y,x,z,t)
  LatticeColorMatrix U0;
  LatticeColorMatrix U1;
  LatticeColorMatrix U2;
  LatticeColorMatrix U3;

  for(int x=0; x<Lx; x++){
    QDPIO::cout << x << std::endl;
    for(int y=0; y<Lx; y++){
      for(int z=0; z<Lx; z++){
        for(int t=0; t<2*Lx; t++){
          multi1d<int> site_rot(4);
          if(inverse == "false"){
            site_rot[0] = y;
            site_rot[1] = (-x + Lx) % Lx;                   /// OMG WHY DO YOU HAVE TO ADD LX THAT IS SO DUMB - BE CAREFUL
            site_rot[2] =  z;
            site_rot[3] =  t;
          } 
          else{
            site_rot[0] = (-y + Lx) % Lx;
            site_rot[1] =  x;
            site_rot[2] =  z;
            site_rot[3] =  t;
          }
          multi1d<int> site_minus(4);
          site_minus[0] = (site_rot[0] - 1 + Lx) % Lx; 
          site_minus[1] = site_rot[1];
          site_minus[2] = site_rot[2];
          site_minus[3] = site_rot[3];

          ColorMatrix u_m0d = peekSite(u[0], site_minus); // u_-0(y,-x,z,t) = [ u_0(y-1,-x,z,t) ]^dagger
          ColorMatrix u_m0  = adj(u_m0d);
          //ColorMatrix u_m0 = peekSite(u[0], site_rot); // u_-0(y,-x,z,t) = [ u_0(y-1,-x,z,t) ]^dagger         // CHECK IDENTITY
          ColorMatrix u_1 = peekSite(u[1], site_rot);   // u_1(y,-x,z,t)
          ColorMatrix u_2 = peekSite(u[2], site_rot);   // u_2(y,-x,z,t)
          ColorMatrix u_3 = peekSite(u[3], site_rot);   // u_3(y,-x,z,t)
          
          //ColorMatrix adj_u_0_site = adj(u_0_site);

          multi1d<int> site(4);
          site[0] = x;
          site[1] = y;
          site[2] = z;
          site[3] = t;
           
          U0 = pokeSite(U0, u_1,  site);   // U_rot_0(x,y,z,t) =  u_1(y,-x,z,t)
          U1 = pokeSite(U1, u_m0, site);   // U_rot_1(x,y,z,t) =  [ u_0(y-1,-x,z,t) ]^dagger
          U2 = pokeSite(U2, u_2,  site);   // U_rot_2(x,y,z,t) =  u_2(y,-x,z,t)
          U3 = pokeSite(U3, u_3,  site);   // U_rot_3(x,y,z,t) =  u_3(y,-x,z,t)
       
        } // t loop
      } // z loop
    } // y loop
  } // x loop 
  
  U_rot[0] = U0;
  U_rot[1] = U1;
  U_rot[2] = U2;
  U_rot[3] = U3;


}

void get_x_p_mu_site(multi1d<int> &site_x_p_mu, int mu, int Lx, int x, int y, int z, int t){
  if(mu == 0){
    site_x_p_mu[0] = (x + 1) % Lx;
    site_x_p_mu[1] = y;
    site_x_p_mu[2] = z;
    site_x_p_mu[3] = t;
  } 
  if(mu == 1){
    site_x_p_mu[0] = x;
    site_x_p_mu[1] = (y + 1) % Lx;
    site_x_p_mu[2] = z;
    site_x_p_mu[3] = t;
  }
  if(mu == 2){
    site_x_p_mu[0] = x;
    site_x_p_mu[1] = y;
    site_x_p_mu[2] = (z + 1) % Lx;
    site_x_p_mu[3] = t;
  }
  if(mu == 3){
    site_x_p_mu[0] = x;
    site_x_p_mu[1] = y;
    site_x_p_mu[2] = z;
    site_x_p_mu[3] = (t + 1) % Lx;
  }
}



void calculate_plaquette(ColorMatrix &U_munu, const multi1d<LatticeColorMatrix> &u, int mu, int nu, int Lx, int x, int y, int z, int t){
    // explcitly check plaquette 
    // U_{\mu\nu}(x) = U_mu(x) U_nu(x+mu) U^dag_mu(x+nu) U^dag_nu(x)
    
    multi1d<int> site_x(4);
    site_x[0] = x;
    site_x[1] = y;
    site_x[2] = z;
    site_x[3] = t;
    
    multi1d<int> site_x_p_mu(4);
    multi1d<int> site_x_p_nu(4);
    get_x_p_mu_site(site_x_p_mu, mu, Lx, x, y, z, t);   
    get_x_p_mu_site(site_x_p_nu, nu, Lx, x, y, z, t);   

    ColorMatrix U_mu_x      = peekSite(u[mu], site_x);
    ColorMatrix U_nu_x_p_mu = peekSite(u[nu], site_x_p_mu);
    ColorMatrix U_mu_x_p_nu = peekSite(u[mu], site_x_p_nu);
    ColorMatrix U_nu_x      = peekSite(u[nu], site_x);

    ColorMatrix adjUmuxpnu_adjUnux = adj(U_mu_x_p_nu) * adj(U_nu_x);
    ColorMatrix Unuxpmu_adjUmuxpnu_adjUnux = U_nu_x_p_mu * adjUmuxpnu_adjUnux;
    ColorMatrix Umux_Unuxpmu_adjUmuxpnu_adjUnux = U_mu_x * Unuxpmu_adjUmuxpnu_adjUnux;
    
    U_munu = Umux_Unuxpmu_adjUmuxpnu_adjUnux;

}


void compare_plaquettes(const multi1d<LatticeColorMatrix> u, const multi1d<LatticeColorMatrix> U_rot, std::string rotation_type, int Lx){
    int mu, nu, x, y, z, t;
    int mu_rot, nu_rot, x_rot, y_rot, z_rot, t_rot;
    // original
    mu = 2;
    nu = 1;
    x = 1;
    y = 2;
    z = 3;
    t = 2;

    if( rotation_type == "identity" || rotation_type == "identity_peekpoke" || rotation_type == "u1u1u1u3"){
      mu_rot = mu;
      nu_rot = nu;
      x_rot = x;
      y_rot = y;
      z_rot = z;
      t_rot = t;
    }
    
    if( rotation_type == "xyz_relabel"){
      mu_rot = 1;
      nu_rot = 0;
      x_rot = x;
      y_rot = y;
      z_rot = z;
      t_rot = t;
    }
    
    if( rotation_type == "xyz_cyclic"){
      mu_rot = 1;
      nu_rot = 0;
      x_rot = y;
      y_rot = z;
      z_rot = x;
      t_rot = t;
    }
    
    if( rotation_type == "xy3piover2"){
      mu_rot = 2;
      nu_rot = 0;
      x_rot = (-y + Lx) % Lx;
      y_rot = x;
      z_rot = z;
      t_rot = t;
    }
    
    ColorMatrix U_munu;
    calculate_plaquette(U_munu, u, mu, nu, Lx, x, y, z, t);
    
    ColorMatrix U_munu_rot;
    calculate_plaquette(U_munu_rot, U_rot, mu_rot, nu_rot, Lx, x_rot, y_rot, z_rot, t_rot);

    for(int a=0; a<Nc; a++){
      for(int b=0; b<Nc; b++){
        Complex val     = peekColor(U_munu, a, b);
        Complex val_rot = peekColor(U_munu_rot, a, b);
        QDPIO::cout << val     << std::endl;
        QDPIO::cout << val_rot << std::endl;
        QDPIO::cout << std::endl;
      }
    }
    //QDPIO::cout << U_munu << std::endl;
    
    // Check that the gauge field is being read correctly 
    Double w_plaq, w_plaq_rot;
    Double s_plaq, s_plaq_rot;
    Double t_plaq, t_plaq_rot;
    Double link, link_rot;

    MesPlq(u,     w_plaq,     s_plaq,     t_plaq,     link); 
    MesPlq(U_rot, w_plaq_rot, s_plaq_rot, t_plaq_rot, link_rot);
    QDPIO::cout << "w_plaq      " << w_plaq         << "\n";
    QDPIO::cout << "w_plaq_rot  " << w_plaq_rot     << "\n\n";


}

