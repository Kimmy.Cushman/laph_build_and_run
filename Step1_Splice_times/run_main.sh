#!/bin/bash 
## sources intel compiler variables (don't change this)
export GCC_VERSION="8.3.1"
module load gcc/${GCC_VERSION}


THREADS=$1
GEOM_X=$2
GEOM_Y=$3
GEOM_Z=$4
GEOM_T=$5

Nc=$6
Lx=$7
Nt=$8
result_dir_base=$9
# max 9 command line arguments workaround 
shift 1
input_xml_base=$9
shift 1
output_mod_base=$9
shift 1
cfg_start=$9
shift 1
cfg_stop=$9
shift 1
cfg_step=$9
# srun parameters 
shift 1
TIME=$9
shift 1
QUEUE=$9
shift 1
BATCH_OUTPUT_FILE=$9
shift 1
TASKS=$9
shift 1
NODES=$9
# executable
shift 1
exe=$9

export OMP_NUM_THREADS=${THREADS}
run="srun -t ${TIME} -p ${QUEUE} -o ${BATCH_OUTPUT_FILE} -n ${TASKS} -N ${NODES}"
geom="-geom ${GEOM_X} ${GEOM_Y} ${GEOM_Z} ${GEOM_T}"
input="${Lx} ${Nt} ${result_dir_base} ${input_xml_base} ${output_mod_base} ${cfg_start} ${cfg_stop} ${cfg_step}" ## command line variables 
runme="${run} ${exe} ${input} ${geom} < /dev/null"

echo "Before analysis: ", `date`
echo "RUN   : ${run} \n"
echo "EXE   : ${exe}\n"
echo "INPUT : $input\n"
echo "RUNME : $runme\n"  
time ${runme}  ## runs script here 
echo "After analysis: ", `date`

