#!/bin/bash 
export GCC_VERSION="8.3.1"
module load gcc/8.3.1

Nc=$1
EXE=$2
HERE=$3 
laph_HOME=$4

echo "C O M P I L I N G  R O T A T E"

Iqdpxx=${laph_HOME}/code/Nc${Nc}_Nd4_install/qdpxx_lapH/include
Ilibxml=${laph_HOME}/code/base_install/libxml2/include/libxml2
Iqmp=${laph_HOME}/code/base_install/qmp/include/

Lqdpxx=${laph_HOME}/code/Nc${Nc}_Nd4_install/qdpxx_lapH/lib/
Llibxml=${laph_HOME}/code/base_install/libxml2/lib
Lqmp=${laph_HOME}/code/base_install/qmp/lib


mpicxx -std=c++14 -O2 -fopenmp -I${Iqdpxx} -I${Ilibxml} -I${Iqmp} ${HERE}/rotate.cc ${HERE}/Include/reunit.cc ${HERE}/Include/mesplq.cc -L${Lqdpxx} -lqdp -lXPathReader -lxmlWriter -lqio -llime -L${Lqmp} -lxml2 -lm -lqmp -lqmp -lfiledb -lfilehash -o ${EXE}

echo "DONE COMPILING ROTATE"
