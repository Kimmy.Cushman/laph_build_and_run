#ifndef SPLICER_FUNCS_H
#define SPLICER_FUNCS_H


#include <iostream>
#include <cstdio>
#include <string>
#include "qdp.h"
#include "qdp_iogauge.h"
// database 
#include "qdp_map_obj_disk.h"
#include "qdp_disk_map_slice.h"
// Including these just to check compilation
//#include "qdp_map_obj_memory.h"
//#include "qdp_map_obj_null.h"

namespace QDP
{

  //****************************************************************************
  //! Prop operator
  struct KeyLatticeColorMatrixTimeSlice_t
  {
    int        t_slice;       /*!< Time slice */
    int        direction;     /*!< direction */
  };

  //----------------------------------------------------------------------------
  // KeyLatticeColorMatrix read
  void read(BinaryReader& bin, KeyLatticeColorMatrixTimeSlice_t& param)
  {
    read(bin, param.t_slice);
    read(bin, param.direction);
  }

  // KeyLatticeColorMatrix write
  void write(BinaryWriter& bin, const KeyLatticeColorMatrixTimeSlice_t& param)
  {
    write(bin, param.t_slice);
    write(bin, param.direction);
  }
}

  //////////////////////////////////////////////////////////////////////////


using namespace QDP;
void fail(int line)
{
  QDPIO::cout << "FAIL: line= " << line << std::endl;
  QDP_finalize();
  exit(1);
}

//**********************************************************************************************
void testMapKeyLatticeColorMatrixInsertionsTimeSlice(MapObjectDisk<KeyLatticeColorMatrixTimeSlice_t, TimeSliceIO<LatticeColorMatrix> >& pc_map, 
					       const multi1d<LatticeColorMatrix>& lcm_array)

{
  // Create the key-type
  KeyLatticeColorMatrixTimeSlice_t the_key = {0,0};

  // OpenMap for Writing
  QDPIO::cout << "Opening Map<KeyLatticeColorMatrixTimeSlice_t,TimeSlice<LCM>> for writing..." << std::endl;
  QDPIO::cout << "Currently map has size = " << pc_map.size() << std::endl;

  QDPIO::cout << "Inserting array element : ";
  for(int i=0; i < lcm_array.size(); i++) { 
    the_key.direction = i;

    try { 
      for(int time_slice=0; time_slice < Layout::lattSize()[Nd-1]; ++time_slice)
      {
        QDPIO::cout << "time: " << time_slice << std::endl; 
        the_key.t_slice = time_slice;

        LatticeColorMatrix fred = lcm_array[i];
        int size = sizeof(fred.elem(4095));
        if (size != 256)
          QDPIO::cout << "LCM size: " << size << std::endl;
        if (pc_map.insert(the_key, TimeSliceIO<LatticeColorMatrix>(fred, time_slice)) != 0)
        {
          QDPIO::cerr << __func__ << ": error writing key\n";
          QDP_abort(1);
        }
        //QDPIO::cout << "i= "<< i << "  time_slice= " << time_slice << std::endl;                                  KIMMY EDIT 
      }
    }
    catch(...) {
      fail(__LINE__);
    }
  }

  QDPIO::cout << "Before exiting map has size = " << pc_map.size() << std::endl;
  QDPIO::cout << "Finishing Map<KeyLatticeColorMatrixTimeSlice_t,TimeSlice<LCM>> for writing..." << std::endl;
}

void testMapKeyLatticeColorMatrixLookupsTimeSlice(MapObjectDisk<KeyLatticeColorMatrixTimeSlice_t, TimeSliceIO<LatticeColorMatrix> >& pc_map, 
					    const multi1d<LatticeColorMatrix>& lcm_array)
{
  // Open map in read mode
  QDPIO::cout << "Opening Map<KeyLatticeColorMatrixTimeSlice_t,TimeSlice<LCM>> for reading.." << std::endl;
  QDPIO::cout << "Before starting map has size = " << pc_map.size() << std::endl;

  QDPIO::cout << "Increasing lookup test:" << std::endl;
  QDPIO::cout << "Looking up with direction = ";
  // Create the key-type
  KeyLatticeColorMatrixTimeSlice_t the_key = {0,0};

  for(int i=0; i < lcm_array.size(); i++) {
    LatticeColorMatrix lcm_tmp;

    the_key.direction=i;
    try{
      for(int time_slice=0; time_slice < Layout::lattSize()[Nd-1]; ++time_slice)
      {
	the_key.t_slice = time_slice;
  
	TimeSliceIO<LatticeColorMatrix> time_slice_lcm(lcm_tmp, time_slice);
	if (pc_map.get(the_key, time_slice_lcm) != 0)
	{
	  QDPIO::cerr << __func__ << ": error in get\n";
	  QDP_abort(1);
	}
	//QDPIO::cout << "i= "<< i << "  time_slice= " << time_slice << std::endl;                                KIMMY  EDIT 
      }
    }
    catch(...) { 
      fail(__LINE__);
    }

    // Compare with lf_array
    LatticeColorMatrix diff;
    diff = lcm_tmp - lcm_array[i];
    Double diff_norm = sqrt(norm2(diff))/Double(Nc*Nc*Layout::vol());
    //if(  toDouble(diff_norm) < 1.0e-6 )  { 
    if(  toDouble(diff_norm) < 1.0 )  { 
      QDPIO::cout << "." ;
    }
    else { 
      QDPIO::cout << "norm2(diff)= " << diff_norm << std::endl;
      fail(__LINE__);
    }

  }
  QDPIO::cout << std::endl << "OK" << std::endl;
}



/*
//! Function object used for constructing the time-slice set
class TimeSliceFunc : public SetFunc
{
public:
  TimeSliceFunc(int t_direction): time_direction(t_direction) {}

  int operator() (const multi1d<int>& coordinate) const {return coordinate[time_direction];}
  int numSubsets() const {return Layout::lattSize()[time_direction];}

  int time_direction;

private:
  TimeSliceFunc() {}  // hide default constructor
};




*/




#endif // SPLICER_FUNCS_H
